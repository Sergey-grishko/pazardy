import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import connect from "react-redux/es/connect/connect";


class LeftPanelCart extends React.Component {
    render() {
        let token = localStorage.getItem("pazardy-token")
        return (
            <div className={css(style.content)}>
                <div className={css(style.header)}>
                    <div className={css(style.home)}>
                        <NavLink
                            to="/">
                            <span>Home</span>
                        </NavLink>
                        <div>
                            <img src={require('../sources/shopping-cart.png')} alt="shopping"/>
                            {this.props.store.cart.ItemsCart !== 0 ?( <div>{this.props.store.cart.ItemsCart}</div>):null}
                        </div>
                    </div>
                    <p></p>
                    <span className={css(style.notifyHeader)}>
                        <div>
                            <img src={require('../sources/bell.png')} alt="bell"/>
                            <div> </div>
                        </div>
                        <span>
                            <select>
                                <option>EN</option>
                                <option>RU</option>
                                <option>UA</option>
                            </select>
                            <div className="select-side">
                                <img src={require('../sources/down-arrow.png')} alt="arrow"/>
                            </div>
                        </span>
                    </span>
                </div>
                <div className={css(style.body)}>
                    <div>
                        <span>Best price for selecter products</span>
                        <h1></h1>
                        <div>
                            <h4>Total</h4>
                            <h3>160$</h3>
                        </div>
                        {token === null ? (<NavLink
                            to="/buy/info">
                            <button>
                                Continue
                            </button>
                        </NavLink>) : (<NavLink
                            to="/buy/address">
                            <button>
                                Continue
                            </button>
                        </NavLink>)}

                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    content: {
        height: "100vh",
        fontFamily: "Poppins",
    },
    header: {
        padding: "29px 0 0 109px",
        display: 'flex',
        ':nth-child(1n) > p': {
            width: 1,
            height: 29,
            backgroundColor: 'rgb(169, 169, 169)',
            border: 'solid 1px rgb(85, 85, 85)',
        }
    },
    home: {
        display: 'flex',
        ':nth-child(1n) > a': {
            textDecoration:"none",
            ':nth-child(1n) > span': {
                width: 49,
                height: 13,
                fontSize: 16,
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontStretch: 'normal',
                lineHeight: 'normal',
                letterSpacing: 0.6,
                textAlign: 'center',
                color: 'rgb(68, 68, 68)',
                ':hover': {
                    cursor: "pointer"
                }
            }
        },
        ':nth-child(1n) > div': {
            padding: "0 32px 0 62px",
            ':nth-child(1n) > div': {
                width: 21,
                height: 21,
                // font-family: Poppins;
                fontSize: 14,
                fontWeight: 500,
                letterSpacing: 0.6,
                textAlign: 'center',
                color: 'rgb(68, 68, 68)',
                borderRadius: "40px",
                position: "absolute",
                top: 24,
                marginLeft: 12,
                backgroundColor: 'rgb(109, 219, 162)',
            }
        }
    },
    notifyHeader: {
        display: 'flex',
        ':nth-child(1n) > div': {
            padding: "0 27px 0 23px",
            ':nth-child(1n) > div': {
                width: 9,
                height: 9,
                backgroundColor: 'rgb(255, 119, 109)',
                position: "absolute",
                top: 30,
                marginLeft: 7,
                borderRadius: 40
            }
        },
        ':nth-child(1n) > span': {
            marginLeft: 60,
            display: 'flex',
            position: 'absolute',
            ':nth-child(1n) > div': {
                pointerEvents: 'none',
                position: 'absolute',
                top: '1px',
                left: '25px',
            },
            ':nth-child(1n) > select': {
                width: 50,
                background: "none",
                border: 'none',
                appearance: 'none',
                height: 29,
                ":focus": {
                    outline: "none"
                },
                ':nth-child(1n) > option': {
                    background: "rgb(250, 250, 250)"
                }

            }
        }
    },
    body: {
        display: "flex",
        height: "100vh",
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "rgb(250, 250, 250)",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > div': {
                paddingTop: 35,
                display: "flex",
                width: "300px",
                justifyContent: "space-between",
                ':nth-child(1n) > h3': {
                    fontSize: 16,
                    color: 'rgb(109, 219, 162)',
                },
                ':nth-child(1n) > h4': {
                    fontSize: 16,
                },
            },
            ':nth-child(1n) > h1': {
                width: "300px",
                height: 1,
                margin: "26px 0",
                background: "#a9a9a9"
            },
            ':nth-child(1n) > span': {
                fontFamily: "Lato",
                fontSize: "18",
                fontWeight: "normal",
                lineHeight: "2.44",
                letterSpacing: "normal",
                color: "rgb(109, 219, 162)"
            },
            ':nth-child(1n) > a': {
                ':nth-child(1n) > button': {
                    cursor: "pointer",
                    marginTop: 63,
                    border: "none",
                    width: 295,
                    backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
                    borderRadius: 7,
                    height: 53,
                    color: "rgb(255, 255, 255)",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: 2.44,
                    letterSpacing: 0.4,
                }
            }
        }
    },
    SubPadding: {
        paddingBottom: 24
    }
});

export default  connect(store => ({store: store}))(withRouter(LeftPanelCart))

