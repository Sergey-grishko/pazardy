import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import connect from "react-redux/es/connect/connect";
import * as CartList from "../../action/cart";
import Dropdown from "rc-dropdown";


class LeftPanel extends React.Component {
    constructor() {
        super()
        this.state = {
            CartArr:null,
            lag:"EU"
        }
    }

    async componentDidMount(){
        let res = await CartList.GetCartLoged()
        this.setState({
            CartArr:res.cart_items

        })
    }
    render() {
        let items = this.state.CartArr === null?null:(
            this.state.CartArr.map((value,index)=>{
               return(
                   <li key={index}>
                       <img src={require('../sources/textBlock.png')} alt="arrow"/>
                       <div>Iteamx</div>
                       <div>x{value.quantity}</div>
                       <p>20$</p>
                   </li>
               )
            })
        )
        return (
            <div className={css(style.content)}>
                <div className={css(style.header)}>
                    <div className={css(style.home)}>
                        <NavLink
                            to="/">
                            <span>Home</span>
                        </NavLink>
                        <div onClick={()=>this.props.history.push('/buy')}>
                            <img src={require('../sources/shopping-cart.png')} alt="shopping"/>
                            {this.props.store.cart.ItemsCart !== 0 ?( <div>{this.props.store.cart.ItemsCart}</div>):null}
                        </div>
                    </div>
                    <p></p>
                    <span className={css(style.notifyHeader)}>
                                <div onClick={() => this.setState({modalNotify: !this.state.modalNotify})}>
                                    <img src={require('../sources/bell.png')} alt="bell"/>
                                    <div> </div>
                                </div>
                                    <Dropdown
                                        trigger={['click']}
                                        overlay={<ul  className={` ${css(style.sortBy)}`}>
                                            <li onClick={()=>this.setState({lag:"EU"})}>EU</li>
                                            <li  onClick={()=>this.setState({lag:"RU"})}>RU</li>
                                            <li  onClick={()=>this.setState({lag:"AR"})}>AR</li>
                                        </ul>}
                                        animation="slide-up"
                                    >
                                    <div className={css(style.sublist)}>
                                        <button className={css(style.buttonSubtitle)}>{this.state.lag}<img
                                            src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                                    </div>
                                    </Dropdown>
                            </span>
                </div>
                <div className={css(style.body)}>
                    <span>
                        <ul>
                            {items}
                        </ul>
                        <div>...</div>
                    </span>
                    <div>
                        <div className={css(style.SubPadding)}>
                            <h4>Subtotal</h4>
                            <h3>150$</h3>
                        </div>
                        <div>
                            <h4>Shipping</h4>
                            <h3>10$</h3>
                        </div>
                        <h1></h1>
                        <div>
                            <h4>Total</h4>
                            <h3>160$</h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    content: {
        height: "100vh",
        fontFamily: "Poppins",
    },
    header: {
        padding: "29px 0 0 109px",
        display: 'flex',
        ':nth-child(1n) > p': {
            width: 1,
            height: 29,
            backgroundColor: 'rgb(169, 169, 169)',
            border: 'solid 1px rgb(85, 85, 85)',
        }
    },
    home: {
        display: 'flex',
        ':nth-child(1n) > a': {
            textDecoration:"none",
            ':nth-child(1n) > span': {
                width: 49,
                height: 13,
                // font-family: Poppins;
                fontSize: 16,
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontStretch: 'normal',
                lineHeight: 'normal',
                letterSpacing: 0.6,
                textAlign: 'center',
                color: 'rgb(68, 68, 68)',
                ':hover': {
                    cursor: "pointer"
                }
            }
        },
        ':nth-child(1n) > div': {
            userSelect:'none',
            cursor:"pointer",
            padding: "0 32px 0 62px",
            ':nth-child(1n) > div': {
                userSelect:"none",
                width: 21,
                height: 21,
                // font-family: Poppins;
                fontSize: 14,
                fontWeight: 500,
                letterSpacing: 0.6,
                textAlign: 'center',
                color: 'rgb(68, 68, 68)',
                borderRadius: "40px",
                position: "absolute",
                top: 24,
                marginLeft: 12,
                backgroundColor: 'rgb(109, 219, 162)',
            }
        }
    },
    buttonSubtitle: {
        marginTop:7,
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'center',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    sublist: {
        marginTop:-13,
        marginLeft:20,
        width: "80%",
        display: "flex",
        flexDirection: "column"
    },
    sortBy: {
        listStyle: "none",
        margin: "-5px 0 0 -10px",
        background: "#fff",
        padding: "10px 0",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        ':nth-child(1n) > li': {
            cursor: "pointer",
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ":hover": {
                backgroundColor: "#ebebeb"
            },
        }
    },
    notifyHeader: {
        display: 'flex',
        alignItems:"flex-start",
        ':nth-child(1n) > div': {
            cursor: "pointer",
            ':nth-child(1n) > div': {
                width: 9,
                height: 9,
                backgroundColor: 'rgb(255, 119, 109)',
                position: "absolute",
                top: 30,
                marginLeft: 7,
                borderRadius: 40
            }
        },
        ':nth-child(1n) > div:first-child': {
            margin: "0 2px 0 23px",
        },
    },
    body: {
        marginTop: "71px",
        backgroundColor: "rgb(250, 250, 250)",
        ':nth-child(1n) > span': {
            ':nth-child(1n) > ul': {
                listStyle: "none",
                ':nth-child(1n) > li': {
                    display: "flex",
                    alignItems: "center",
                    justifyContent: 'space-between',
                    width: "300px",
                    marginBottom: 27,
                    height: 107,
                    fontSize: 14,
                    ':nth-child(1n) > p': {
                        color: 'rgb(109, 219, 162)',
                        margin: 0
                    },
                },

            },
            ':nth-child(1n) > div': {
                paddingLeft: "0 82px",
                width: "80%",
                paddingBottom: 26,
                textAlign: "center",
                letterSpacing: 3,
                fontSize: 20,
                color: "#a9a9a9",
                ":hover": {
                    cursor: "pointer"
                }

            }
        },
        ':nth-child(1n) > div': {
            ':nth-child(1n) > div': {
                display: "flex",
                width: "300px",
                justifyContent: "space-between",
                ':nth-child(1n) > h3': {
                    fontSize: 16,
                    color: 'rgb(109, 219, 162)',
                },
                ':nth-child(1n) > h4': {
                    fontSize: 16,
                },
            },
            ':nth-child(1n) > h1': {
                width: "300px",
                height: 1,
                margin: "26px 0",
                background: "#a9a9a9"
            },
            paddingLeft: 39
        }
    },
    SubPadding: {
        paddingBottom: 24
    }
});


export default  connect(store => ({store: store}))(withRouter(LeftPanel))
