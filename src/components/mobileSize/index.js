import React, {Component} from 'react'
import {Switch, Route, withRouter, Redirect} from 'react-router-dom'
import {css, StyleSheet} from 'aphrodite';

import {MobileView, isAndroid, isIOS} from "react-device-detect";


class Mobile extends Component {

    render() {
        return (
            <div className={` ${css(style.content)}`}>
                <div>
                    <div>
                        Get <br/>
                        The <br/>
                        Pazardy
                    </div>
                    {/*Change the text here*/}
                    <span>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est facere libero molestiae optio?
                    </span>
                    {/*Change the text here*/}
                    <div>
                        {/*Change the link here*/}
                        <a href="#">
                            <img src={require('../sources/app-store-copy.png')} alt="app"/>
                        </a>
                        <a href="#">
                            <img src={require('../sources/google-play-copy.png')} alt="app"/>
                        </a>
                        {/*Change the link here*/}
                    </div>
                </div>
            </div>
        )
    }

}

const style = StyleSheet.create({
    content: {
        margin: "auto",
        ':nth-child(1n) > div': {
            width: 276,
            ':nth-child(1n) > div:first-child': {
                fontSize: 50,
                fontFamily: "Lato",
            },
            ':nth-child(1n) > span': {
                fontSize: 18,
                margin: "10px 0",
                textAlign: "left",
                fontFamily: "Poppins",
            },
            ':nth-child(1n) > div:last-child': {
                display: "flex",
                ':nth-child(1n) > a:last-child': {
                    marginLeft: 20
                }
            },
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            alignItems: 'flex-start',
            justifyContent: 'center'
        }
    }
});


export default withRouter(Mobile)
