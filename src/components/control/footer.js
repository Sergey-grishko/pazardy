import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import bg from "../sources/bgfooter.png"


const bgImg = {
    height: 649,
    background: `url(${bg}) no-repeat`,
    backgroundSize: "cover",
}
const bgHome = {
    height: 649,
    background: `url(${bg}) no-repeat`,
    backgroundSize: "cover",
    backgroundColor: "#fafafa"
}

class Footer extends React.Component {
    constructor() {
        super()
        this.state = {}
    }

    render() {
        return (
            <div style={this.props.history.location.pathname === "/"?bgHome:bgImg}>

                <div className={`container ${css(style.footer)}`}>
                    <div>
                        <div>
                            <ul>
                                <li>PRIVACY POLICY</li>
                                <li>TERMS & CONDITION</li>
                                <li>ABOUT</li>
                                <li>DELIVERY</li>
                                <li>SECURE PAYMENT</li>
                            </ul>
                            <ul>
                                <li>CUSTOMER SUPPORT</li>
                                <li>SHIPPING INFO</li>
                                <li>CARIERS</li>
                                <li>CONTACT</li>
                            </ul>
                        </div>
                        <div>
                            <p>SUBSCRIBE</p>
                            <div className={css(style.submit)}>
                                <input placeholder="e-mail"/>
                                <button>Submit</button>
                            </div>
                            <div className={css(style.social)}>
                                <span><i className="fab fa-facebook-f"/></span>
                                <span><i className="fab fa-telegram-plane"/></span>
                                <span><i className="fab fa-youtube"/></span>
                            </div>
                        </div>
                    </div>
                    <div className={css(style.download)}>
                        <div>DOWNLOAD THE PAZARDY</div>
                        <div>
                            <img src={require('../sources/app-store-copy.png')} alt="app"/>
                            <img src={require('../sources/google-play-copy.png')} alt="google"/>
                        </div>
                    </div>
                    <div>
                        Copyright 2018, All rights reserved
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    footer: {
        ':nth-child(1n) > div:first-child': {
            display: "flex",
            paddingTop: 180,
            color: "#fff",
            justifyContent: 'space-between',
            ':nth-child(1n) > div': {
                display: "flex",
                ':nth-child(1n) > ul:first-child': {
                    paddingRight: 80,
                    paddingLeft: 0
                },
                ':nth-child(1n) > ul': {
                    listStyle: "none",
                    ':nth-child(1n) > li': {
                        fontFamily: "Poppins",
                        fontWeight: 300,
                        cursor: "pointer",
                        margin: "20px 0"
                    }
                }
            },
            ':nth-child(1n) > div:last-child': {
                marginTop: 20,
                flexDirection: "column",
                ':nth-child(1n) > div:first-child': {
                    ':nth-child(1n) >p': {
                        fontFamily: "Lato",
                        fontSize: 16,
                        color: "#fffefe"
                    }
                }
            },
        },
        ':nth-child(1n) > div:last-child': {
            color:"#fffefe",
            marginTop:"5%",
            fontWeight:300,
            fontSize:14,
            fontFamily: "Lato",
        }
    },
    submit: {
        display: "flex",
        height: 53,
        ':nth-child(1n) > input': {
            background: "transparent",
            border: "2px solid #3aab75",
            borderRight: "none",
            borderBottomLeftRadius: 5,
            marginRight: -3,
            borderTopLeftRadius: 5,
            paddingLeft: 20,
            widths: 200,
            color: "#fff",
            ':focus': {
                outline: "none"
            }
        },
        ':nth-child(1n) > button': {
            backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
            border: "2px solid #3aab75",
            borderRadius: 5,
            color: "#fff",
            fontFamily: "Poppins",
            padding: "0 30px"
        }
    },
    social: {
        marginTop: 20,
        ':nth-child(1n) > span:first-child': {
            paddingLeft: 0
        },
        ':nth-child(1n) > span': {
            padding: "0 22.5px",

            ':nth-child(1n) > i': {
                fontSize: 40,
                cursor: "pointer",
            }
        }
    },
    download: {
        color: "#fff",
        display: 'flex',
        flexDirection: "column",
        alignItems: "center",
        ':nth-child(1n) > div:first-child': {
            paddingBottom:36
        },
        ':nth-child(1n) > div:last-child': {
            ':nth-child(1n) > img:first-child': {
                margin:"0 12px 0 0",
                cursor:"pointer"
            },
            ':nth-child(1n) > img:last-child': {
                margin:"0 0 0 12px",
                cursor:"pointer"
            }
        }
    }
});

export default (withRouter(Footer))
