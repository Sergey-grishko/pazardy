import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import ModalL from "./modal"
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import connect from "react-redux/es/connect/connect";
import * as cart from "../../action/cart";


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        padding: 0,
        marginRight: '-50%',
        borderRadius: 0,
        border: "none",
        width: 585,
        height: 500,
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "none",
    }
};

class Fixheader extends React.Component {
    constructor() {
        super()
        this.state = {
            showModal: false,
            lag:"EU",
            modalNotify: true,
        }
        this.handleOpenModal = this.handleOpenModal.bind(this);
    }

    handleOpenModal() {
        this.setState({showModal: true});
    }

    render() {
        return (
            <div className={`${css(style.content)}`}>
                <div className={` container`}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/LogoHeade.png')} alt="Logo"/>
                    </NavLink>
                    <div>
                        <ul>
                            <NavLink
                                to="/">
                                <li>Home</li>
                            </NavLink>
                            <li onClick={()=>this.handleOpenModal()}>About Us</li>
                            <NavLink
                                to="/account">
                                <li>My Account</li>
                            </NavLink>
                            <li>Orders</li>
                        </ul>
                        <div className={css(style.control)}>
                            <div>
                                <div className={css(style.Shop)}>
                                    <NavLink
                                        to="/buy">
                                        <img src={require('../sources/shopping-cart--3--copy.png')} alt="shopping"/>
                                        {this.props.store.cart.ItemsCart !== 0 ?( <div>{this.props.store.cart.ItemsCart}</div>):null}
                                    </NavLink>
                                </div>
                            </div>
                            <p></p>
                            <span className={css(style.notifyHeader)}>
                                <div onClick={() => this.setState({modalNotify: !this.state.modalNotify})}>
                                    <img src={require('../sources/bell--1-.png')} alt="bell"/>
                                    <div> </div>
                                </div>
                                    <Dropdown
                                        trigger={['click']}
                                        overlay={<ul  className={` ${css(style.sortBy)}`}>
                                            <li onClick={()=>this.setState({lag:"EU"})}>EU</li>
                                            <li  onClick={()=>this.setState({lag:"RU"})}>RU</li>
                                            <li  onClick={()=>this.setState({lag:"EU"})}>EU</li>
                                        </ul>}
                                        animation="slide-up"
                                    >
                                    <div className={css(style.sublist)}>
                                        <button className={css(style.buttonSubtitle)}>{this.state.lag}<img
                                            src={require('../sources/down-arrow--2--copy-26.png')} alt="arrow"/></button>
                                    </div>
                                    </Dropdown>
                            </span>
                            <div style={this.state.modalNotify === true ? {display: "none"} : null}
                                 className={css(style.notifyModal)}>
                                <p>new</p>
                                <div>
                                    Here will be the right thing to do when it will be API.<br/>Since it is not known
                                    how data is to be obtained
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ModalL
                    open={this.state.showModal}
                    handleCloseModal={() => this.setState({showModal: false})}
                />
            </div>
        )
    }
}

const style = StyleSheet.create({
    notifyModal: {
        position: "fixed",
        zIndex: 100,
        top: 98,
        width: 511,
        '@media (max-width: 1199px)': {
            left: 'calc(50% - 255px)',
        },
        height: 354,
        left: '50%',
        background: "#fff",
        padding: "0 75px",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        ':nth-child(1n) > p': {
            height: 50,
            display: 'flex',
            margin: 0,
            alignItems: "center",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 16,
            fontWeight: 400,
        }
    },
    content: {
        position: "fixed",
        zIndex: 100,
        width: "100%",
        top: 0,
        ':nth-child(1n) > div:first-child': {
            display: "flex",
            alignItems: "center",
            height: "100%",
            justifyContent: 'space-between',
            ':nth-child(1n) > div': {
                display: "flex",
                alignItems: "center",
                ':nth-child(1n) > ul': {
                    '@media (max-width: 1199px)': {
                        fontSize: 13
                    },
                    listStyle: "none",
                    margin: "0",
                    color: "#fff",
                    display: "flex",
                    ':nth-child(1n) > li': {
                        fontFamily: "Poppins",
                        fontWeight: "300",
                        margin: "0 10px",
                        cursor: "pointer"
                    },
                    ':nth-child(1n) > a': {
                        color: "#fff",
                        textDecoration: 'none',
                        ':nth-child(1n) > li': {
                            fontFamily: "Poppins",
                            fontWeight: "300",
                            margin: "0 10px",
                            cursor: "pointer"
                        }
                    }
                }
            }
        },
        backgroundColor: "#252525",
        height: 89,
    },
    Shop: {
        padding: "0 32px 0 0",
        ':nth-child(1n) > a': {
            ':nth-child(1n) > div': {
                width: 21,
                height: 21,
                fontFamily: "Poppins",
                fontSize: 14,
                fontWeight: 500,
                letterSpacing: 0.6,
                textAlign: 'center',
                color: '#fff',
                borderRadius: "40px",
                position: "absolute",
                top: 27,
                marginLeft: 15,
                backgroundColor: 'rgb(109, 219, 162)',
            }
        }
    },
    control: {
        marginRight: 25,
        padding: "0 0 0 46px",
        display: 'flex',
        alignItems: "center",
        ':nth-child(1n) > p': {
            marginTop: 18,
            width: 1,
            height: 29,
            backgroundColor: 'rgb(169, 169, 169)',
            border: '1px solid #555555',
        }
    },
    notifyHeader: {
        display: 'flex',
        alignItems:"center",
        ':nth-child(1n) > div': {
            cursor: "pointer",
            ':nth-child(1n) > div': {
                width: 9,
                height: 9,
                backgroundColor: 'rgb(255, 119, 109)',
                position: "absolute",
                top: 35,
                marginLeft: 7,
                borderRadius: 40
            }
        },
        ':nth-child(1n) > div:first-child': {
            margin: "0 27px 0 23px",
        },
    },
    sortBy: {
        listStyle: "none",
        margin: "-5px 0 0 -10px",
        background: "#fff",
        padding: "10px 0",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        ':nth-child(1n) > li': {
            cursor: "pointer",
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ":hover": {
                backgroundColor: "#ebebeb"
            },
        }
    },
    listSubtitle: {
        padding: 0,
        listStyle: 'none',
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        width: "60%",
        background: "#fff"
    },
    sublist: {
        width: "80%",
        display: "flex",
        flexDirection: "column"
    },
    buttonSubtitle: {
        marginTop:7,
        padding: "0",
        color: "#fff",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'center',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    }
});

export default  connect(store => ({store: store}))(withRouter(Fixheader))
