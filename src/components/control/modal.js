import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Modal from 'react-modal';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        padding: 0,
        marginRight: '-50%',
        borderRadius: 0,
        border: "none",
        width: 585,
        height: 500,
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "none",
    }
};

class ModalL extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            modalNotify: true
        }
    }




    render() {
        return (
            <div className={`${css(style.content)}`}>
                <div className={` container`}>
                    <Modal
                        style={customStyles}
                        isOpen={this.props.open}
                        contentLabel="Minimal Modal Example"
                    >
                        <div className={css(style.TitleReg)}>
                            <span>Sign up</span>
                            <span>Sign in</span>
                        </div>
                        <div className={css(style.Reg)}>
                            <div>
                                <input placeholder="email@mail.com"/>
                                <input placeholder="password"/>
                                <button>create account</button>
                            </div>
                            <span>
                                <p></p>
                                <span>or</span>
                                <p></p>
                            </span>
                            <div>
                                <button className={css(style.connectGo)}>continue with <div>
                                    <i className="fab fa-google-plus-g"/></div>
                                </button>
                                <button className={css(style.connectF)}>continue with <div>
                                    <i className="fab fa-facebook-f"/></div></button>
                                <button className={css(style.connectT)}>continue with <div>
                                    <i className="fab fa-twitter"/></div></button>
                            </div>
                        </div>
                    </Modal>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    Reg: {
        display: "flex",
        alignItems: "center",
        margin: "0 50px",
        ':nth-child(1n) > div:first-child': {
            display: "flex",
            flexDirection: "column",
            alignItems: 'center',
            width: "400px",
            ':nth-child(1n) > input': {
                width: 170,
                border: 'none',
                marginBottom: "25px",
                borderBottom: "1px solid #555",
                ":focus": {
                    outline: "none"
                }
            },
            ':nth-child(1n) > button': {
                width: 140,
                height: 37,
                borderRadius: 7,
                border: "1px solid #6ddba2",
                backgroundColor: "transparent",
                color: "#a9a9a9",
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 300,
                cursor: "pointer",
                transition: "0.9s",
                ":hover": {
                    transition: "0.9s",
                    color: "#fff",
                    border: "1px solid #6ddba2",
                    backgroundColor: "#6ddba2",
                },
                ":focus": {
                    outline: "none"
                }
            }
        },
        ':nth-child(1n) > span': {
            display: "flex",
            flexDirection: "column",
            alignItems: 'center',
            ':nth-child(1n) > span': {
                color: "#000000",
                fontFamily: "Poppins",
                fontSize: 18,
                margin: "20px 0",
                fontWeight: 300,
            },
            ':nth-child(1n) > p': {
                margin: 0,
                height: 94,
                width: 1,
                background: '#555'
            }
        },
        ':nth-child(1n) > div:last-child': {
            width: 400,
            display: "flex",
            flexDirection: "column",
            alignItems: 'center',
            ':nth-child(1n) > button': {
                width: 192,
                margin: "10px 0",
                height: 37,
                borderRadius: 7,
                display: "flex",
                justifyContent: "space-evenly",
                alignItems: 'center',
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 300,
                cursor: "pointer",
                transition: "0.9s",
                ":focus": {
                    outline: "none"
                },
                ':nth-child(1n) > div': {
                    display: "flex",
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: 'center',
                    width: 26,
                    height: 26,
                    borderRadius: 40
                }
            }
        }
    },
    connectGo: {
        backgroundColor: "transparent",
        color: "#a9a9a9",
        border: "1px solid #e62b33",
        ':nth-child(1n) > div': {
            background: "#e62b33"
        },
        ":hover": {
            transition: "0.9s",
            color: "#fff",
            border: "1px solid #e62b33",
            backgroundColor: "#e62b33",
        },
    },
    connectT: {
        backgroundColor: "transparent",
        color: "#a9a9a9",
        border: "1px solid #43bdef",
        ':nth-child(1n) > div': {
            background: "#43bdef"
        },
        ":hover": {
            transition: "0.9s",
            color: "#fff",
            border: "1px solid #43bdef",
            backgroundColor: "#43bdef",
        },
    },
    connectF: {
        backgroundColor: "transparent",
        color: "#a9a9a9",
        border: "1px solid #6081c4",
        ':nth-child(1n) > div': {
            background: "#6081c4"
        },
        ":hover": {
            transition: "0.9s",
            color: "#fff",
            border: "1px solid #6081c4",
            backgroundColor: "#6081c4",
        },
    },
    TitleReg: {
        display: "flex",
        justifyContent: "center",
        margin: "40px 0 70px 0",
        ':nth-child(1n) > span': {
            width: "100%",
            color: "#000000",
            fontFamily: "Poppins",
            fontSize: 20,
            fontWeight: 300,
            lineWeight: "33.31px"
        },
        ':nth-child(1n) > span:first-child': {
            padding: "10px 50px 10px 0",
            textAlign: "right"
        },
        ':nth-child(1n) > span:last-child': {
            padding: "10px 0 10px 50px",
            borderBottom: "1px solid #555",
            borderLeft: "1px solid #555"
        }
    },
});

export default (withRouter(ModalL))
