import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';

import InfiniteScroll from "react-infinite-scroll-component";


import FixHeader from '../control/Fixheader'
import Header from '../control/header'
import Footer from '../control/footer'
import VisibilitySensor from "react-visibility-sensor";
import StarRatings from 'react-star-ratings';
import Modal from 'react-modal';
import * as Categorie from "../../action/categorie";
import * as Items from "../../action/items";
import {toast, ToastContainer} from "react-toastify";
import {store} from "../../index";
import {CART} from "../../reducers/const";
import connect from "react-redux/es/connect/connect";
import _ from "lodash"
import * as Cart from "../../action/cart";

const Scroll = {
    overflow: "hidden",
    display: 'flex',
    flexWrap: "wrap"
}

const customStyles = {
    content: {
        marginTop: 520,
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        padding: 0,
        marginRight: '-50%',
        borderRadius: 0,
        border: "none",
        width: "100%",
        // height: "100%",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: "absolute",
        top: '0',
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "none",
    }
};

class OneItems extends React.Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps, this.props)

    }

    render() {
        const {item, addFavorite, idFavorite, onChange, AddCart} = this.props;
        if (item.count === undefined) {
            item.count = 1
        }
        return (
            <div className={`col-xl-3 col-md-6 ${css(style.Items)}`}>
                <div className={` ${css(style.slideBox)}`}>
                    <div>
                                    <span><StarRatings
                                        starSpacing="1px"
                                        starDimension="15px"
                                        rating={item.rating}
                                        starRatedColor="#ff776d"
                                        numberOfStars={5}
                                        name='rating'
                                    /></span><br/>
                        <h5>
                            <img
                                src={item.item.normal_image}
                                alt="pngpix"/>
                        </h5>
                        <div>
                            <p>${item.price}</p>
                            <div>
                                <div onClick={() => onChange("-")}>-</div>
                                <div>{item.count}</div>
                                <div onClick={() => onChange("+")}>+</div>
                            </div>
                        </div>
                    </div>
                    <div className={css(style.selectSlider)}>
                                    <span className={css(style.selectStyle)}>
                                       <ul className="col-xl-3 col-md-12 top-level-menu">
                                            <li>
                                                <a className={css(style.sortByTitle)}> Store Name<img
                                                    src={require('../sources/down-arrow.png')} alt="arrow"/></a>
                                                <ul className={`second-level-menu ${css(style.sortBy)}`}>
                                                    <li>
                                                        <a>
                                                        Other available store<br/>
                                                        in your aresa</a>
                                                    </li>
                                                    <li><a href="#">Store Name <span>10$</span></a></li>
                                                    <li><a href="#">Store Name <span>10$</span></a></li>
                                                    <li><a href="#">Store Name <span>10$</span></a></li>
                                                    <li><a href="#">Store Name <span>10$</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </span>
                        <button onClick={() => AddCart(item.count)}>Add</button>
                    </div>
                </div>
                <div className={css(style.NameItems)}>
                    <div>
                        <NavLink
                            to={`/items/${item.item.id}`}>
                            <p>{item.item.english_name}</p>
                        </NavLink>
                        <h5>{item.item.english_description}</h5>
                    </div>
                    <span>
                                    <StarRatings
                                        changeRating={() => addFavorite()}
                                        starSpacing="1px"
                                        starDimension="20px"
                                        rating={1}
                                        starRatedColor={idFavorite ? "#6ddba2" : "#cbd3e3"}
                                        numberOfStars={1}
                                        name='rating'
                                    />
                                </span>
                </div>
            </div>
        )
    }
}

class AllCategories extends React.Component {
    constructor() {
        super()
        this.state = {
            isVisible: true,
            showModal: false,
            SelectNumber: 0,
            categories: null,
            items: [],
            name: '',
            page: 1,
            path: null,
            category_id: null,
            sub_category_id: null,
            id: null,
            idFavorite: []
        }
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.fetchMoreData = this.fetchMoreData.bind(this);
        this.AddtoFavorite = this.AddtoFavorite.bind(this);
    }

    static propTypes = {}

    static defaultProps = {}

    async componentDidMount() {
        window.scrollTo(0, 0);
        let id = this.props.match.params.id;
        let path = this.props.match.path;
        let category_id = this.props.match.params.category_id;
        let sub_category_id = this.props.match.params.sub_category_id;
        let categoriesName = await Categorie.GetCategorieName();
        let categories = await Categorie.GetCategorie();

        let index = 0;
        for (let key in categories) {
            categoriesName.Store_cateogires[index].cateogires = categories[key];
            index++
        }
        let Favorite;
        let idFavorite = this.state.idFavorite;
        let accessToken = localStorage.getItem('pazardy-token');
        accessToken !== null ? (
            Favorite = await Items.AllFavorited()) : null
        accessToken !== null ? (
            Favorite.favorited_items.map(value => {
                let id = {
                    id: value.id
                }
                idFavorite.push(id)
            })) : null
        let items = [];
        if (path === "/Subcategories/:category_id&:sub_category_id") {
            items = await Items.GetItemsSubCategories(category_id, sub_category_id, 1, this.props.store.location.code.latitude, this.props.store.location.code.longitude)
        } else {
            items = await Items.GetItemsCategories(id, 1, this.props.store.location.code.latitude, this.props.store.location.code.longitude)
        }
        console.log(categoriesName.Store_cateogires)
        if (items.length === 0) {
            this.setState({
                idFavorite,
                categories: categoriesName.Store_cateogires,
                items: 0,
                // name:items[0].sub_category.category.english_name,
                path,
                id,
                category_id,
                sub_category_id,
            })
        } else {
            this.setState({
                idFavorite,
                categories: categoriesName.Store_cateogires,
                items,
                // name:items[0].sub_category.category.english_name,
                path,
                id,
                category_id,
                sub_category_id,
            })
        }

    }


    AddCart = async (store_item_id, quantity) => {
        if (localStorage.getItem("PazardyCartId") !== null) {
            let date = {
                cart_item: {
                    store_item_id: store_item_id,
                    quantity: quantity
                }
            }
            let res = await Cart.AddCartLoged(localStorage.getItem("PazardyCartId"), date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        } else {
            let cartId = await Cart.CartInitialize()
            localStorage.setItem("PazardyCartId", cartId.cart.cart_id)
            let date = {
                cart_item: {
                    store_item_id: store_item_id,
                    quantity: quantity
                }
            }
            let res = await Cart.AddCartLoged(cartId.cart.cart_id, date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        }
    }

    handleOpenModal() {
        this.setState({showModal: true});
    }

    handleCloseModal() {
        this.setState({showModal: false});
    }

    onChangeIsVisible(isVisible) {
        this.setState({
            isVisible
        })
    }


    onChangeCount = (type, index) => {
        let items = this.state.items;
        if (items[index].count === undefined) {
            if (type === '+') items[index].count = 2
        } else {
            if (type === '+') {
                items[index].count++
            }
            if (items[index].count !== 1) {
                if (type === '-') {
                    items[index].count--
                }
            }
        }
        this.setState({items: items})
    }

    async AddtoFavorite(id) {
        var idItems = id
        try {
            let response = await Items.AddFavorited(idItems);
            let idFavorite = this.state.idFavorite;
            let id = {
                id: idItems
            }
            idFavorite.some((val) => val.id === idItems) ? (idFavorite.map((value, index) => {
                if (idItems === value.id) {
                    idFavorite.splice(index, 1)
                }
            })) : (idFavorite.push(id))
            this.setState({
                idFavorite
            });
            console.log(this.state.idFavorite)
            toast.success(response.message)
        } catch (error) {
            toast.error(error.message);
        }

    }

    async fetchMoreData() {
        let items;
        if (this.state.path === "/Subcategories/:category_id&:sub_category_id") {
            items = await Items.GetItemsSubCategories(this.state.category_id, (this.state.sub_category_id, this.state.page + 1), this.props.store.location.code.latitude, this.props.store.location.code.longitude)
        } else {
            items = await Items.GetItemsCategories(this.state.id, this.state.page + 1, this.props.store.location.code.latitude, this.props.store.location.code.longitude)
        }
        let itemsOld = this.state.items
        this.setState({
            items: itemsOld.concat(items),
            // name:items[0].sub_category.category.english_name
            page: this.state.page + 1
        })
    }

    render() {
        let Categories = this.state.categories !== null ? (this.state.categories.map((value, index) => {
            console.log(value.cateogires)
            let subtitle = value.cateogires !== undefined ? value.cateogires.map((value, index) => {
                let list = value.sub_categories.map((v, index) => {
                    return (
                        <li onClick={()=> {this.props.history.push(`/Subcategories/` + value.id + `&` + v.id); window.location.reload()}} key={index}>{v.english_name}</li>
                    )
                })
                return (
                    <Dropdown
                        key={index}
                        // trigger={['hover']}
                        overlay={<ul className={css(style.listSubtitle)}>{list}</ul>}
                        // animation="slide-up"
                    >
                        <div className={css(style.sublist)}>
                            <button onClick={()=> {this.props.history.push(`/categories/` + value.id); window.location.reload()}} className={css(style.buttonSubtitle)}>{value.english_name}<img
                                src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                        </div>
                    </Dropdown>
                )
            }) : null
            return (
                <div className="col-2">
                    <p className={css(style.titleSublist)}>{value.name}</p>
                    {subtitle}
                </div>
            )
        })) : null
        return (
            <div>
                <VisibilitySensor
                    onChange={this.onChangeIsVisible.bind(this)}
                    partialVisibility={true}
                >
                    <Header/>
                </VisibilitySensor>
                {!this.state.isVisible && <FixHeader/>}
                <div className={`container ${css(style.categoriesItems)}`}>
                    <Modal
                        style={customStyles}
                        isOpen={this.state.showModal}
                        overlayClassName="container ReactModal__Overlay"
                        onRequestClose={this.handleCloseModal}
                    >
                        <div className={` col-xs-12 ${css(style.selectCategories)}`}>
                            {Categories}
                        </div>
                    </Modal>
                    <p onClick={this.handleOpenModal}>All Categories <img src={require('../sources/icon-category.png')}
                                                                          alt="bell"/></p>
                    <span>
                        <span>{this.props.store.categories.name}</span>
                        <span>
                            <ul className="col-xl-3 col-md-12 top-level-menu">
                            <li>
                                <a className={css(style.sortByTitle)}> Sort by<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></a>
                                <ul className={`second-level-menu ${css(style.sortBy)}`}>
                                    <li><a href="#">lowest price</a></li>
                                    <li><a href="#">higgest price</a></li>
                                    <li><a href="#">most popular</a></li>
                                    <li><a href="#">bigest saving</a></li>
                                </ul>
                            </li>
                        </ul>
                        </span>
                    </span>
                    <div style={this.state.showModal === true ? {opacity: "0.5"} : {opacity: "1"}}>
                        {this.state.items !== 0 ? (<InfiniteScroll
                            style={Scroll}
                            dataLength={this.state.items === null ? 0 : this.state.items.length}
                            next={this.fetchMoreData}
                            hasMore={true}
                            loader={<div style={{marginTop: 40}} className="loader"></div>}
                        >
                            {this.state.items !== 0 ? (this.state.items.map((value, index) => {
                                return (
                                    <OneItems
                                        item={value}
                                        addFavorite={() => this.AddtoFavorite(value.id)}
                                        idFavorite={this.state.idFavorite.some((val) => val.id === value.id)}
                                        onChange={(e) => this.onChangeCount(e, index)}
                                        AddCart={(e) => this.AddCart(value.item.id, e)}
                                    />
                                )
                            })) : null}
                        </InfiniteScroll>) : (<div className={css(style.emptyList)}>
                            No products found by your coordinates
                        </div>)}
                    </div>
                </div>
                <Footer/>
                <ToastContainer/>
            </div>
        )
    }
}

const style = StyleSheet.create({
    emptyList: {
        fontSize: 40,
        textAlign: 'center',
    },
    titleSublist: {
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 16,
        fontWeight: 400,
        textTransform: "uppercase"
    },
    buttonSubtitle: {
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'left',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    listSubtitle: {
        boxShadow: "0 5px 9px 1px RGBA(0,0,0,0.5)",
        padding: 0,
        listStyle: 'none',
        color: "#444444",
        borderRadius: 10,
        marginTop: -5,
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        width: "60%",
        background: "#fff",
        ':nth-child(1n) > li': {
            paddingLeft:5,
            borderRadius: 10,
            ":hover": {
                cursor:'pointer',
                background: "RGBA(69,69,69,0.2)"
            }
        }
    },
    sublist: {
        display: "flex",
        flexDirection: "column"
    },
    selectCategories: {
        flexWrap: "wrap",
        justifyContent: "center",
        margin: "20px 10px 0 10px",
        display: 'flex',
        ':nth-child(1n) > div': {
            marginBottom: 20,
        }
    },
    Items: {
        marginBottom: 50
    },
    SelectItems: {
        ':nth-child(1n) > li': {
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ':nth-child(1n) > a': {
                width: "100%",
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > span': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                    lineHeight: "40px"
                }
            }
        },
    },
    slideBox: {
        borderRadius: 7,
        border: "1px solid #e1e1e1",
        height: "300px",
        ':nth-child(1n) > div:first-child': {
            padding: "10px 10px 0 10px",
            borderBottom: "1px solid #e1e1e1",
            ':nth-child(1n) > h5': {
                display: "flex",
                justifyContent: 'center',
                ':nth-child(1n) > img': {
                    width: "120px",
                    height: "100%",
                    display: "flex",
                }
            },
            ':nth-child(1n) > div': {
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > p': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                },
                ':nth-child(1n) > div': {
                    display: "flex",
                    alignItems: "center",
                    height: "14px",
                    marginTop: 8,
                    ':nth-child(1n) > div': {
                        padding: "0 12.5px",
                        color: "#a9a9a9",
                        userSelect: "none",
                        fontFamily: "Poppins",
                        fontSize: 14,
                        fontWeight: 300,
                    },
                    ':nth-child(1n) > div:last-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                    ':nth-child(1n) > div:first-child': {
                        width: 22,
                        userSelect: "none",
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                }
            }
        }
    },
    selectSlider: {
        display: 'flex',
        alignItems: 'center',
        height: "calc(100% - 248px)",
        ':nth-child(1n) > span': {
            width: "80%",
            alignItems: 'center',
            height: "100%",
            ':nth-child(1n) > select': {
                color: "#000000",
                border: "none",
            }
        },

        ':nth-child(1n) > button': {
            outline: "none",
            cursor: "pointer",
            background: "none",
            border: "none",
            borderLeft: "1px solid #e1e1e1",
            color: "#6ddba2",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
        }

    },
    selectStyle: {
        width: "14%",
        display: 'flex',
        ':nth-child(1n) > div': {
            marginLeft: "130px",
            pointerEvents: 'none',
            position: 'absolute',
            '@media (max-width: 1199px)': {
                display: "none"
            }
        },
        ':nth-child(1n) > select': {
            width: "100%",
            paddingLeft: "15px",
            fontSize: 17,
            fontFamily: "Lato",
            color: "#a9a9a9",
            background: "none",
            border: 'none',
            borderLeft: "1px solid #555555",
            borderRight: "1px solid #555555",
            appearance: 'none',
            height: 29,
            ":focus": {
                outline: "none"
            },

        },
        ':nth-child(1n) > ul': {
            ':nth-child(1n) > li': {
                display: "flex",
                justifyContent: 'center',
                ':nth-child(1n) > ul': {
                    width: 192,
                    top: 50
                }
            }
        }
    },
    listEmpty: {
        color: 'red'
    },
    NameItems: {
        marginTop: 25,
        display: "flex",
        justifyContent: "space-between",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > a': {
                textDecoration: 'none',
                ':nth-child(1n) > p': {
                    margin: 0,
                    cursor: "pointer",
                    color: "#444444",
                    fontSize: 18,
                    fontFamily: "Poppins",
                    fontWeight: 400
                }
            },
            ':nth-child(1n) > h5': {
                color: "#a9a9a9",
                fontSize: 14,
                fontFamily: "Poppins",
                fontWeight: 300
            }
        }
    },
    categoriesItems: {
        ':nth-child(1n) > p': {
            cursor: "pointer",
            display: "flex",
            alignItems: "center",
            marginTop: 100,
            color: "#444",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
            marginBottom: 50,
            textTransform: "uppercase",
            ':nth-child(1n) > img': {
                paddingLeft: 10
            }

        },
        ':nth-child(1n) > span': {
            display: "flex",
            marginBottom: 40,
            justifyContent: "space-between",
            ':nth-child(1n) > span:first-child': {
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
            }
        },
        ':nth-child(1n) > div': {
            margin: 0,
            padding: 0
        }
    },
    sortBy: {
        width: 180,
        top: 30,
        ':nth-child(1n) > li': {
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ':nth-child(1n) > a': {
                width: "100%",
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > span': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                    lineHeight: "40px"
                }
            }
        },
        ':nth-child(1n) > li:first-child': {
            height: "100%",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 12,
            fontWeight: 400,
        }
    },
    sortByTitle: {
        justifyContent: "flex-end",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
    }
});

export default connect(store => ({store: store}))(withRouter(AllCategories))
