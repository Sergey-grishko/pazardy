import React, {Component} from 'react'
import {Switch, Route, withRouter, Redirect} from 'react-router-dom'
import {css, StyleSheet} from 'aphrodite';


import CustomerInfo from '../content/info'
import Address from '../content/address'
import paymentLogin from '../content/paymentLogin'
import paymentLogout from '../content/paymentLogout'
import Review from '../content/Review'
import Cart from '../content/cart'
import PanelItems from '../leftpanel/index'
import PanelCart from '../leftpanel/cart'

import {ToastContainer} from 'react-toastify';


class App extends Component {

    render() {
        return (
            <div>
                <div className={`col-12 row ${css(style.content)}`}>
                    <div className={`col-8 ${css(style.contentRight)}`}>
                        <Switch>
                            <Redirect exact from='/buy' to='/buy/cart'/>
                            <Route path="/buy/cart" component={Cart}/>
                            <Route path="/buy/info" component={CustomerInfo}/>
                            <Route path="/buy/address" component={Address}/>
                            <Route path="/buy/PLogin" component={paymentLogin}/>
                            <Route path="/buy/NoLogin" component={paymentLogout}/>
                            <Route path="/buy/review" component={Review}/>
                        </Switch>
                    </div>
                    <div className={`col-4 ${css(style.contentLeft)}`}>
                        {this.props.location.pathname === "/buy/cart" ? (
                                <PanelCart/>) :
                            (
                                <PanelItems/>
                            )}
                    </div>
                </div>
                <ToastContainer/>
            </div>
        )
    }

}

const style = StyleSheet.create({
    contentLeft: {
        height: "100%",
        backgroundColor: "rgb(250, 250, 250)",

    },
    contentRight: {
        marginRight: 20,
    },
    content: {
        margin: "0",
        padding: '0',
        height: 1081,
        ':nth-child(1n) > div': {
            margin: "0",
        },
        '@media (max-width: 760px)': {
            display: "none"
        }

    },
    text: {
        display: "none",
        '@media (max-width: 760px)': {
            display: "flex"
        }
    }
});


export default withRouter(App)
