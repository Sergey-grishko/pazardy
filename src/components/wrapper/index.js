import React, {Component} from 'react'
import {Switch, Route, withRouter, Redirect} from 'react-router-dom'
import {css, StyleSheet} from 'aphrodite';


import Buy from '../buy/index'
import Home from '../home/index'
import Mobile from '../mobileSize/index'
import AllCategories from '../categories/AllCategories'
import Items from '../items/index'
import News from '../news/index'
import Account from '../account/index'
import {store} from "../../index";
import * as Cart from "../../action/cart";
import {geolocated} from 'react-geolocated';
import {LOCATION} from "../../reducers/const";
import {CART} from "../../reducers/const";

import _ from 'lodash';


class App extends Component {

    async componentDidMount() {
        if (localStorage.getItem("PazardyCartId") !== null) {
            let res = await Cart.GetCartLoged()
            let cart = res.cart_items.length
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart
            });
        }

    }

    Location() {
        let data;
        if (this.props.coords !== null) {
            data = {
                latitude: this.props.coords.latitude,
                longitude: this.props.coords.longitude
            }
            store.dispatch({
                type: LOCATION.LOCATION,
                payload: data
            })
        }
    }

    render() {
        this.Location()
        return (
            <div>
                <div className={` ${css(style.content)}`}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/buy" component={Buy}/>
                        <Route path="/categories/:id" component={AllCategories}/>
                        <Route path="/Subcategories/:category_id&:sub_category_id" component={AllCategories}/>
                        <Route path="/items/:id" component={Items}/>
                        <Route path="/news/:id" component={News}/>
                        <Route path="/account" component={Account}/>
                    </Switch>
                </div>
                <div className={css(style.text)}>
                    <Mobile/>
                </div>
            </div>
        )
    }

}

const style = StyleSheet.create({
    content: {
        '@media (max-width: 760px)': {
            display: "none"
        }

    },
    text: {
        display: "none",
        '@media (max-width: 760px)': {
            display: "flex"
        }
    }
});


withRouter(App)

export default geolocated()(App);
