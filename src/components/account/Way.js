import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';


class Way extends React.Component {
    constructor() {
        super()
    }


    render() {
        return (
            <div className={css(style.way)}>
                <ul>
                    <li>
                        <div></div>
                        <span>497 Evergreen Rd. Roseville, <br/>
                                    <h5>CA 95673</h5>
                            </span>
                    </li>
                    <li>
                        <p></p>
                    </li>
                    <li>
                        <p></p>
                    </li>
                    <li>
                        <p></p>
                    </li>
                    <li>
                        <h5></h5>
                        <span>To 89 Palmspring Way Roseville,<br/>
                                    <h5>CA 39847</h5>
                            </span>
                    </li>

                </ul>
                <div>
                    <div>
                        <p>Duration</p>
                        <h5>30 min</h5>
                    </div>
                    <div>
                        <p>Total earnings</p>
                        <h5>87$</h5>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    way: {
        display: "flex",
        justifyContent: 'space-between',
        flexDirection: "column",
        width: 308,
        padding: 20,
        height: 262,
        backgroundColor: '#ffffff',
        ':nth-child(1n) > ul': {
            padding: 0,
            listStyle: "none",
            ':nth-child(1n) > li': {
                display: "flex",
                alignItems: 'flex-start',
                ':nth-child(1n) > div': {
                    width: 13,
                    height: 13,
                    borderRadius: 40,
                    backgroundColor: '#6ddba2',
                },
                ':nth-child(1n) > p': {
                    width: 6,
                    marginLeft: 3,
                    borderRadius: 40,
                    height: 6,
                    backgroundColor: '#6ddba2',
                },
                ':nth-child(1n) > h5': {
                    width: 13,
                    height: 13,
                    borderRadius: 40,
                    backgroundColor: '#ff776d',
                },
                ':nth-child(1n) > span': {
                    marginLeft: 25,
                    height: 20,
                    color: '#444444',
                    fontFamily: 'Poppins',
                    fontSize: 14,
                    fontWeight: 400,
                    ':nth-child(1n) > h5': {
                        color: '#444444',
                        fontFamily: 'Poppins',
                        fontSize: 14,
                        fontWeight: 100,
                    }
                }
            }
        },
        ':nth-child(1n) > div': {
            ':nth-child(1n) > div': {
                display: "flex",
                justifyContent: 'space-between',
                ':nth-child(1n) > p': {
                    color: '#a9a9a9',
                    fontFamily: 'Poppins',
                    fontSize: 14,
                    fontWeight: 300,
                },
                ':nth-child(1n) > h5': {
                    color: '#6ddba2',
                    fontFamily: 'Poppins',
                    fontSize: 14,
                    fontWeight: 300,
                }
            }
        }
    },
});

export default (withRouter(Way))
