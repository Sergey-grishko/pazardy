import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';


class PersonalData extends React.Component {
    constructor() {
        super()
        this.state = {}
    }


    render() {
        return (
            <div>
                <p className={css(style.title)}>Personal Data</p>
                <div className={css(style.editPhoto)}>
                    <img src={require('../sources/photoDATA.png')} alt="arrow"/>
                    <div>
                        <img src={require('../sources/edit.png')} alt="arrow"/>
                        edit photo
                    </div>
                </div>
                <p className={css(style.IInformation)}>Edit Personal IInformation</p>
                <div className={` ${css(style.changePassword)}`}>
                    <input placeholder="Full name"/>
                    <input placeholder="anna@mail.com"/>
                    <select>
                        <option defaultChecked={true} hidden={true}>12/03/2018</option>
                        <option>UA</option>
                    </select>
                    <button>Save Changes</button>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    IInformation:{
        color: "#444",
        fontFamily: "Poppins",
        fontSize: 16,
        fontWeight: 300,
        marginBottom:50
    },
    changePassword: {
        display: "flex",
        flexDirection: "column",
        ':nth-child(1n) > input': {
            marginBottom: 25,
            paddingLeft: 25,
            width: 329,
            height: 48,
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            ":focus": {
                outline: "none",
            }
        },
        ':nth-child(1n) > button': {
            width: 226,
            height: 53,
            cursor: "pointer",
            borderRadius: 7,
            border: "none",
            backgroundImage: "linear-gradient(to right, #37a973 0%, #6ddba2 100%)",
            color: "#ffffff",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
            ":focus": {
                outline: "none",
            },
        },
        ':nth-child(1n) > select': {
            color: 'rgb(154, 154, 154)',
            height: 48,
            width: 329,
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },
    editPhoto: {
        marginBottom:50,
        display: "flex",
        alignItems: "flex-end",
        ':nth-child(1n) > div': {
            color: "#a9a9a9",
            display: "flex",
            alignItems: "flex-end",
            fontFamily: "Poppins",
            fontSize: 12,
            fontWeight: 300,
            ':nth-child(1n) > img': {
                paddingRight:5
            }
        }
    },
    title: {
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },

});

export default (withRouter(PersonalData))
