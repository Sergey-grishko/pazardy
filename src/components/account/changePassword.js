import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import * as create from "../../action/auth";
import {toast} from 'react-toastify';


class Logout extends React.Component {
    constructor() {
        super()
        this.state = {
            New:"",
            Confirm:""
        }
        this.changePassword = this.changePassword.bind(this)
    }

    async changePassword() {
        try {
            if (this.state.New === "" || this.state.New.length <= 6) throw new Error("The field Password is very short. Minimum number of characters 7")
            if (this.state.Confirm === "" || this.state.Confirm.length <= 6) throw new Error("The field RePassword is very short. Minimum number of characters 7")
            if (this.state.Confirm !== this.state.New) throw new Error("Passwords are different. Try again")
            let response = await create.ChangePassword(this.state.New)
            toast.success(response.message)
            this.setState({ModalOpen: true})
        } catch (error) {
            toast.error(error.message);
        }

    }


    render() {
        return (
            <div>
                <p className={css(style.title)}>Change Password</p>
                <div className={` col-xl-12 ${css(style.changePassword)}`}>
                    <input type="password" value={this.state.New} onChange={(e) => this.setState({New: e.target.value})} placeholder="New Password*"/>
                    <input type="password" value={this.state.Confirm} onChange={(e) => this.setState({Confirm: e.target.value})}placeholder="Confirm Password*"/>
                    <button onClick={this.changePassword}>Save Changes</button>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    changePassword: {
        display: "flex",
        padding:0,
        flexDirection: "column",
        ':nth-child(1n) > input': {
            marginBottom: 25,
            paddingLeft: 25,
            width: 329,
            height: 48,
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            ":focus": {
                outline: "none",
            }
        },
        ':nth-child(1n) > button': {
            width: 226,
            height: 53,
            cursor: "pointer",
            borderRadius: 7,
            border: "none",
            backgroundImage: "linear-gradient(to right, #37a973 0%, #6ddba2 100%)",
            color: "#ffffff",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
            ":focus": {
                outline: "none",
            }
        }
    },
    title: {
        paddingLeft: 15,
        marginBottom: 45,
        padding:0,
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },

});

export default (withRouter(Logout))
