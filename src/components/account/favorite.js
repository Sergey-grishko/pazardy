import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import StarRatings from 'react-star-ratings';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import * as Items from "../../action/items";
import * as Cart from "../../action/cart";
import {store} from "../../index";
import {CART} from "../../reducers/const";
import connect from "react-redux/es/connect/connect";
import {toast} from 'react-toastify';


class Favorite extends React.Component {
    constructor() {
        super()
        this.state = {
            favorite: [],
            count: 1,
            arr: true
        }
        this.AddCart = this.AddCart.bind(this)
    }

    async componentDidMount() {
        let favorite = await Items.AllFavorited()
        if (favorite.favorited_items.length === 0) {
            this.setState({
                arr: false
            })
        }
        this.setState({
            favorite: favorite.favorited_items
        })

    }

    async AddCart(store_item_id, quantity) {
        if (localStorage.getItem("PazardyCartId") !== null) {
            let date = {
                cart_item: {
                    store_item_id: store_item_id,
                    quantity: quantity
                }
            }
            let res = await Cart.AddCartLoged(localStorage.getItem("PazardyCartId"), date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        } else {
            let cartId = await Cart.CartInitialize()
            localStorage.setItem("PazardyCartId", cartId.cart.cart_id)
            let date = {
                cart_item: {
                    store_item_id: store_item_id,
                    quantity: quantity
                }
            }
            let res = await Cart.AddCartLoged(cartId.cart.cart_id, date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        }
    }

    onChangeCount = (index, type = '+') => {
        let items = this.state.favorite;
        if (items[index].count === undefined) {
            if (type === '+') items[index].count = 2
        } else {
            if (type === '+') {
                items[index].count++
            }
            if (items[index].count !== 1) {
                if (type === '-') {
                    items[index].count--
                }
            }
        }
        this.setState({favorite: items})
    }

    render() {
        let Items = this.state.favorite.map((value, index) => {
            if (value.count === undefined) {
                value.count = 1
            }
            return (
                <div className={`col-xl-6 col-md-12 ${css(style.Items)}`} key={index}>
                    <div className={` ${css(style.slideBox)}`}>
                        <div>
                                    <span><StarRatings
                                        starSpacing="1px"
                                        starDimension="15px"
                                        rating={value.rating}
                                        starRatedColor="#ff776d"
                                        numberOfStars={5}
                                        name='rating'
                                    /></span><br/>
                            <h5>
                                <img
                                    src={value.item.normal_image}
                                    alt="pngpix"/>
                            </h5>
                            <div>
                                <p>${value.price}</p>
                                <div>
                                    <div onClick={() => this.onChangeCount(index, '-')}>-</div>
                                    <div>{value.count}</div>
                                    <div onClick={() => this.onChangeCount(index, '+')}>+</div>
                                </div>
                            </div>
                        </div>
                        <div className={css(style.selectSlider)}>
                            <Dropdown
                                trigger={['click']}
                                overlay={<ul className={` ${css(style.sortBy)}`}>
                                    <li>
                                        <a>
                                            Other available store<br/>
                                            in your aresa</a>
                                    </li>
                                    <li><a>Store Name <span>10$</span></a></li>
                                    <li><a>Store Name <span>10$</span></a></li>
                                    <li><a>Store Name <span>10$</span></a></li>
                                    <li><a>Store Name <span>10$</span></a></li>
                                </ul>}
                                animation="slide-up"
                            >
                                <div className={css(style.sublist)}>
                                    <button className={css(style.buttonSubtitle)}>Store Name<img
                                        src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                                </div>
                            </Dropdown>
                            <button onClick={() => this.AddCart(value.item.id, value.count)}>Add</button>
                        </div>
                    </div>
                    <div className={css(style.NameItems)}>
                        <div>
                            <NavLink
                                to={`/items/${value.id}`}>
                                <p>{value.item.english_name}</p>
                            </NavLink>
                            <h5>{value.item.english_description}</h5>
                        </div>
                    </div>
                </div>
            )
        });
        console.log(Items)
        return (
            <div className={css(style.ItemS)}>
                <p className={css(style.title)}>My Favorite Products</p>
                {this.state.arr === false ? (<div style={{fontSize:30, textAlign:"center"}}>You have not added any items yet</div>) : (
                    <div style={{height: "100vh", overflow: "auto"}} className="col-xl-12 row listFavorite">


                        {Items.length === 0 ? (<div className="loader"></div>) : Items}

                    </div>)}
            </div>
        )
    }
}

const style = StyleSheet.create({
    ItemS: {
        ':nth-child(1n) > div': {
            '@media (max-width: 1199px)': {
                height: "100vh",
                overflow: 'auto'
            }
        }
    },
    listSubtitle: {
        padding: 0,
        listStyle: 'none',
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        width: "60%",
        background: "#fff"
    },
    sublist: {
        width: "80%",
        display: "flex",
        flexDirection: "column"
    },
    buttonSubtitle: {
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'center',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    title: {
        paddingLeft: 15,
        marginBottom: 45,
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },
    NameItems: {
        marginTop: 25,
        display: "flex",
        justifyContent: "space-between",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > a': {
                textDecoration: 'none',
                ':nth-child(1n) > p': {
                    margin: 0,
                    cursor: "pointer",
                    color: "#444444",
                    fontSize: 18,
                    fontFamily: "Poppins",
                    fontWeight: 400
                }
            },
            ':nth-child(1n) > h5': {
                color: "#a9a9a9",
                fontSize: 14,
                fontFamily: "Poppins",
                fontWeight: 300
            }
        }
    },
    sortBy: {
        width: "100%",
        top: 30,
        listStyle: "none",
        background: "#fff",
        padding: "22px 0",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        ':nth-child(1n) > li': {
            cursor: "pointer",
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ":hover": {
                backgroundColor: "#ebebeb"
            },
            ':nth-child(1n) > a': {
                textDecoration: "none",
                color: '#000',
                width: "100%",
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > span': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                    lineHeight: "40px"
                }
            }
        },
        ':nth-child(1n) > li:first-child': {
            height: "100%",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 12,
            fontWeight: 400,
        }
    },
    sortByTitle: {
        justifyContent: "flex-end",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
    },
    Items: {
        marginBottom: 50
    },
    SelectItems: {
        ':nth-child(1n) > li': {
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ':nth-child(1n) > a': {
                width: "100%",
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > span': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                    lineHeight: "40px"
                }
            }
        },
    },
    slideBox: {
        borderRadius: 7,
        border: "1px solid #e1e1e1",
        height: "300px",
        ':nth-child(1n) > div:first-child': {
            padding: "10px 10px 0 10px",
            borderBottom: "1px solid #e1e1e1",
            ':nth-child(1n) > h5': {
                display: "flex",
                justifyContent: 'center',
                ':nth-child(1n) > img': {
                    width: "120px",
                    height: "100%",
                    display: "flex",
                }
            },
            ':nth-child(1n) > div': {
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > p': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                },
                ':nth-child(1n) > div': {
                    display: "flex",
                    alignItems: "center",
                    height: "14px",
                    marginTop: 8,
                    ':nth-child(1n) > div': {
                        padding: "0 12.5px",
                        userSelect: 'none',
                        color: "#a9a9a9",
                        fontFamily: "Poppins",
                        fontSize: 14,
                        fontWeight: 300,
                    },
                    ':nth-child(1n) > div:last-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                    ':nth-child(1n) > div:first-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                }
            }
        }
    },
    selectSlider: {
        display: 'flex',
        alignItems: 'center',
        height: "calc(100% - 248px)",
        ':nth-child(1n) > span': {
            width: "80%",
            alignItems: 'center',
            height: "100%",
            ':nth-child(1n) > select': {
                color: "#000000",
                border: "none",
            }
        },
        ':nth-child(1n) > button': {
            cursor: "pointer",
            outline: "none",
            background: "none",
            border: "none",
            borderLeft: "1px solid #e1e1e1",
            color: "#6ddba2",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
            transition: "0.9s",
            ":hover": {
                transition: "0.9s",
                background: "RGBA(69,69,69,0.1)"
            }

        }
    },
    selectStyle: {
        width: "14%",
        display: 'flex',
        ':nth-child(1n) > div': {
            marginLeft: "130px",
            pointerEvents: 'none',
            position: 'absolute',
            '@media (max-width: 1199px)': {
                display: "none"
            }
        },
        ':nth-child(1n) > select': {
            width: "100%",
            paddingLeft: "15px",
            fontSize: 17,
            fontFamily: "Lato",
            color: "#a9a9a9",
            background: "none",
            border: 'none',
            borderLeft: "1px solid #555555",
            borderRight: "1px solid #555555",
            appearance: 'none',
            height: 29,
            ":focus": {
                outline: "none"
            },

        },
        ':nth-child(1n) > ul': {
            ':nth-child(1n) > li': {
                display: "flex",
                justifyContent: 'center',
                ':nth-child(1n) > ul': {
                    width: 192,
                    top: 50
                }
            }
        }
    },
});

export default connect(store => ({store: store}))(withRouter(Favorite))
