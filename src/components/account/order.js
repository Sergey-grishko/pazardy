import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';


class Order extends React.Component {
    constructor() {
        super()
        this.state = {}
    }


    render() {
        return (
            <div>
                <p className={css(style.title)}>Order Details</p>
                <div className={css(style.orderData)}>
                    <div>
                        <p>Order placed</p>
                        <span>Mar 3,2018</span>
                    </div>
                    <div>
                        <p>Order number</p>
                        <span>#000000000</span>
                    </div>
                </div>
                <div className={css(style.orderList)}>
                    <div>
                        <p>Billing Address</p>
                        <ul>
                            <li>Anna Smith </li>
                            <li>4 LEWIS CIR </li>
                            <li>WILMINGTON, </li>
                            <li>DE 19804</li>
                            <li>United States </li>
                        </ul>
                    </div>
                    <div>
                        <p>Payment Method</p>
                        <ul>
                            <li>Master Card</li>
                            <li>************0000</li>
                            <li>Amount: $123</li>
                        </ul>
                    </div>
                    <div>
                        <p>Payment Total</p>
                        <ul>
                            <li><div>Subtotal</div> <span>$5</span></li>
                            <li><div>Shipping</div> <span>$6</span></li>
                            <li><div>Total</div> <span>$11</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    orderList:{
        marginTop:100,
        display:"flex",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > p': {
                color: "#444444",
                width:200,
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 400,
            },
            ':nth-child(1n) > ul': {
                listStyle:"none",
                padding:0,
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 300,
                lineHeight: "26px",
                ':nth-child(1n) > li': {
                    display:'flex',
                    ':nth-child(1n) > div': {
                        width:130
                    },
                    ':nth-child(1n) > span': {
                        color: "#000000",
                        width:200,
                        fontFamily: "Poppins",
                        fontSize: 18,
                        fontWeight: 400,
                    }
                }
            }
        }
    },
    orderData:{
        marginTop:50,
        ':nth-child(1n) > div': {
            display:'flex',
            // justifyContent:'space-between',
            ':nth-child(1n) > p': {
                color: "#444444",
                width:200,
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 400,
            },
            ':nth-child(1n) > span': {
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 300,
            }
        }
    },
    title: {
        padding:0,
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },

});

export default (withRouter(Order))
