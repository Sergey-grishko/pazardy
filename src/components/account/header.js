import React, {Component} from 'react'
import {withRouter, NavLink, Link} from 'react-router-dom'
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import connect from "react-redux/es/connect/connect";


class HeaderAccount extends Component {

    render() {
        return (
            <div>
                <div className={css(style.header)}>
                    <div className={`${css(style.logo)}`}>
                        <Link
                            to="/">
                            <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                        </Link>
                    </div>
                    <div>
                        <div className={css(style.home)}>
                            <Link
                                to="/">
                                <span>Home</span>
                            </Link>
                            <Link
                                to="/buy">
                                <div>
                                    <img src={require('../sources/shopping-cart.png')} alt="shopping"/>
                                    {this.props.store.cart.ItemsCart !== 0 ?( <div>{this.props.store.cart.ItemsCart}</div>):null}
                                </div>
                            </Link>
                        </div>
                        <p></p>
                        <span className={css(style.notifyHeader)}>

                                    <div>
                                        <img src={require('../sources/bell.png')} alt="bell"/>
                                        <div> </div>
                                    </div>

                             <Dropdown
                                 trigger={['click']}
                                 overlay={<ul className={` ${css(style.sortBy)}`}>
                                     <li>EU</li>
                                     <li>EU</li>
                                     <li>EU</li>
                                 </ul>}
                                 animation="slide-up"
                             >
                                <div className={css(style.sublist)}>
                                    <button className={css(style.buttonSubtitle)}>EU<img
                                        src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                                </div>
                            </Dropdown>
                    </span>
                    </div>
                </div>
            </div>
        )
    }

}

const style = StyleSheet.create({
    sortBy: {
        listStyle: "none",
        margin: "-15px 0 0 -10px",
        background: "#fff",
        padding: "10px 0",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        ':nth-child(1n) > li': {
            cursor: "pointer",
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ":hover": {
                backgroundColor: "#ebebeb"
            },
        }
    },
    listSubtitle: {
        padding: 0,
        listStyle: 'none',
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        width: "60%",
        background: "#fff"
    },
    sublist: {
        width: "80%",
        display: "flex",
        flexDirection: "column"
    },
    buttonSubtitle: {
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'center',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    logo: {
        ':nth-child(1n) > a': {
            width: "40%",
            display: 'flex',
            alignItems: "center"
        }
    },
    header: {
        display: "flex",
        paddingTop: 25,
        justifyContent: 'space-between',
        ':nth-child(1n) > div:last-child': {
            display: 'flex',
            ':nth-child(1n) > p': {
                width: 1,
                height: 29,
                background: "rgb(85, 85, 85)",
                border: 'solid 1px rgb(85, 85, 85)',
            }
        }
    },
    home: {
        display: 'flex',
        ':nth-child(1n) > a': {
            textDecoration: 'none',
            ':nth-child(1n) > span': {
                width: 49,
                height: 13,
                fontSize: 16,
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontStretch: 'normal',
                lineHeight: 'normal',
                letterSpacing: 0.6,
                textAlign: 'center',
                color: 'rgb(68, 68, 68)',
                ':hover': {
                    cursor: "pointer"
                }
            },
            ':nth-child(1n) > div': {
                padding: "0 32px 0 62px",
                ':nth-child(1n) > div': {
                    width: 21,
                    height: 21,
                    // font-family: Poppins;
                    fontSize: 14,
                    fontWeight: 500,
                    letterSpacing: 0.6,
                    textAlign: 'center',
                    color: 'rgb(68, 68, 68)',
                    borderRadius: "40px",
                    position: "absolute",
                    top: 24,
                    marginLeft: 12,
                    backgroundColor: 'rgb(109, 219, 162)',
                }
            }
        },

    },
    notifyHeader: {
        display: 'flex',
        ':nth-child(1n) > div': {
            cursor: "pointer",
            ':nth-child(1n) > div': {
                width: 9,
                height: 9,
                backgroundColor: 'rgb(255, 119, 109)',
                position: "absolute",
                top: 30,
                marginLeft: 7,
                borderRadius: 40
            }
        },
        ':nth-child(1n) > div:first-child': {
            margin: "0 27px 0 23px",
        },
    },
});

export default  connect(store => ({store: store}))(withRouter(HeaderAccount))
