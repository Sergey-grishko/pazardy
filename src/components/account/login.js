import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import * as create from "../../action/auth"
import {toast} from 'react-toastify';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


class Order extends React.Component {
    constructor() {
        super()
        this.state = {
            email: '',
            Password: '',
        }
        this.SignUp = this.SignUp.bind(this)
    }

    async SignUp() {
        try {
            if (this.state.email === "") throw new Error("The field Email is empty")
            if (this.state.Password === "" || this.state.Password.length <= 6) throw new Error("The field Password is very short. Minimum number of characters 7")
            if (reg.test(this.state.email) === false) throw new Error("Email incorrect")
            let body = {
                "email": this.state.email,
                "password": this.state.Password,

            }
            let response = await create.signIn(body)
            this.props.history.push("/account/favorite")
        } catch (error) {
            toast.error(error.message);
        }

    }

    render() {
        return (
            <div>
                <div className={css(style.Login)}>
                    <div>
                        <p>MY ACCOUNT LOGIN </p>
                        <span>*REQUIRED</span>
                    </div>
                    <div className={css(style.input)}>
                        <input value={this.state.email} onChange={(e) => this.setState({email: e.target.value})}
                               placeholder="Email*"/>
                        <input type="password" value={this.state.Password}
                               onChange={(e) => this.setState({Password: e.target.value})}
                               placeholder="Password*"/>
                        <button onClick={this.SignUp}>Sing in
                        </button>
                    </div>
                    <div className={css(style.cheack)}>
                        <label className={css(style.labelCheck)}>
                            {this.state.rememberMe ?
                                <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                    <span className={css(style.IconSpan)}>

                                            </span>)
                            }
                            <input
                                className={css(style.check)}
                                type="checkbox"
                                checked={this.state.rememberMe}
                                onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                            />
                            Remember Me
                        </label>
                        <div className={css(style.forgot)}>
                            Forgot Password
                        </div>
                    </div>
                </div>
                <div className={css(style.news)}>
                    <p>MY ACCOUNT LOGIN </p>
                    <span>Creating an account is easy. Just fill
out the form below.</span>
                    <NavLink
                        to="/account/create">
                        <button>Create an Account
                        </button>
                    </NavLink>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
        news: {
            '@media (max-width: 1199px)': {
                margin: "50px 0 0 0",
            },
            margin: "-10px 0 0 400px",
            ':nth-child(1n) > p': {
                color: '#000000',
                fontFamily: 'Poppins',
                fontSize: 18,
                margin: 0,
                fontWeight: 400,
            },
            ':nth-child(1n) > span': {
                color: '#444',
                fontSize: 16,
                width: 301,
                fontFamily: 'Poppins',
                fontWeight: 400,
            },
            ':nth-child(1n) > a': {
                ':nth-child(1n) > button': {
                    color: '#ffffff',
                    fontFamily: 'Poppins',
                    fontSize: 18,
                    fontWeight: 400,
                    marginTop: 50,
                    width: 240,
                    height: 53,
                    borderRadius: 7,
                    cursor: "pointer",
                    border: "none",
                    backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
                    ":focus": {
                        outline: "none"
                    }
                }
            }
        },
        cheack: {
            display: "flex",
            alignItems: "center"
        },
        labelCheck: {
            margin: "0 50px 0 25px",
            paddingRight: 50,
            fontSize: 12,
            cursor: 'pointer',
            fontWeight: 300,
            lineHeight: 4,
            letterSpacing: 0.4,
            color: "rgb(169, 169, 169)"
        },
        checkIconSpan: {
            marginTop: "-2px",
            width: '18px',
            height: '18px',
            display: 'inline-flex',
            justifyContent: 'center',
            alignItems: 'center',
            verticalAlign: 'middle',
            marginRight: '10px',
            borderRadius: '5px',
            cursor: 'pointer',
        },
        forgot: {
            cursor: "pointer",
            textDecoration: "underline",
            width: 105,
            fontSize: 12,
            fontWeight: 300,
            lineHeight: 2,
            letterSpacing: 0.4,
            color: "rgb(169, 169, 169)"
        },
        IconSpan: {
            marginTop: "-2px",
            width: '18px',
            height: '18px',
            display: 'inline-flex',
            justifyContent: 'center',
            alignItems: 'center',
            verticalAlign: 'middle',
            marginRight: '10px',
            border: "solid 2px rgb(235, 235, 235)",
            borderRadius: '5px',
            cursor: 'pointer',
        },
        check: {
            display: 'none',
        },
        checkIcon: {
            textAlign: "center",
            paddingTop: "2px",
            fontSize: '10px',
            width: '20px',
            height: '18px',
            border: "solid 2px rgb(109, 219, 162)",
            borderRadius: '5px',
            backgroundColor: "rgb(109, 219, 162)",
            color: '#fff'
        },
        Login: {
            top: 0,
            '@media (max-width: 1199px)': {
                background: "none",
                position: "static",
            },
            background: "#fafafa",
            position: "fixed",
            height: "100%",
            width: 381,
            ':nth-child(1n) > div:first-child': {
                '@media (max-width: 1199px)': {
                    margin: 0,
                },
                margin: "160px 0 0 25px",
                ':nth-child(1n) > p': {
                    color: '#000000',
                    fontFamily: 'Poppins',
                    fontSize: 18,
                    margin: 0,
                    fontWeight: 400,
                },
                ':nth-child(1n) > span': {
                    color: '#a9a9a9',
                    fontSize: 16,
                    fontFamily: 'Poppins',
                    fontWeight: 400,
                }
            },
        },
        input: {
            '@media (max-width: 1199px)': {
                margin: "50px 0 0 0",
            },
            margin: "50px 0 0 25px",
            ':nth-child(1n) > input': {
                width: 329,
                height: 48,
                padding: "0 0 0 26px",
                borderRadius: 5,
                border: "solid 1px rgba(85, 85, 85,0.5)",
                ":focus": {
                    outline: "none"
                }
            },
            ':nth-child(1n) > input:first-child': {
                marginBottom: 25
            },
            ':nth-child(1n) > button': {
                color: '#ffffff',
                fontFamily: 'Poppins',
                fontSize: 18,
                fontWeight: 400,
                marginTop: 50,
                width: 170,
                height: 53,
                borderRadius: 7,
                cursor: "pointer",
                border: "none",
                backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
                ":focus": {
                    outline: "none"
                }

            }
        }
    })
;

export default (withRouter(Order))
