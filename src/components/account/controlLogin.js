import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';


class ControlLogin extends React.Component {
    render() {
        return (
            <div className={css(style.content)}>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>My account</div>
                </div>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>Account Setting</div>
                    <NavLink
                        to="/account/create">
                        <span className="Navlink">Creat an account</span>
                    </NavLink>
                </div>
                <div className={css(style.help)}>
                    Need Help?
                    If you have any questions or need
                    help with your account, you may
                    contact us to assist you.
                    <div>Contact Us</div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    help: {
        width: 281,
        color: '#444444',
        fontFamily: 'Poppins',
        fontSize: 16,
        fontWeight: 300,
        lineHeight: "26px",
        ':nth-child(1n) > div': {
            marginTop:20,
            cursor:"pointer",
            textDecoration: 'underline',
        }
    },
    block: {
        display: "flex",
        flexDirection: "column",
        ':nth-child(1n) > a': {
            textDecoration:'none',
            ':nth-child(1n) > span': {
                ":hover": {
                    cursor: "pointer",
                    textDecoration: "underline"
                }
            }
        },
        ':nth-child(1n) > span': {
            ":hover": {
                cursor: "pointer",
                textDecoration: "underline"
            }
        }
    },
    title: {
        color: "#000000",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 500,
    },
    content: {
        ':nth-child(1n) > div:first-child': {
            marginBottom: 50
        },
        ':nth-child(1n) > div': {
            marginBottom: 100
        }
    }
});

export default (withRouter(ControlLogin))
