import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import * as create from "../../action/auth"


class Controle extends React.Component {
    constructor(props) {
        super(props)
        this.SignOut = this.SignOut.bind(this)
    }

    async SignOut() {
        await create.signOut()
        this.props.history.push("/")
        localStorage.removeItem('pazardy-token');
        localStorage.removeItem('expiry');
        localStorage.removeItem('client');
        localStorage.removeItem('uid');
    }

    render() {
        return (
            <div className={css(style.content)}>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>My account</div>
                    <span onClick={() => this.SignOut()}
                          className="Navlink">Logout</span>
                </div>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>Account Setting</div>
                    <NavLink
                        to="/account/personalData">
                        <span className="Navlink">Personal Data</span>
                    </NavLink>
                    <NavLink
                        to="/account/changePassword">
                        <span className="Navlink">Change Password</span>
                    </NavLink>
                    <NavLink
                        to="/account/trips">
                        <span className="Navlink">My Trips and Earnings</span>
                    </NavLink>
                    <NavLink
                        to="/account/book">
                        <span className="Navlink">Adress Book</span>
                    </NavLink>
                    <NavLink
                        to="/account/carrier">
                        <span className="Navlink">Carrier</span>
                    </NavLink>
                </div>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>Order Information</div>
                    <NavLink
                        to="/account/order">
                        <span className="Navlink">Resent Orders</span>
                    </NavLink>
                </div>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>Favorite</div>
                    <NavLink
                        to="/account/favorite">
                        <span className="Navlink">My Favorte Products</span>
                    </NavLink>
                </div>
                <div className={css(style.block)}>
                    <div className={css(style.title)}>My Location</div>
                    <Dropdown
                        trigger={['click']}
                        overlay={
                            <ul className={css(style.listSubtitle)}>
                                <li>Turkish</li>
                                <li>English</li>
                                <li>Arabic</li>
                            </ul>}
                        animation="slide-up"
                    >
                        <div className={css(style.sublist)}>
                            <button className={css(style.buttonSubtitle)}>Language<img
                                src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                        </div>
                    </Dropdown>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    listSubtitle: {
        padding:"5px 0",
        listStyle: 'none',
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        width: "100px",
        textAlign:"center",
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        background: "#fff",
        ':nth-child(1n) > li': {
            cursor:"pointer",
           ":hover":{
                margin:"0 10px",
               background:"#f9f9f9"
           }
        }
    },
    sublist: {
        display: "flex",
        flexDirection: "column"
    },
    buttonSubtitle: {
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'left',
        cursor: "pointer",
        width: "80%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    block: {
        display: "flex",
        flexDirection: "column",
        ':nth-child(1n) > a': {
            textDecoration: "none",
            ':nth-child(1n) > span': {
                ":hover": {
                    cursor: "pointer",
                    textDecoration: "underline"
                }
            }
        },
        ':nth-child(1n) > span': {
            ":hover": {
                cursor: "pointer",
                textDecoration: "underline"
            }
        }
    },
    title: {
        color: "#000000",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 500,
    },
    content: {
        ':nth-child(1n) > div:first-child': {
            marginBottom: 50
        },
        ':nth-child(1n) > div': {
            marginBottom: 100
        }
    }
});

export default (withRouter(Controle))
