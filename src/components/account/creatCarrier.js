import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';
import * as create from "../../action/auth"
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-modal';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        padding: 0,
        marginRight: '-50%',
        borderRadius: 0,
        border: "none",
        width: 473,
        height: 344,
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "RGBA(0,0,0,0.5)",
    }
};

class Create extends React.Component {
    constructor() {
        super()
        this.state = {
            email: '',
            Password: '',
            RePassword: '',
            Phone: '',
            Telephone: '',
            FirstName: '',
            LastName: '',
            check: "customer",
            ModalOpen: false,
            Code: ''
        }
        this.SignUp = this.SignUp.bind(this)
        this.handleModal = this.handleModal.bind(this)
        this.Verify = this.Verify.bind(this)
    }

    async SignUp() {
        try {
            if (this.state.FirstName === "" || this.state.FirstName.length <= 2) throw new Error("The field FirstName is very short. Minimum number of characters 3")
            if (this.state.LastName === "" || this.state.LastName.length <= 2) throw new Error("The field LastName is very short. Minimum number of characters 3")
            if (this.state.email === "") throw new Error("The field Email is empty")
            if (this.state.Password === "" || this.state.Password.length <= 6) throw new Error("The field Password is very short. Minimum number of characters 7")
            if (this.state.RePassword === "" || this.state.RePassword.length <= 6) throw new Error("The field RePassword is very short. Minimum number of characters 7")
            if (this.state.Phone === "") throw new Error("The field Phone is empty")
            if (this.state.Telephone === "") throw new Error("The field Telephone is empty")
            if (this.state.Password !== this.state.RePassword) throw new Error("Passwords are different. Try again")
            if (reg.test(this.state.email) === false) throw new Error("Email incorrect")
            let body = {
                "email": this.state.email,
                "password": this.state.Password,
                "password_confirmation": this.state.RePassword,
                "first_name": this.state.FirstName,
                "last_name": this.state.LastName,
                "telephone_number": this.state.Telephone,
                "mobile_phone_number": this.state.Phone
            }
            let response = await create.signup(body)
            toast.success(response.message)
            this.setState({ModalOpen: true})
        } catch (error) {
            toast.error(error.message);
        }
    }


    handleKeyPress = async  (event) => {
        if(event.key == 'Enter'){
            await this.SignUp()
        }
    }

    async Verify() {
        try {
            if (this.state.Code === "") throw new Error("The field is empty")
            let body = {
                "verification_code": this.state.Code,
            }
            let response = await create.Verify(body)
            toast.success(response.message)
            this.setState({ModalOpen: false})
            this.props.history.push("/account/favorite")
        } catch (error) {
            toast.error(error.message);
        }
    }

    handleModal() {
        this.setState({
            ModalOpen: !this.state.ModalOpen
        })
        this.props.history.push("/account")
    }

    render() {
        return (
            <div>
                <div className={css(style.createTitle)}>
                    <p><h5>CARRIER</h5>
                    </p>
                    <span>*REQUIRED</span>
                </div>
                <div className={css(style.carrier)}>
                    <div className={`col-xl-12 row`}>
                        <div className={`col-xl-6`}>
                            <div>
                                <input placeholder="Attach  ID  (ID with picture)*"/>
                                <img
                                    src={require('../sources/attachment.png')} alt="arrow"/>
                            </div>
                            <input placeholder="SSN*"/>
                        </div>
                        <div className={`col-xl-6`}>
                            <Dropdown
                                trigger={['click']}
                                overlay={<ul>
                                    <li>123</li>
                                </ul>}
                                animation="slide-up"
                            >
                                <button className={css(style.buttonAddress)}>Work Address*<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                            </Dropdown>
                            <input placeholder="Company Name*"/>
                        </div>
                    </div>
                    <p>CREDIT CARD INFO</p>
                    <span>*REQUIRED</span>
                    <div className={`col-xl-12 row`}>
                        <div className={`col-xl-6`}>
                            <Dropdown
                                trigger={['click']}
                                overlay={<ul>
                                    <li>123</li>
                                </ul>}
                                animation="slide-up"
                            >
                                <button className={css(style.buttonAddress)}>Cardholder Name*<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                            </Dropdown>
                            <input placeholder="Card Number*"/>
                            <input placeholder="Address*"/>
                        </div>
                        <div className={`col-xl-6`}>
                            <input placeholder="Expire Date*"/>
                            <input placeholder="CVV*"/>
                        </div>
                    </div>
                    <label className={css(style.labelCheck)}>
                        {this.state.rememberMe ?
                            <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                <span className={css(style.IconSpan)}>

                                            </span>)
                        }
                        <input
                            className={css(style.check)}
                            type="checkbox"
                            checked={this.state.rememberMe}
                            onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                        />
                        Save cart
                    </label>
                    <h5>Pazardy does not share or sell personal information</h5>
                    <button>Change Account</button>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    ModalStyle: {
        padding: 20,
        ':nth-child(1n) > p': {
            margin: 0,
            display: "flex",
            alignItems: 'center',
            justifyContent: "flex-end",
            ':nth-child(1n) > img': {
                cursor: "pointer"
            }
        },
        ':nth-child(1n) > span': {
            width: 350,
            height: 66,
            color: '#808080',
            fontFamily: 'Poppins',
            fontSize: 16,
            fontWeight: 400,
            lineHeight: "26px",
            display: 'flex',
            textAlign: "center",
            margin: "30px auto"
        },
        ':nth-child(1n) > input': {
            width: "100%",
            height: 48,
            padding: "0 0 0 26px",
            borderRadius: 5,
            margin: "20px 0 0 0",
            border: "solid 1px rgba(85, 85, 85,0.5)",
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > div': {
            marginTop: 20,
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            ':nth-child(1n) > button': {
                color: '#ffffff',
                fontFamily: 'Poppins',
                fontSize: 18,
                fontWeight: 400,
                width: 156,
                height: 53,
                borderRadius: 7,
                cursor: "pointer",
                border: "none",
                backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
                ":focus": {
                    outline: "none"
                }
            },
            ':nth-child(1n) > h5': {
                cursor: "pointer",
                color: '#9a9a9a',
                fontFamily: 'Poppins',
                fontSize: 16,
                fontWeight: 300,
                textDecoration: 'underline',
            },
        }
    },
    carrier: {
        ':nth-child(1n) > button': {
            color: '#ffffff',
            fontFamily: 'Poppins',
            fontSize: 18,
            fontWeight: 400,
            marginTop: 50,
            width: 240,
            height: 53,
            borderRadius: 7,
            cursor: "pointer",
            border: "none",
            backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > h5': {
            color: '#a9a9a9',
            fontFamily: 'Poppins',
            fontSize: 12,
            fontWeight: 400,
        },
        ':nth-child(1n) > p': {
            margin: 0,
            display: "flex",
            color: '#000000',
            fontFamily: 'Poppins',
            fontSize: 18,
            fontWeight: 400
        },
        ':nth-child(1n) > span': {
            color: '#a9a9a9',
            fontFamily: 'Poppins',
            fontSize: 16,
            marginBottom: 50,
            fontWeight: 400,
        },
        ':nth-child(1n) > div': {
            '@media (max-width: 1199px)': {
                margin: 0,
            },
            margin: "50px 0 0 0",
            padding: 0,
            ':nth-child(1n) > div': {
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > input': {
                        width: "80%",
                        height: 48,
                        padding: "0 0 0 26px",
                        borderRadius: 5,
                        marginBottom: 10,
                        border: "solid 1px rgba(85, 85, 85,0.5)",
                        ":focus": {
                            outline: "none"
                        },
                    },
                    ':nth-child(1n) > img': {
                        marginLeft: 10
                    }
                },
                ':nth-child(1n) > input': {
                    width: "80%",
                    height: 48,
                    padding: "0 0 0 26px",
                    borderRadius: 5,
                    marginBottom: 10,
                    border: "solid 1px rgba(85, 85, 85,0.5)",
                    ":focus": {
                        outline: "none"
                    }
                },
                ':nth-child(1n) > button': {
                    width: "80%",
                    background: "#fff",
                    color: '#9a9a9a',
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: 300,
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: 'center',
                    height: 48,
                    padding: "0 26px 0 26px",
                    borderRadius: 5,
                    marginBottom: 10,
                    border: "solid 1px rgba(85, 85, 85,0.5)",
                    ":focus": {
                        outline: "none"
                    }
                },
                padding: 0,
            }
        },
    },
    customer: {
        '@media (max-width: 1199px)': {
            margin: 0,
        },
        marginTop: 50,
        ':nth-child(1n) > div': {
            display: 'flex',
            flexDirection: 'column',
            ':nth-child(1n) > input': {
                width: 329,
                height: 48,
                padding: "0 0 0 26px",
                borderRadius: 5,
                marginBottom: 25,
                border: "solid 1px rgba(85, 85, 85,0.5)",
                ":focus": {
                    outline: "none"
                }
            },
            ':nth-child(1n) > span': {
                color: '#a9a9a9',
                fontFamily: 'Poppins',
                fontSize: 12,
                fontWeight: 400,
            },
            ':nth-child(1n) > button': {
                color: '#ffffff',
                fontFamily: 'Poppins',
                fontSize: 18,
                fontWeight: 400,
                marginTop: 50,
                width: 240,
                height: 53,
                borderRadius: 7,
                cursor: "pointer",
                border: "none",
                backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
                ":focus": {
                    outline: "none"
                }
            }
        }
    },
    createTitle: {
        ':nth-child(1n) > p': {
            margin: 0,
            display: "flex",
            color: '#000000',
            fontFamily: 'Poppins',
            fontSize: 18,
            ':nth-child(1n) > span': {
                fontWeight: 400
            },
            ':nth-child(1n) > h5': {
                cursor: "pointer",
                margin: "0 5px"
            }
        },
        ':nth-child(1n) > span': {
            color: '#a9a9a9',
            fontFamily: 'Poppins',
            fontSize: 16,
            fontWeight: 400,
        }
    },
    cheack: {
        display: "flex",
        alignItems: "center"
    },
    labelCheck: {
        margin: "0 50px 0 0",
        paddingRight: 50,
        fontSize: 12,
        cursor: 'pointer',
        fontWeight: 300,
        lineHeight: 4,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    checkIconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    IconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        border: "solid 2px rgb(235, 235, 235)",
        borderRadius: '5px',
        cursor: 'pointer',
    },
    check: {
        display: 'none',
    },
    checkIcon: {
        textAlign: "center",
        paddingTop: "2px",
        fontSize: '10px',
        width: '20px',
        height: '18px',
        border: "solid 2px rgb(109, 219, 162)",
        borderRadius: '5px',
        backgroundColor: "rgb(109, 219, 162)",
        color: '#fff'
    },
});

export default (withRouter(Create))
