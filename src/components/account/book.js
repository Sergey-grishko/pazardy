import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import * as info from "../../action/info"
import Autocomplete from 'react-google-autocomplete';
import Geocode from "react-geocode";
import connect from "react-redux/es/connect/connect";
import * as Address from '../../action/address'
import {toast} from "react-toastify";


class Book extends React.Component {
    constructor() {
        super()
        this.state = {
            address: '',
            placeCity: '',
            placeDistrict: '',
            Street: '',
            Building: '',
            Floot: '',
            Code: '',
            Appartment: '',
            city_name: '',
            district_name: '',
            street: '',
            building: '',
            floor: '',
            zip_code: '',
            apartment_number: '',
            editBool: false,
            idAddress: 0,
            placeName: ''
        }
        this.GetAddress = this.GetAddress.bind(this);
        this.Edit = this.Edit.bind(this);
    }

    async componentDidMount() {
        await this.GetAddress()
    }

    async GetAddress() {
        let res = await Address.GetArddress();
        if (res.addresses.length !== 0) {
            this.setState({
                city_name: res.addresses[0].district.city.city_name,
                idAddress: res.addresses[0].id,
                district_name: res.addresses[0].district.district_name,
                street: res.addresses[0].address_detail.street,
                building: res.addresses[0].address_detail.building,
                floor: res.addresses[0].address_detail.floor,
                zip_code: res.addresses[0].address_detail.zip_code,
                apartment_number: res.addresses[0].address_detail.apartment_number
            })
        }
    }

    async AddAddress() {
        try {
            if (this.state.Appartment === "") throw new Error("The field Appartment is empty.")
            if (this.state.Floot === "") throw new Error("The field Floot is empty.")
            if (this.state.Code === "") throw new Error("The field Code is empty.")
            if (this.state.Building === "") throw new Error("The field Building is empty.")
            if (this.state.placeDistrict === "" || this.state.placeCity === '') throw new Error("Please choose the address from the list")
            let data = {
                address: {
                    address_detail_attributes: {
                        long: this.state.place.geometry.viewport.b.b,
                        lat: this.state.place.geometry.viewport.f.f,
                        apartment_number: this.state.Appartment,
                        floor: this.state.Floot,
                        zip_code: this.state.Code,
                        street: this.state.Street,
                        building: this.state.Building
                    },
                    district_attributes: {
                        district_name: this.state.placeDistrict,
                        city_attributes: {
                            city_name: this.state.placeCity
                        }
                    }
                }
            }
            let res = await Address.addAddress(data)
            await this.GetAddress()
            this.setState({
                editBool: false,
                Building: "",
                Street: "",
                Code: "",
                Floot: "",
                Appartment: "",
                placeName: "",
                placeCity: ''
            })
            toast.success("The address has been changed");
        } catch (e) {
            toast.error(e.message);
        }
        // console.log(this.state.place)
        // console.log(this.state.place.formatted_address)
        // console.log(this.state.place.geometry.viewport.b.b)
        // console.log(this.state.place.geometry.viewport.b.f)
        // console.log(this.state.place.Appartment)

    }

    Edit() {
        this.setState({
            editBool: true,
            Building: this.state.building,
            Street: this.state.street,
            Code: this.state.zip_code,
            Floot: this.state.floor,
            Appartment: this.state.apartment_number,
            placeName: this.state.city_name
        })
    }

    async OnEdit() {
        try {
            if (this.state.Appartment === "") throw new Error("The field Appartment is empty.")
            if (this.state.Floot === "") throw new Error("The field Floot is empty.")
            if (this.state.Code === "") throw new Error("The field Code is empty.")
            if (this.state.Building === "") throw new Error("The field Building is empty.")
            if (this.state.placeDistrict === "" || this.state.placeCity === '') throw new Error("Please choose the city from the list")
            let data = {
                address: {
                    address_detail_attributes: {
                        long: this.state.place.geometry.viewport.b.b,
                        lat: this.state.place.geometry.viewport.f.f,
                        apartment_number: this.state.Appartment,
                        floor: this.state.Floot,
                        zip_code: this.state.Code,
                        street: this.state.Street,
                        building: this.state.Building
                    },
                    district_attributes: {
                        district_name: this.state.placeDistrict,
                        city_attributes: {
                            city_name: this.state.placeCity
                        }
                    }
                }
            }
            let res = await Address.PutArddress(this.state.idAddress, data)
            await this.GetAddress()
            this.setState({
                editBool: false,
                Building: "",
                Street: "",
                Code: "",
                Floot: "",
                Appartment: "",
                placeName: ""
            })
            toast.success("The address has been changed");
        } catch (e) {
            toast.error(e.message);
        }
    }


    // handelMarker(lat = this.props.store.location.code.latitude, lng = this.props.store.location.code.longitude) {
    //     Geocode.setApiKey("AIzaSyDkoz0whWt9RBLL7rMoX-33weWkJUtX-bI");
    //
    //
    //     Geocode.fromLatLng(lat, lng).then(
    //         response => {
    //             // console.log(response.results.length)
    //
    //             const address = response.results[0].formatted_address;
    //             this.setState({address: address})
    //         },
    //         error => {
    //             console.error(error);
    //         }
    //     );
    // }


    render() {
        // console.log(this.state)
        return (
            <div>
                <p className={css(style.title)}>Address Book</p>
                <div className={css(style.Address)}>
                    <p>Delivery Address</p>
                    <div className={css(style.AddressBody)}>
                        {this.state.city_name === '' ? (<div>You have not added an address yet</div>) : (
                            <div>
                                <div>{this.state.city_name},{this.state.district_name}, {this.state.street},{this.state.building},{this.state.apartment_number}, {this.state.floor}, {this.state.zip_code}</div>
                                <h5 onClick={() => this.Edit()}>edit</h5></div>)}
                    </div>
                    <div className={css(style.addAddress)}>
                        <div><span>+</span></div>
                        <span>Add new address</span>
                    </div>
                </div>
                <div className={css(style.inputAddress)}>
                    <div>
                        <div className={`col-12 ${css(style.LogoBody)}`}>
                            <p>SHIPPING ADDRESS</p>
                            <span>* REQUIRED</span>
                        </div>
                        <div className="col-12 row">
                            <div className={`col-xl-6 col-md-12 ${css(style.InputBody)}`}>
                                {/*<input value={this.state.Street} onChange={(e)=>this.setState({Street:e.target.value})} placeholder="Country.City.District**"/>*/}
                                <Autocomplete
                                    value={this.state.placeName}
                                    onChange={(e) => this.setState({placeName: e.target.value})}
                                    style={{width: '90%'}}
                                    placeholder="City.District*"
                                    onPlaceSelected={(place) => {
                                        this.setState({
                                            placeDistrict: place.address_components[1].long_name,
                                            placeName: place.address_components[0].long_name,
                                            placeCity: place.address_components[0].long_name,
                                            place
                                        }, console.log(place))
                                    }}
                                />
                                <input value={this.state.Street}
                                       onChange={(e) => this.setState({Street: e.target.value})} placeholder="Street*"/>
                                <input value={this.state.Building}
                                       onChange={(e) => this.setState({Building: e.target.value})}
                                       placeholder="Building*"/>
                            </div>
                            <div className={`col-xl-6 col-md-12 ${css(style.InputBody)}`}>
                                <input value={this.state.Appartment}
                                       onChange={(e) => this.setState({Appartment: e.target.value})}
                                       placeholder="Appartment*"/>
                                <input value={this.state.Floot} onChange={(e) => this.setState({Floot: e.target.value})}
                                       placeholder="Floot*"/>
                                <input value={this.state.Code} onChange={(e) => this.setState({Code: e.target.value})}
                                       placeholder="ZIP Code*"/>
                            </div>
                        </div>
                    </div>
                    {this.state.editBool === false ? (
                        <button onClick={() => this.AddAddress()} className={css(style.save)}>Add Address</button>) : (
                        <button onClick={() => this.OnEdit()} className={css(style.save)}>Save Changes</button>)}

                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    save: {
        width: 226,
        height: 53,
        cursor: "pointer",
        borderRadius: 7,
        border: "none",
        backgroundImage: "linear-gradient(to right, #37a973 0%, #6ddba2 100%)",
        color: "#ffffff",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
        ":focus": {
            outline: "none",
        }
    },
    inputAddress: {
        marginTop: 25,
        ':nth-child(1n) > div': {
            ':nth-child(1n) > div:last-child': {
                ':nth-child(1n) > div:last-child': {
                    paddingLeft: 10
                },
                ':nth-child(1n) > div:first-child': {
                    paddingRight: 10
                }
            }
        }
    },
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 51,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    InputBody: {
        padding: 0,
        ':nth-child(1n) > input': {
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > select': {
            color: 'rgb(154, 154, 154)',
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },
    addAddress: {
        display: 'flex',
        alignItems: "center",
        ':nth-child(1n) > div': {
            cursor: "pointer",
            width: 45,

            height: 45,
            borderRadius: 40,
            backgroundImage: "linear-gradient(to right, #37a973 0%, #6ddba2 100%)",
            ':nth-child(1n) > span': {
                alignItems: "center",
                display: "flex",
                justifyContent: "center",
                fontSize: 30,
                color: "#fff",
                marginTop: -2,
                fontWeight: 100
            }
        },
        ':nth-child(1n) > span': {
            cursor: "pointer",
            marginLeft: 10,
            color: "#a9a9a9",
            fontFamily: "Poppins",
            fontSize: 14,
            fontWeight: 400,
        }
    },
    AddressBody: {
        ':nth-child(1n) > div': {
            margin: "15px 0 100px 0",
            ':nth-child(1n) > div': {
                width: 246,
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
                lineHeight: "24px"

            },
            ':nth-child(1n) > h5': {
                color: "#444444",
                margin: "15px 0 100px 0",
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: 400,
                textDecoration: "underline",
                cursor: "pointer"
            },
        }

    },
    Address: {
        marginTop: 50,
        ':nth-child(1n) > p': {
            marginBottom: 40,
            color: "#6ddba2",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 300,
        }
    },
    title: {
        padding: 0,
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },

});
export default connect(store => ({store: store}))(withRouter(Book))

