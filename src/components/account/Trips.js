import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import Way from "./Way"
import 'rc-dropdown/assets/index.css';


class Trips extends React.Component {
    constructor() {
        super()
        this.state = {}
    }


    render() {
        return (
            <div>
                <p className={css(style.title)}>Trips and Earnings</p>
                <div className={css(style.earnings)}>
                    <div>Earnings</div>
                    <div>
                        <div><p>Total earned</p><h5>$5500</h5></div>
                        <div><p>This month vs last month</p><h5>$1100</h5><h4>9% <img
                            src={require('../sources/play-button.png')} alt="arrow"/></h4></div>
                        <div><p>This month vs last month</p><h5>$1950</h5><h4>5$ <img
                            src={require('../sources/play-button_copy.png')} alt="arrow"/></h4></div>
                    </div>
                </div>
                <div className={css(style.Status)}>
                    <div>Today Status</div>
                    <div>
                        <div><p>Completed trips</p><h5>15</h5></div>
                        <div><p>Hours drived </p><h5>7 h 30 min</h5></div>
                    </div>
                </div>
                <div className={css(style.Trip)}>
                    <div>
                        <p>My Trips</p>
                        <div>
                            <p>Trip#0000000</p>
                            <Dropdown
                                trigger={['click']}
                                overlay={<Way/>}
                                animation="slide-up"
                            >
                                <button className={css(style.buttonSubtitle)}>Trip details<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                            </Dropdown>
                        </div>
                    </div>
                    <div>
                        <p></p>
                        <div>
                            <p>Trip#0000000</p>
                            <Dropdown
                                trigger={['click']}
                                overlay={<Way/>}
                                animation="slide-up"
                            >
                                <button className={css(style.buttonSubtitle)}>Trip details<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                            </Dropdown>
                        </div>
                    </div>
                    <div>
                        <p></p>
                        <div>
                            <p>Trip#0000000</p>
                            <Dropdown
                                trigger={['click']}
                                overlay={<Way/>}
                                animation="slide-up"
                            >
                                <button className={css(style.buttonSubtitle)}>Trip details<img
                                    src={require('../sources/down-arrow.png')} alt="arrow"/></button>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    Trip: {
        marginTop:50,
        ':nth-child(1n) > div': {
            display: "flex",
            padding: "10px 0",
            borderBottom: '1px solid RGBA(69,69,69,0.5)',
            alignItems: "center",
            justifyContent: "space-between",
            ':nth-child(1n) > p': {
                color: '#6ddba2',
                fontFamily: 'Poppins',
                fontSize: 18,
                margin: 0,
                fontWeight: 400,
            },
            ':nth-child(1n) > div': {
                display: "flex",
                alignItems: "center",
                ':nth-child(1n) > p': {
                    margin: "0 100px 0 0",
                    color: '#444444',
                    fontFamily: 'Poppins',
                    fontSize: 18,
                    fontWeight: 400,
                }
            }
        }
    },
    buttonSubtitle: {
        padding: "0",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: 400,
        lineHeight: "36px",
        textAlign: 'left',
        cursor: "pointer",
        width: "100%",
        background: "none",
        border: 'none',
        ":focus": {
            outline: "none"
        },
        ':nth-child(1n) > img': {
            marginLeft: 10
        }
    },
    Status: {
        marginTop: 25,
        borderBottom: '1px solid RGBA(69,69,69,0.5)',
        display: 'flex',
        ':nth-child(1n) > div:last-child': {
            display: "flex",
            flexDirection: "column",
            ':nth-child(1n) > div': {
                display: "flex",
                ':nth-child(1n) > p': {
                    color: "#444",
                    fontSize: 16,
                    fontFamily: "Poppins",
                    fontWeight: 400,
                    width: 250
                },
                ':nth-child(1n) > h5': {
                    color: "#6ddba2",
                    fontSize: 20,
                    fontFamily: "Poppins",
                    fontWeight: 400
                },
            }
        },
        ':nth-child(1n) > div:first-child': {
            width: 180,
            color: "#00000",
            fontSize: 18,
            fontFamily: "Poppins",
            fontWeight: 400
        }
    },
    earnings: {
        borderBottom: '1px solid RGBA(69,69,69,0.5)',
        display: 'flex',
        ':nth-child(1n) > div:last-child': {
            display: "flex",
            flexDirection: "column",
            ':nth-child(1n) > div': {
                display: "flex",
                ':nth-child(1n) > p': {
                    color: "#444",
                    fontSize: 16,
                    fontFamily: "Poppins",
                    fontWeight: 400,
                    width: 250
                },
                ':nth-child(1n) > h5': {
                    width: 100,
                    color: "#6ddba2",
                    fontSize: 20,
                    fontFamily: "Poppins",
                    fontWeight: 400
                },
                ':nth-child(1n) > h4': {
                    width: 100,
                    color: "#ff776d",
                    fontSize: 16,
                    display: "flex",
                    alignItems: "center",
                    fontFamily: "Poppins",
                    fontWeight: 300,
                    ':nth-child(1n) > img': {
                        marginLeft: 5,
                    }
                }
            }
        },
        ':nth-child(1n) > div:first-child': {
            width: 180,
            color: "#6ddba2",
            fontSize: 18,
            fontFamily: "Poppins",
            fontWeight: 400
        }
    },
    title: {
        color: "#000",
        fontSize: 18,
        fontFamily: "Poppins",
        fontWeight: 300
    },

});

export default (withRouter(Trips))
