import React, {Component} from 'react'
import {Switch, Route, withRouter, Redirect} from 'react-router-dom'
import {css, StyleSheet} from 'aphrodite';
import Controle from "./control"
import ControlLogin from "./controlLogin"
import Favorite from "./favorite"
import Header from "./header"
import changePassword from "./changePassword"
import Book from "./book"
import Order from "./order"
import PersonalData from "./PersonalData"
import Trips from "./Trips"
import Login from "./login"
import Create from "./Create"
import Carrier from "./creatCarrier"

import {ToastContainer} from 'react-toastify';


class App extends Component {
    constructor(props){
        super(props)
    }

    componentDidMount(){
        if(localStorage.getItem("pazardy-token") === null){
            this.props.history.push("/account/login")
        }else {
            this.props.history.push("/account/favorite")
        }
    }
    render() {
        return (
            <div>
                <div style={localStorage.getItem("pazardy-token")!== null ? null:{display:"none"}} className={css(style.bg)}>
                    <div className="container">
                        <Header/>
                        <div className={` ${css(style.content)}`}>
                            <div className={`${css(style.contentLeft)}`}>
                                <Controle/>
                            </div>
                            <div className={`${css(style.contentRight)}`}>
                                <Switch>
                                    <Redirect exact from='/account' to='/account/favorite'/>
                                    <Route path="/account/favorite" component={Favorite}/>
                                    <Route path="/account/changePassword" component={changePassword}/>
                                    <Route path="/account/book" component={Book}/>
                                    <Route path="/account/order" component={Order}/>
                                    <Route path="/account/carrier" component={Carrier}/>
                                    <Route path="/account/personalData" component={PersonalData}/>
                                    <Route path="/account/trips" component={Trips}/>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={localStorage.getItem("pazardy-token") !== null?{display:"none"}:null} className={this.props.history.location.pathname !== "/account/login"?css(style.bg):null}>
                    <div className="container">
                        <Header/>
                        <div className={` ${css(style.content)}`}>
                            <div className={`${css(style.contentLeft)}`}>
                                <ControlLogin/>
                            </div>
                            <div className={`${css(style.contentRight)}`}>
                                <Switch>
                                    <Redirect exact from='/account' to='/account/login'/>
                                    <Route path="/account/login" component={Login}/>
                                    <Route path="/account/create" component={Create}/>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer/>
            </div>
        )
    }

}

const style = StyleSheet.create({
    bg: {
        background: "linear-gradient(to right, #ffffff 40%,#fafafa 40%)"
    },
    contentLeft: {
        width: "40%",
        height: "100%",
    },
    contentRight: {
        width: "60%"
    },
    content: {
        display: "flex",
        margin: "100px 0 0 0",
        padding: '0',
        '@media (max-width: 1199px)': {
            height: "100vh",
        },
        height: "100%",
        ':nth-child(1n) > div': {
            margin: "0",
        },
    },
});


export default withRouter(App)
