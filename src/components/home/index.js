import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import * as Categorie from "../../action/categorie"

import bg1 from "../sources/bgSection1.png"
import Slider from "react-slick";

import FixHeader from '../control/Fixheader'
import Header from '../control/header'
import Footer from '../control/footer'
import VisibilitySensor from "react-visibility-sensor";
import StarRatings from 'react-star-ratings';
import {store} from "../../index";
import {CATEGORIES} from "../../reducers/const";


const slide = [
    {
        id: "1",
        name: "Lays Chips",
        subtitle: "Subtitle"

    },
    {
        id: "2",
        name: "Lays Chips",
        subtitle: "Subtitle"

    },
    {
        id: "3",
        name: "Lays Chips",
        subtitle: "Subtitle"

    },
    {
        id: "4",
        name: "Lays Chips",
        subtitle: "Subtitle"

    },
    {
        id: "5",
        name: "Lays Chips",
        subtitle: "Subtitle"

    },
];

const carrerArr = [
    {
        img: 'carrarDow.png',
        num: "1",
        title: "DOWNLOAD",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?"
    },
    {
        img: 'carrerDe.png',
        num: "2",
        title: "DELIVERY",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?"
    },
    {
        img: 'carrerEa.png',
        num: "3",
        title: "EARN",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?"
    },
]

const NewsArr = [
    {
        titleNew: "OUR",
        titleNew2: "NEWS",
        descriptionNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
        imgNews: "photo-1.png",
        dataNews: "12/08/2018",
        imgTitleNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
    },
    {
        titleNew: "OUR",
        titleNew2: "NEWS",
        descriptionNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
        imgNews: "2-phone.png",
        dataNews: "13/08/2018",
        imgTitleNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
    },
    {
        titleNew: "OUR",
        titleNew2: "NEWS",
        descriptionNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
        imgNews: "photo-1.png",
        dataNews: "14/08/2018",
        imgTitleNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
    },
]

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: null,
            indexSlid: 0,
            NameSlide: "Restaurant",
            IdSlide: 1,
            categoriesName: null,
            isVisible: true,
            titleNew: "OUR",
            titleNew2: "NEWS",
            descriptionNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",
            imgNews: "photo-1.png",
            dataNews: "12/08/2018",
            imgTitleNews: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, nostrum?",

        };
        this.onChangeListSlide = this.onChangeListSlide.bind(this);
        this.onChangeListSlideBottom = this.onChangeListSlideBottom.bind(this)
    }


    async componentDidMount() {
        window.scrollTo(0, 0);
        let categoriesName = await Categorie.GetCategorieName();
        let categories = await Categorie.GetCategorie();
        console.log(categoriesName);
        console.log(categories);
        let index = 0;
        for (let key in categories) {
            categoriesName.Store_cateogires[index].cateogires = categories[key]
            index++
        }
        // console.log(categoriesName)
        // categoriesName.Store_cateogires.map((value,index)=>{
        //     categoriesName.Store_cateogires[index].concat(categories[index])
        // })
        // console.log(categoriesName.Store_cateogires.concat(categories))
        // console.log(categories)
        console.log(categoriesName.Store_cateogires)
        this.setState({
            categories: categoriesName.Store_cateogires
            // categoriesName
        })
        console.log(this.state)
    }

    onChangeIsVisible(isVisible) {
        this.setState({
            isVisible
        })
    }

    onChangeListSlide(value, id) {
        this.setState({
            NameSlide: value,
            IdSlide: id
        })
    }

    onChangeListSlideBottom(id) {
        if (NewsArr[id] === undefined) {
            alert("News end")
        } else {
            this.setState({
                titleNew: NewsArr[id].titleNew,
                titleNew2: NewsArr[id].titleNew2,
                descriptionNews: NewsArr[id].descriptionNews,
                imgNews: NewsArr[id].imgNews,
                dataNews: NewsArr[id].dataNews,
                imgTitleNews: NewsArr[id].imgTitleNews,
                indexSlid: id,
            })
        }
    }

    OnClickCat(value) {
        store.dispatch({
            type: CATEGORIES.CATEGORIES,
            payload: value
        })
    }


    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        // centerMode: true,
                        slidesToScroll: 1,
                    }
                }
            ]
        };

        // onClick={() => {this.OnClickCat(value.name);this.props.history.push(`/categories/` + value.id)}}
        let categories = this.state.categories !== null ? (this.state.categories.map((value, index) => {
            return (
                <ul style={value.id === 4 ? {marginBottom: 150} : null} key={index}
                    className="col-xl-3 col-md-6 top-level-menu">
                    <li>
                        <a style={{textTransform: "uppercase"}}>{value.name} <img
                            src={require('../sources/down-arrow.png')} alt="arrow"/></a>
                        {value.cateogires !== undefined ? (
                            <ul className="second-level-menu">
                                {value.cateogires.map((v, i) => {
                                    return (
                                        <li key={i}>
                                            <a onClick={() => {
                                                this.OnClickCat(v.english_name);
                                                this.props.history.push(`/categories/` + v.id)
                                            }}>{v.english_name}</a>
                                            <ul className="third-level-menu">
                                                {v.sub_categories.map((vv, i) => {
                                                    return (
                                                        <li onClick={() => {
                                                            this.OnClickCat(vv.english_name);
                                                            this.props.history.push(`/Subcategories/` + v.id + `&` + vv.id)
                                                        }} key={i}>
                                                            <a>{vv.english_name}</a>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </li>
                                    )
                                })}
                            </ul>
                        ) : null}

                    </li>
                </ul>
            )
        })) : (<div className="loader"></div>)
        let slidee = slide.map((value, index) => {
            return (
                <div key={index}>
                    <div className={css(style.slideBox)}>
                        <div>
                                    <span><StarRatings
                                        starSpacing="1px"
                                        starDimension="15px"
                                        rating={4}
                                        starRatedColor="#ff776d"
                                        numberOfStars={5}
                                        name='rating'
                                    /></span>
                            <img
                                src={require('../sources/pngpix-com-lays-potato-chips-pack-png-image-500x689-copy-11.png')}
                                alt="pngpix"/>
                            <div>
                                <p>$10</p>
                                <div>
                                    <div>-</div>
                                    <div>1</div>
                                    <div>+</div>
                                </div>
                            </div>
                        </div>
                        <div className={css(style.selectSlider)}>
                                    <span className={css(style.selectStyle)}>
                                        <select>
                                            <option>Store Name</option>
                                            <option>Store Name</option>
                                            <option>Store Name</option>
                                        </select>
                                        <div className="select-side">
                                            <img src={require('../sources/down-arrow.png')} alt="arrow"/>
                                        </div>
                                    </span>
                            <button>Add</button>
                        </div>
                    </div>
                    <div className={css(style.NameItems)}>
                        <div>
                            <p>{value.name}</p>
                            <h5>{value.subtitle}</h5>
                        </div>
                        <span>
                                    <StarRatings
                                        starSpacing="1px"
                                        starDimension="20px"
                                        rating={0}
                                        starRatedColor="#ff776d"
                                        numberOfStars={1}
                                        name='rating'
                                    />
                                </span>
                    </div>
                </div>
            )
        });
        let carrer = carrerArr.map((value, index) => {
            return (
                <span key={index}>
                    <div><img src={require(`../sources/${value.img}`)} alt="carrarDow"/></div>
                    <span>{value.num}</span>
                    <p>{value.title}</p>
                    <div>{value.description}</div>
                </span>
            )
        });
        let allCategories = this.state.categories !== null ? (this.state.categories.map((value, index) => {
            return (
                <option style={{textTransform: "uppercase"}} key={index}>{value.name}</option>
            )
        })) : null;
        return (
            <div>
                <VisibilitySensor
                    onChange={this.onChangeIsVisible.bind(this)}
                    partialVisibility={true}
                >
                    <Header/>
                </VisibilitySensor>
                {!this.state.isVisible && <FixHeader/>}
                <div className={` container ${css(style.content)}`}>
                    <h1 className={css(style.categoriesTitle)}><span>CA</span>TEGORIES</h1>
                    <div className={`col-12 row ${css(style.categoriesSelect)}`}>
                        {categories}
                    </div>
                    <div className={css(style.search)}>
                        <input placeholder="Search Products and Brands..."/>
                        <span className={css(style.selectStyle)}>
                            <select>
                                <option>All Categories</option>
                                {allCategories}
                            </select>
                            <div className="select-side">
                                <img src={require('../sources/down-arrow.png')} alt="arrow"/>
                            </div>
                        </span>
                        <div className={css(style.Search)}>
                            <img src={require('../sources/search.png')} alt="search"/>
                        </div>
                    </div>
                </div>
                <div className={css(style.sectionOneBg)}>
                    <div className={`container ${css(style.sectionOne)}`}>
                        <div className={`col-xl-6 col-md-12 ${css(style.WeWork)}`}>
                            <span>
                                How<br/>
                                We<br/>
                                Work<br/>
                            </span>
                            <p></p>
                            <div>Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit. Necessitatibus, nostrum?
                            </div>
                            <button>Get Started</button>
                        </div>
                        <div className={`col-xl-6 col-md-12 ${css(style.WeWorkList)}`}>
                            <ul>
                                <li>
                                    <img src={require('../sources/choose-icon.png')} alt="choose"/>
                                    <div>
                                        <span><p>1</p> CHOOSE</span>
                                        <h5>Choose shopping location</h5>
                                    </div>
                                </li>
                                <li><img src={require('../sources/search-icon.png')} alt="choose"/>
                                    <div>
                                        <span><p>2</p> SEARCH</span>
                                        <h5>Search for your favorite
                                            products from local shops
                                            nearby</h5>
                                    </div>
                                </li>
                                <li><img src={require('../sources/delivery-icon.png')} alt="choose"/>
                                    <div>
                                        <span><p>3</p> DELIVERY</span>
                                        <h5>Search for your favorite
                                            products from local shops
                                            nearby</h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div style={{marginBottom: 70}} className={` container ${css(style.sliders)}`}>
                    <div>
                        <p>Recent Purchased</p>
                        <h5>expand all</h5>
                    </div>
                    <Slider {...settings}>
                        {slidee}
                    </Slider>
                </div>
                <div className={` container ${css(style.sliders)}`}>
                    <div>
                        <p>Favorite products</p>
                        <h5>expand all</h5>
                    </div>
                    <Slider {...settings}>
                        {slidee}
                    </Slider>
                </div>
                <div className={css(style.nearby)}>
                    <span>Nearby Product</span>
                    <p></p>
                    <div>Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Quisquam, veniam.
                    </div>
                </div>
                <ul className={` container ${css(style.listSlide)}`}>
                    <li onClick={() => this.onChangeListSlide("Restaurant", 1)}
                        className={this.state.IdSlide === 1 ? css(style.ActiveListSlide) : css(style.listSlideli)}>RESTAURANT
                    </li>
                    <li onClick={() => this.onChangeListSlide("Bakery", 2)}
                        className={this.state.IdSlide === 2 ? css(style.ActiveListSlide) : css(style.listSlideli)}>BAKERY
                    </li>
                    <li onClick={() => this.onChangeListSlide("Supermarket", 3)}
                        className={this.state.IdSlide === 3 ? css(style.ActiveListSlide) : css(style.listSlideli)}>SUPERMARKET
                    </li>
                    <li onClick={() => this.onChangeListSlide("Pastery", 4)}
                        className={this.state.IdSlide === 4 ? css(style.ActiveListSlide) : css(style.listSlideli)}>PASTERY
                    </li>
                    <li onClick={() => this.onChangeListSlide("Cafe", 5)}
                        className={this.state.IdSlide === 5 ? css(style.ActiveListSlide) : css(style.listSlideli)}>CAFE
                    </li>
                    <li onClick={() => this.onChangeListSlide("Pharmacy", 6)}
                        className={this.state.IdSlide === 6 ? css(style.ActiveListSlide) : css(style.listSlideli)}>PHARMACY
                    </li>
                </ul>
                <div className={` container ${css(style.sliders)}`}>
                    <div>
                        <p>Neraby by {this.state.NameSlide}</p>
                    </div>
                    <Slider {...settings}>
                        {slidee}
                    </Slider>
                </div>
                <div className={css(style.downloadApp)}>
                    <div className={` container ${css(style.downloadAppContant)}`}>
                        <div className="col-xl-6 col-md-12">
                            <img src={require('../sources/2-phone.png')} alt="phone"/>
                        </div>
                        <div className={`col-xl-6 col-md-12 ${css(style.GetPazardy)}`}>
                            <span>
                                Get<br/>
                                The<br/>
                                Pazardy<br/>
                            </span>
                            <p></p>
                            <div>Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit. Necessitatibus, nostrum?
                            </div>
                            <h5>
                                <div><img src={require('../sources/app-store-copy.png')} alt="app"/></div>
                                <div><img src={require('../sources/google-play-copy.png')} alt="google"/></div>
                            </h5>
                        </div>
                    </div>
                </div>
                <div className={`container ${css(style.workAsCarrer)}`}>
                    <div>
                        <span>Work as Carrer</span>
                        <p></p>
                        <div>Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit. Necessitatibus, nostrum?
                        </div>
                    </div>
                    <div className={css(style.listWork)}>
                        {carrer}
                    </div>
                    <div>
                        <button>Register Now</button>
                    </div>
                </div>
                <div className={css(style.sliderBottomBg)}>
                    <div className={`container ${css(style.sectionOne)}`}>
                        <div className={`col-xl-6 col-md-12 ${css(style.NewsSlideBottom)}`}>
                            <div>
                            <span>
                                {this.state.titleNew}<br/>
                                {this.state.titleNew2}<br/>
                            </span>
                                <p></p>
                                <div>{this.state.descriptionNews}
                                </div>
                            </div>
                            <div>
                                <span onClick={() => this.onChangeListSlideBottom(this.state.indexSlid - 1)}><img
                                    src={require(`../sources/left-arrow.png`)} alt="carrarDow"/></span>
                                <span onClick={() => this.onChangeListSlideBottom(this.state.indexSlid + 1)}><img
                                    src={require(`../sources/right-arrow.png`)} alt="carrarDow"/></span>
                            </div>
                        </div>
                        <div className={`col-xl-6 col-md-12 ${css(style.ItemsSlideBottom)}`}>
                            <img src={require(`../sources/${this.state.imgNews}`)} alt="carrarDow"/>
                            <div>
                                <span>{this.state.imgTitleNews}</span>
                                <span>{this.state.dataNews}</span>
                            </div>
                            <NavLink
                                to={`/news/${this.state.indexSlid}`}>
                                <span>Read More <img src={require('../sources/right-arrowsmoll.png')}
                                                     alt="choose"/></span>
                            </NavLink>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

const style = StyleSheet.create({
    content: {
        height: "900px"
    },
    categoriesTitle: {
        marginTop: 200,
        fontFamily: "Lato",
        fontSize: 24,
        fontWeight: 700,
        textAlign: "center",
        ':nth-child(1n) > span': {
            textDecoration: "underline",
            textDecorationColor: "#444444",
        }
    },
    categoriesSelect: {
        justifyContent: 'space-between',
        margin: "150px 0 0 0",
        padding: 0,
        '@media (max-width: 1199px)': {
            margin: "100px 0 0 0",
        },
        ':nth-child(1n) > ul': {
            display: 'flex',
            justifyContent: "center",
            '@media (max-width: 1199px)': {
                marginBottom: 40
            }
        },
        ':nth-child(1n) > ul:last-child': {
            ':nth-child(1n) > li': {
                ':nth-child(1n) > ul': {
                    ':nth-child(1n) > li': {
                        ':nth-child(1n) > ul': {
                            right: 250,
                            '@media (max-width: 1199px)': {
                                right: -250,
                            }
                        }
                    }
                }
            }
        }
    },
    search: {
        display: 'flex',
        marginTop: "200px",
        height: 52,
        borderRadius: 4,
        alignItems: "center",
        border: "1px solid #acacac",
        ':nth-child(1n) > input': {
            paddingLeft: 29,
            borderRadius: 4,
            width: "80%",
            border: "none",
            ":focus": {
                outline: "none"
            },
        },
        ':nth-child(1n) > div': {
            cursor: "pointer",
            width: "6%",
            textAlign: 'center'
        }
    },
    Search: {
        opacity: 0.5,
        transition: "0.5s",
        ":hover": {
            transition: "0.5s",
            opacity: 1
        }

    },
    selectStyle: {
        width: "14%",
        display: 'flex',
        ':nth-child(1n) > div': {
            marginLeft: "130px",
            pointerEvents: 'none',
            position: 'absolute',
            '@media (max-width: 1199px)': {
                display: "none"
            }
        },
        ':nth-child(1n) > select': {
            width: "100%",
            paddingLeft: "15px",
            fontSize: 17,
            fontFamily: "Lato",
            color: "#a9a9a9",
            background: "none",
            border: 'none',
            borderLeft: "1px solid #555555",
            borderRight: "1px solid #555555",
            appearance: 'none',
            height: 29,
            ":focus": {
                outline: "none"
            },

        }
    },
    sectionOneBg: {
        height: "1278px",
        background: `url(${bg1}) no-repeat`,
        backgroundSize: "cover",
    },
    sectionOne: {
        '@media (max-width: 1199px)': {
            display: "block"
        },
        display: 'flex',
        alignItems: "center",
        height: "100%"
    },
    WeWork: {
        '@media (max-width: 1199px)': {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            textAlign: 'center',
            paddingTop: "160px"
        },
        ':nth-child(1n) > span': {
            fontSize: 72,
            fontFamily: "Lato",
            fontWeight: 300,
            '@media (max-width: 1199px)': {
                fontSize: 50,
            },
        },
        ':nth-child(1n) > p': {
            width: 114,
            margin: "50px 0",
            height: 2,
            background: "#464646"
        },
        ':nth-child(1n) > div': {
            width: 386,
            fontFamily: "Poppins",
            fontWeight: 300,
            fontSize: 15,
            letterSpacing: "0.6px",
            lineHeight: "40px",
            color: "#444444"
        },
        ':nth-child(1n) > button': {
            outline: "none",
            marginTop: "100px",
            color: "#fff",
            fontSize: 18,
            border: 'none',
            '@media (max-width: 1199px)': {
                marginTop: "20px",
            },
            transition: ".9s",
            ":hover": {
                color: "#000",
                background: "linear-gradient(to left, #ff3021 0%, #ff776d 100%)",
                transition: ".9s",

            },
            fontFamily: "Poppins",
            fontWeight: 400,
            borderRadius: 7,
            width: "224px",
            height: "53px",
            cursor: "pointer",
            backgroundImage: "linear-gradient(to right, #ff3021 0%, #ff776d 100%)",
            ":focus": {
                outline: "none"
            },
        }
    },
    WeWorkList: {
        ':nth-child(1n) > ul': {
            '@media (max-width: 1199px)': {
                display: 'flex',
                alignItems: 'center',
                flexDirection: "column"
            },
            listStyle: "none",
            ':nth-child(1n) > li': {
                '@media (max-width: 1199px)': {
                    padding: "20px 0",
                },
                display: "flex",
                alignItems: "center",
                padding: "120px 0",
                ':nth-child(1n) > div': {
                    paddingLeft: 100,
                    ':nth-child(1n) > span': {
                        display: "flex",
                        alignItems: 'baseline',
                        fontSize: "24px",
                        fontFamily: "Poppins",
                        fontWeight: 400,
                        ':nth-child(1n) > p': {
                            margin: '0 35px 0 0',
                            color: "#6ddba2",
                            fontSize: 48,
                        }
                    },
                    ':nth-child(1n) > h5': {
                        width: 275,
                        color: "#a9a9a9",
                        fontFamily: "Poppins",
                        fontSize: 20,
                        fontWeight: 400,
                    }
                }
            }
        }
    },
    sliders: {
        marginTop: 20,
        ':nth-child(1n) > div:first-child': {
            display: "flex",
            marginBottom: 40,
            ':nth-child(1n) > p': {
                marginRight: 79,
                color: "#444444",
                cursor: "pointer",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
            },
            ':nth-child(1n) > h5': {
                cursor: "pointer",
                color: "#444444",
                textDecoration: "underline",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
            }
        }
    },
    slideBox: {
        borderRadius: 7,
        border: "1px solid #e1e1e1",
        height: "300px",
        ':nth-child(1n) > div:first-child': {
            padding: "10px 10px 0 10px",
            borderBottom: "1px solid #e1e1e1",
            ':nth-child(1n) > img': {
                margin: "25px auto"
            },
            ':nth-child(1n) > div': {
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > p': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                },
                ':nth-child(1n) > div': {
                    display: "flex",
                    alignItems: "center",
                    height: "14px",
                    marginTop: 8,
                    ':nth-child(1n) > div': {
                        padding: "0 12.5px",
                        color: "#a9a9a9",
                        fontFamily: "Poppins",
                        fontSize: 14,
                        fontWeight: 300,
                    },
                    ':nth-child(1n) > div:last-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                    ':nth-child(1n) > div:first-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                }
            }
        }
    },
    selectSlider: {
        display: 'flex',
        alignItems: 'center',
        height: "calc(100% - 248px)",
        ':nth-child(1n) > span': {
            width: "80%",
            alignItems: 'center',
            height: "100%",
            ':nth-child(1n) > select': {
                color: "#000000",
                border: "none",
            }
        },
        ':nth-child(1n) > button': {
            cursor: "pointer",
            outline: "none",
            background: "none",
            border: "none",
            borderLeft: "1px solid #e1e1e1",
            color: "#6ddba2",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
        }
    },
    NameItems: {
        marginTop: 25,
        display: "flex",
        justifyContent: "space-between",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > p': {
                margin: 0,
                color: "#444444",
                fontSize: 18,
                fontFamily: "Poppins",
                fontWeight: 400
            },
            ':nth-child(1n) > h5': {
                color: "#a9a9a9",
                fontSize: 14,
                fontFamily: "Poppins",
                fontWeight: 300
            }
        }
    },
    nearby: {
        marginTop: 110,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        ':nth-child(1n) > span': {
            color: "#000000",
            fontFamily: "Lato",
            fontSize: 36,
            fontWeight: 300,
        },
        ':nth-child(1n) > p': {
            width: 114,
            margin: "45px 0",
            height: 2,
            backgroundColor: '#000000'
        },
        ':nth-child(1n) > div': {
            width: 389,
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 20,
            fontWeight: 400,
            letterSpacing: "0.6px",
            lineHeight: "40px",
            textAlign: 'center'
        }
    },
    listSlide: {
        marginBottom: 40,
        listStyle: 'none',
        display: "flex",
        justifyContent: "space-between",
        marginTop: 67,
        ':nth-child(1n) > li': {
            '@media (max-width: 1000px)': {
                fontSize: 18
            }
        }
    },
    listSlideli: {
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 24,
        fontWeight: 400,
        cursor: "pointer"
    },
    ActiveListSlide: {
        color: "#ff776d",
        textDecoration: "underline",
        fontFamily: "Poppins",
        fontSize: 24,
        fontWeight: 400,
        cursor: "pointer"
    },
    downloadApp: {
        marginTop: 90,
        height: 1100,
        display: 'flex',
        background: "#fafafa"
    },
    downloadAppContant: {
        '@media (max-width: 1199px)': {
            display: "block"
        },
        display: 'flex',
        alignItems: 'center',
        ':nth-child(1n) > div:first-child': {
            '@media (max-width: 1199px)': {
                display: 'flex',
                justifyContent: 'center'
            }
        }
    },
    GetPazardy: {
        fontFamily: "Lato",
        paddingLeft: 250,
        '@media (max-width: 1199px)': {
            display: 'flex',
            flexDirection: "column",
            alignItems: "center",
            padding: 0,
            textAlign: 'center'
        },
        ':nth-child(1n) > span': {
            '@media (max-width: 1199px)': {
                fontSize: 60,
            },
            fontSize: 72,
            fontWeight: 300,
            color: "#000000"
        },
        ':nth-child(1n) > p': {
            width: 114,
            margin: "30px 0",
            height: 2,
            background: "#464646"
        },
        ':nth-child(1n) > div': {
            width: "100%",
            fontFamily: "Poppins",
            '@media (max-width: 1199px)': {
                marginBottom: 20,
            },
            fontWeight: 300,
            fontSize: 15,
            marginBottom: 100,
            letterSpacing: "0.6px",
            lineHeight: "40px",
            color: "#444444"
        },
        ':nth-child(1n) > h5': {
            display: "flex",
            width: "100%",
            justifyContent: "space-between"
        }
    },
    workAsCarrer: {
        marginTop: 80,
        ':nth-child(1n) > div:first-child': {
            display: 'flex',
            flexDirection: "column",
            alignItems: "center",
            textAlign: 'center',
            ':nth-child(1n) > span': {
                fontSize: 72,
                fontWeight: 300,
                color: "#000000"
            },
            ':nth-child(1n) > p': {
                width: 114,
                margin: "30px 0",
                height: 2,
                background: "#464646"
            },
            ':nth-child(1n) > div': {
                width: 386,
                fontFamily: "Poppins",
                fontWeight: 300,
                fontSize: 15,
                marginBottom: 100,
                letterSpacing: "0.6px",
                lineHeight: "40px",
                color: "#444444"
            },
        },
        ':nth-child(1n) > div:last-child': {
            marginTop: 100,
            display: 'flex',
            justifyContent: 'center',
            ':nth-child(1n) > button': {
                cursor: "pointer",
                outline: "none",
                width: 224,
                height: 53,
                border: "none",
                borderRadius: 7,
                background: "linear-gradient(to right, #ff3021 0%, #ff776d 100%)",
                color: "#000000",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
                transition: ".9s",
                ":hover": {
                    color: "#fff",
                    background: "linear-gradient(to left, #ff3021 0%, #ff776d 100%)",
                    transition: ".9s",

                }
            }
        }
    },
    listWork: {
        display: 'flex',
        '@media (max-width: 1199px)': {
            flexDirection: "column"
        },
        justifyContent: 'space-around',
        ':nth-child(1n) > span': {
            '@media (max-width: 1199px)': {
                paddingBottom: 20,
            },
            display: "flex",
            flexDirection: 'column',
            alignItems: 'center',
            ':nth-child(1n) > span': {
                color: "#6ddba2",
                fontFamily: "Poppins",
                fontSize: 48,
                fontWeight: 400,
                padding: "30px 0",
            },
            ':nth-child(1n) > p': {
                color: "#000000",
                fontFamily: "Poppins",
                fontSize: 24,
                fontWeight: 400,
            },
            ':nth-child(1n) > div:last-child': {
                width: 281,
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 16,
                textAlign: 'center',
                fontWeight: 300,
            }
        }
    },
    sliderBottomBg: {
        '@media (max-width: 1199px)': {
            height: "100%",
        },
        marginTop: 90,
        height: 1080,
        display: 'flex',
        background: "#fafafa"
    },
    NewsSlideBottom: {
        display: 'flex',
        flexDirection: "column",
        '@media (max-width: 1199px)': {
            flexDirection: 'column-reverse'
        },
        ':nth-child(1n) > div:first-child': {
            '@media (max-width: 1199px)': {
                display: "flex",
                flexDirection: 'column',
                alignItems: 'center',
                textAlign: 'center',
            },
            ':nth-child(1n) > span': {
                fontSize: 72,
                fontFamily: "Lato",
                fontWeight: 300,
                '@media (max-width: 1199px)': {
                    fontSize: 50,
                },
            },
            ':nth-child(1n) > p': {
                width: 114,
                margin: "50px 0",
                height: 2,
                background: "#464646"
            },
            ':nth-child(1n) > div': {
                width: 386,
                fontFamily: "Poppins",
                fontWeight: 300,
                fontSize: 15,
                letterSpacing: "0.6px",
                lineHeight: "40px",
                color: "#444444"
            },
        },
        ':nth-child(1n) > div:last-child': {
            '@media (max-width: 1199px)': {
                marginTop: 20,
                marginBottom: 80,
                textAlign: 'center',
            },
            marginTop: 100,
            ':nth-child(1n) > span:first-child': {
                marginRight: 85
            },
            ':nth-child(1n) > span': {
                cursor: "pointer"
            }
        }
    },
    ItemsSlideBottom: {
        display: "flex",
        flexDirection: 'column',
        ':nth-child(1n) > div': {
            marginTop: 25,
            display: "flex",
            justifyContent: "space-between",
            color: "#aaaaaa",
            fontFamily: "Lato",
            fontWeight: 300,
            ':nth-child(1n) > span:first-child': {
                width: 330,
                fontSize: 20,

            },
            ':nth-child(1n) > span:last-child': {
                fontSize: 18,

            }
        },
        ':nth-child(1n) > a': {
            textDecoration: "none",
            ':nth-child(1n) > span': {
                marginTop: 50,
                color: "#ff776d",
                fontFamily: "Lato",
                fontSize: 20,
                fontWeight: 400,
                ':nth-child(1n) > img': {
                    paddingLeft: 20
                }
            }
        }
    }
});

export default (withRouter(Home))
