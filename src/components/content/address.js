import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';

import {MyMapComponent} from './map';
import Geocode from "react-geocode";
import connect from "react-redux/es/connect/connect";
import Autocomplete from "react-google-autocomplete";
import * as Addresss from '../../action/address'


Geocode.enableDebug();


class Address extends React.Component {
    constructor() {
        super();
        this.state = {
            markers: [
                {
                    lat: 0,
                    lng: 0
                }
            ],
            length: 432,
            address: "",
            city: '',
            place: '',
            Street: '',
            Building: '',
            Floot: '',
            Code: '',
            Appartment: '',
            city_name: '',
            district_name: '',
            street: '',
            building: '',
            floor: '',
            zip_code: '',
            apartment_number: '',
            long: null,
            lat: null
        };
        this.GetAddress = this.GetAddress.bind(this);

    }

    async componentDidMount() {
        await this.GetAddress()
        // this.handelMarker()
    }

    async GetAddress() {
        let res = await Addresss.GetArddress();
        if (res.addresses.length !== 0) {
            let marker = [
                {
                    lat: res.addresses[0].address_detail.lat,
                    lng: res.addresses[0].address_detail.long
                }
            ]
            this.setState({
                city_name: res.addresses[0].district.city.city_name,
                district_name: res.addresses[0].district.district_name,
                street: res.addresses[0].address_detail.street,
                building: res.addresses[0].address_detail.building,
                floor: res.addresses[0].address_detail.floor,
                zip_code: res.addresses[0].address_detail.zip_code,
                apartment_number: res.addresses[0].address_detail.apartment_number,
                long: res.addresses[0].address_detail.long,
                lat: res.addresses[0].address_detail.lat,
                markers: marker
            })
        }
    }


    // handelMarker(lat = this.props.store.location.code.latitude, lng = this.props.store.location.code.longitude) {
    //     let markers;
    //     markers = {
    //         lat: lat, lng: lng
    //     };
    //     this.setState({markers: markers});
    //     Geocode.setApiKey("AIzaSyDkoz0whWt9RBLL7rMoX-33weWkJUtX-bI");
    //
    //
    //     Geocode.fromLatLng(lat, lng).then(
    //         response => {
    //             let city = response.results[response.results.length - 2].formatted_address
    //             const address = response.results[0].formatted_address;
    //             this.setState({address: address, city})
    //         },
    //         error => {
    //             console.error(error);
    //         }
    //     );
    // }


    render() {
        let token = localStorage.getItem("pazardy-token")
        return (
            <div className={css(style.content)}>
                <div className={css(style.logo)}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div>
                    <div className={css(style.text)}>
                        <label className={css(style.labelfirst)}>Customer information</label>
                        <label className={css(style.labelNow)}>Shipping address</label>
                        <label className={css(style.label)}>Payment method</label>
                        <label style={{textAlign: "right"}} className={css(style.labelLast)}>Review &
                            Confirmation</label>
                    </div>
                    <div className={css(style.progressDiv)}>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progressNow)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>

                    </div>
                </div>
                <div>
                    <div className={css(style.mapBlock)}>
                        {this.state.long === null ? (<div style={{
                            justifyContent: "center",
                            fontSize: 24,
                        }}>You have not added an address yet</div>) : (
                            <div>
                                <MyMapComponent markers={this.state.markers}
                                                lat={this.state.lat}
                                                log={this.state.long}
                                                handelMarker={this.handelMarker}
                                                containerElement={<div className={css(style.map)}/>}
                                                mapElement={<div style={{height: `100%`, width: "430px"}}/>}
                                />

                                <div>
                                    <p>
                                        SHIPPING ADDRESS
                                    </p>
                                    <span>{this.state.city_name},{this.state.district_name}, {this.state.street},{this.state.building},{this.state.apartment_number}, {this.state.floor}, {this.state.zip_code}</span>
                                </div>
                            </div>
                        )}
                    </div>
                    <span className={css(style.addNewsMap)}>
                        <div>
                            +
                        </div>
                        <h5>Add new address</h5>
                    </span>
                    <div className={`col-12 row ${css(style.Login)}`}>
                        <div className={`col-12 ${css(style.LogoBody)}`}>
                            <p>SHIPPING ADDRESS</p>
                            <span>* REQUIRED</span>
                        </div>
                        <div className="col-12 row">
                            <div className={`col-5 ${css(style.InputBody)}`}>
                                <Autocomplete
                                    style={{width: '90%'}}
                                    placeholder="Country.City.District*"
                                    onPlaceSelected={(place) => {
                                        this.setState({
                                            place
                                        })
                                    }}
                                />
                                <input value={this.state.Street}
                                       onChange={(e) => this.setState({Street: e.target.value})} placeholder="Street*"/>
                                <input value={this.state.Building}
                                       onChange={(e) => this.setState({Building: e.target.value})}
                                       placeholder="Building*"/>
                            </div>
                            <div className={`col-5 offset-1 ${css(style.InputBody)}`}>
                                <input value={this.state.Appartment}
                                       onChange={(e) => this.setState({Appartment: e.target.value})}
                                       placeholder="Appartment*"/>
                                <input value={this.state.Floot} onChange={(e) => this.setState({Floot: e.target.value})}
                                       placeholder="Floot*"/>
                                <input value={this.state.Code} onChange={(e) => this.setState({Code: e.target.value})}
                                       placeholder="ZIP Code*"/>
                            </div>
                        </div>
                        <div className={css(style.SingIn)}>
                            <div>
                                <label className={css(style.labelCheck)}>
                                    {this.state.rememberMe ?
                                        <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                            <span className={css(style.IconSpan)}>

                                            </span>)
                                    }
                                    <input
                                        className={css(style.check)}
                                        type="checkbox"
                                        checked={this.state.rememberMe}
                                        onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                                    />
                                    Save address
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css(style.footer)}>
                    <div>
                        {token === null ? (
                            <NavLink
                                to="/buy/info">
                                <div>
                                    <img src={require('../sources/right-arrow .png')} alt="arrow"/>
                                    <span>Return to customer information</span>
                                </div>
                            </NavLink>) : <div></div>}
                        <NavLink
                            to="/buy/NoLogin">
                            <button>
                                Continue
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 51,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    InputBody: {
        padding: 0,
        ':nth-child(1n) > input': {
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > select': {
            color: 'rgb(154, 154, 154)',
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },
    Login: {
        padding: "0 0 0 15px"
    },
    labelCheck: {
        paddingRight: 50,
        fontSize: 12,
        cursor: 'pointer',
        fontWeight: 300,
        lineHeight: 4,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    checkIconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    forgot: {
        cursor: "pointer",
        textDecoration: "underline",
        width: 105,
        height: 15,
        fontSize: 12,
        fontWeight: 300,
        lineHeight: 2,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    IconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        border: "solid 2px rgb(235, 235, 235)",
        borderRadius: '5px',
        cursor: 'pointer',
    },
    check: {
        display: 'none',
    },
    checkIcon: {
        textAlign: "center",
        paddingTop: "2px",
        fontSize: '10px',
        width: '20px',
        height: '18px',
        border: "solid 2px rgb(109, 219, 162)",
        borderRadius: '5px',
        backgroundColor: "rgb(109, 219, 162)",
        color: '#fff'
    },
    SingIn: {
        ':nth-child(1n) > button': {
            width: 170,
        }
    },
    Button: {
        border: "none",
        backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
        borderRadius: 7,
        height: 53,
        color: "rgb(255, 255, 255)",
        fontFamily: "Poppins",
        fontSize: 18,
        lineHeight: 2.44,
        letterSpacing: 0.4,
    },
    addNewsMap: {
        paddingBottom: 25,
        marginTop: 50,
        display: "flex",
        alignItems: "center",
        cursor: "pointer",
        ':nth-child(1n) > div': {
            height: 45,
            width: 45,
            backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
            borderRadius: 50,
            display: "flex",
            alignItems: 'center',
            justifyContent: "center",
            fontSize: 24,
            color: "#fff",
            fontWeight: 300
        },
        ':nth-child(1n) > h5': {
            fontSize: 14,
            color: "#a9a9a9",
            margin: 0,
            paddingLeft: 7,
            fontWeight: "normal"
        }
    },
    mapBlock: {
        ':nth-child(1n) > div': {
            marginTop: 26,
            display: "flex",
            alignItems: "center",
            '@media (max-width: 1025px)': {
                flexDirection: "column"
            },
            ':nth-child(1n) > div': {
                ':nth-child(1n) > p': {
                    fontSize: 18,
                    fontWeight: 'normal',
                    lineHeight: 1.56,
                    letterSpacing: 0.5,
                    color: "#000"
                },
                ':nth-child(1n) > span': {
                    width: 240,
                    fontSize: 18,
                    fontWeight: 100,
                    lineHeight: 1.33,
                    letterSpacing: 0.5,
                    color: "#686868"
                }
            }
        }
    },
    map: {
        marginRight: "10%",
        height: 224,
        width: 430,
    },
    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    progressDiv: {
        width: "92%",
        margin: "auto",
        justifyContent: "center",
        display: 'flex',
        alignItems: "center",
    },
    progressNow: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(161, 255, 211)",
        borderRadius: 50
    },
    progress: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(238, 238, 238)",
        borderRadius: 50
    },
    progressLine: {
        height: 1,
        width: "24%",
        backgroundColor: "rgb(238, 238, 238)"
    },
    labelNow: {
        textAlign: "center",
        color: "rgb(161, 255, 211)",
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelLast: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "right",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelfirst: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "left",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    label: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "center",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    text: {
        display: "flex",
        width: "100%",
        justifyContent: "space-between",
    },
    footer: {
        position: "relative",
        zIndex: 1000,
        display: 'flex',
        flexDirection: "column",
        justifyContent: "flex-end",
        flex: 1,
        ':nth-child(1n) > div': {
            justifyContent: "space-between",
            display: "flex",
            paddingBottom: "54px",
            alignItems: "flex-end",
            ':nth-child(1n) > a': {
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > span': {
                        fontSize: 14,
                        fontWeight: 300,
                        fontFamily: "Poppins",
                        lineHeight: 2,
                        letterSpacing: "0.2px",
                        color: "RGB(68,68,68)",
                        paddingLeft: 23
                    }
                },
                ":hover": {
                    textDecoration: "none",

                },
                ':nth-child(1n) > button': {
                    cursor: "pointer",
                    outline: "none",
                    border: "none",
                    backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
                    borderRadius: 7,
                    width: 189,
                    height: 53,
                    color: "rgb(255, 255, 255)",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: 2.44,
                    letterSpacing: 0.4,
                }
            }
        }
    }
});


export default connect(store => ({store: store}))(withRouter(Address))
