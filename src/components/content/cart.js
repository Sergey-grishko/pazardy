import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';

import * as CartList from "../../action/cart";
import {store} from "../../index";
import {CART} from "../../reducers/const";
import connect from "react-redux/es/connect/connect";
import {toast} from "react-toastify";


class Cart extends React.Component {
    constructor() {
        super()
        this.state = {
            rememberMe: true,
            Subscribe: false,
            CartArr: null
        }
    }

    async componentDidMount() {
        let res = await CartList.GetCartLoged()
        this.setState({
            CartArr: res.cart_items
        })
    }

    async DeleteItems(id) {
        console.log(id)
        let res = await CartList.DeletItemCart(id)
        this.setState({
            CartArr: res.cart_items
        })
        store.dispatch({
            type: CART.ITEMSCART,
            payload: res.cart_items.length
        });
        toast.success("The product has been deleted");
    }

    render() {
        let iteamx = this.state.CartArr !== null ? (this.state.CartArr.map((value, index) => {
            return (
                <li key={index}>
                    <img src={require('../sources/textBlock.png')} alt="arrow"/>
                    <div>
                        <div className={css(style.ItemsName)}>
                            <span>Iteamx x {value.quantity}</span>
                            <h6>20$</h6>
                        </div>
                        <div className={css(style.storeName)}>
                            <span>Store Name</span>
                            <img onClick={()=>this.DeleteItems(value.id)} src={require('../sources/cross.png')} alt="cross"/>
                        </div>
                        <div className={css(style.select)}>
                            <select className={css(style.selectItem)}>
                                <option>2</option>
                            </select>
                            <div className={css(style.selectItemImg)}>
                                <img src={require('../sources/down-arrow.png')} alt="arrow"/>
                            </div>

                            <select className={css(style.selectItemName)}>
                                <option>other available store in your ares</option>
                            </select>
                            <div className={css(style.selectItemNameImg)}>
                                <img src={require('../sources/down-arrow.png')} alt="arrow"/>
                            </div>
                        </div>
                    </div>
                </li>
            )
        })) : <div className="loader"></div>

        return (
            <div className={css(style.content)}>
                <div className={`${css(style.logo)}`}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div className={`${css(style.body)}`}>
                    <span>
                        SHOPPING CART({this.state.CartArr === null ? null : this.state.CartArr.length})
                    </span>
                    <ul className={css(style.listItem)}>
                        {iteamx}
                    </ul>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    body: {
        padding: "0 0 0 15px",
        flex: "0 0 0",
        justifyContent: "space-between",
        ':nth-child(1n) > input': {
            fontFamily: "Lato",
            fontSize: "18px",
            marginBottom: "49px",
            lineHeight: 2.44,
            letterSpacing: "normal",
            color: "rgb(68, 68, 68)"
        }
    },
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 51,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    Button: {
        border: "none",
        backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
        borderRadius: 7,
        height: 53,
        color: "rgb(255, 255, 255)",
        fontFamily: "Poppins",
        fontSize: 18,
        lineHeight: 2.44,
        letterSpacing: 0.4,
    },
    listItem: {
        height:"100%",
        padding: 0,
        ':nth-child(1n) > li': {
            marginTop: 49,
            listStyle: "none",
            display: "flex",
            ':nth-child(1n) > img': {
                height: 150,
                width: 150
            },
            ':nth-child(1n) > div': {
                display: "flex",
                width: 250,
                padding: "21px 0 0 23px",
                flexDirection: "column",
                justifyContent: "space-between"
            }
        }
    },
    ItemsName: {
        height: 27,
        fontSize: 18,
        fontWeight: 300,
        lineHeight: 1,
        letterSpacing: 0.4,
        color: "rgb(68, 68, 68)",
        width: "80%",
        ':nth-child(1n) > h6': {
            textAlign: "right",
            color: "rgb(109, 219, 162)"
        }
    },
    storeName: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        fontSize: 18,
        fontWeight: 300,
        letterSpacing: 0.4,
        color: "rgb(68, 68, 68)",
        ':nth-child(1n) > img': {
            cursor: "pointer",
            height: 16,
            width: 16
        },
    },
    select: {
        display: "flex",
        justifyContent: "space-between",
        fontSize: 14,
        ':nth-child(1n) > div': {
            pointerEvents: 'none',
            position: 'absolute',
        },
    },
    selectItem: {
        border: "solid 1px rgba(85, 85, 85,0.5)",
        paddingLeft: 5,
        width: "20%",
        appearance: 'none',
        color: "rgb(68, 68, 68)"
    },
    selectItemImg: {
        background: "#fff",
        margin: "1px 0 0 30px",
    },
    selectItemName: {
        border: "solid 1px rgba(85, 85, 85,0.5)",
        appearance: 'none',
        width: "75%",
        padding: "0 0 0 5px",
        color: "rgb(68, 68, 68)"
    },
    selectItemNameImg: {
        background: "#fff",
        margin: "1px 0 0 213px",
        padding: "0 1px 0 2px"
    },
});


export default connect(store => ({store: store}))(withRouter(Cart))
