import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import visa from '../sources/visalogo.png';


class PaymentLogin extends React.Component {
    constructor() {
        super()
        this.state = {
            rememberMe: true,
        }
    }

    componentDidMount(){
        if(localStorage.getItem("pazardy-token") === null){
            this.props.history.push("/buy/NoLogin")
        }
    }

    render() {

        return (
            <div className={css(style.content)}>
                <div className={css(style.logo)}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div>
                    <div className={css(style.text)}>
                        <label  className={css(style.labelfirst)}>Customer information</label>
                        <label  className={css(style.label)}>Shipping address</label>
                        <label  className={css(style.labelNow)}>Payment method</label>
                        <label style={{textAlign:"right"}} className={css(style.labelLast)}>Review & Confirmation</label>
                    </div>
                    <div className={css(style.progressDiv)}>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progressNow)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>

                    </div>
                </div>
                <div>
                    <div className={css(style.CardBlock)}>
                        <select>
                            <option>VISA</option>
                        </select>
                        <div className={`col-12 row ${css(style.cardBody)}`}>
                            <div  className={`col-5 row`}>
                                <p>Name on Card</p>
                                <span>John Doe</span>
                            </div>
                            <div  className={`col-7 row`}>
                                <p>Billing Address</p>
                                <span>
                                    John Doe<br/>
                                    497 Evegreen Rd.Rosevile<br/>
                                    CA 95673 apt 2.<br/>
                                    floor 5,<br/>
                                    3457899335<br/>
                                </span>
                            </div>
                        </div>
                        <div className={css(style.cardControle)}>
                            <span>Delete</span>
                            <span>Edit</span>
                        </div>
                    </div>
                    <span className={css(style.addNewsMap)}>
                        <div>
                            +
                        </div>
                        <h5>Add new cart</h5>
                    </span>
                    <div className={`col-12 row ${css(style.Login)}`}>
                        <div className={`col-12 ${css(style.LogoBody)}`}>
                            <p>CREDIT CARD INFO</p>
                            <span>* REQUIRED</span>
                        </div>
                        <div className="col-12 row">
                            <div className={`col-5 ${css(style.InputBody)}`}>
                                <select>
                                    <option defaultChecked={true} hidden={true}>Cardholder Name*</option>
                                    <option>UA</option>
                                </select>
                                <input placeholder="Card Number*"/>
                                <input placeholder="Address*"/>
                            </div>
                            <div className={`col-5 offset-1 ${css(style.InputBody)}`}>
                                <input placeholder="Expire Data*"/>
                                <input placeholder="CVV*"/>
                            </div>
                        </div>
                        <div className={css(style.SingIn)}>
                            <div>
                                <label className={css(style.labelCheck)}>
                                    {this.state.rememberMe ?
                                        <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                            <span className={css(style.IconSpan)}>

                                            </span>)
                                    }
                                    <input
                                        className={css(style.check)}
                                        type="checkbox"
                                        checked={this.state.rememberMe}
                                        onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                                    />
                                    Save address
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css(style.footer)}>
                    <div>
                        <NavLink
                            to="/buy/address">
                            <div>
                                <img src={require('../sources/right-arrow .png')} alt="arrow"/>
                                <span>Return to shipping address</span>
                            </div>
                        </NavLink>
                        <NavLink
                            to="/buy/review">
                            <button>
                                Continue
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    cardControle:{
        display:"flex",
        justifyContent:"flex-end",
        ':nth-child(1n) > span': {
            fontSize:13,
            paddingRight:24,
            color: "#a9a9a9",
            textDecoration:"underline",
            cursor:"pointer"
        }
    },
    cardBody:{
        display:"flex",
        justifyContent:"space-between",
        margin:0,
        padding:0,
        ':nth-child(1n) > div': {
            display:"flex",
            flexDirection:"column",
            padding:"15px 26px 0 26px",
            ':nth-child(1n) > p': {
                margin:0,
                fontSize: 16,
                fontWeight: 300,
                lineHeight: 3,
                letterSpacing: "0.5px",
                color: "#a9a9a9"
            },
            ':nth-child(1n) > span': {
                fontSize:14,
                fontWeight: 100,
                lineHeight: 1.71,
                letterSpacing: 0.4,
                color: "#a9a9a9"
            }
        }
    },
    CardBlock:{
        height:246,
        width:430,
        margin:"26px auto",
        boxShadow: "0px 5px 2.7px 0.3px rgba(174, 175, 180, 0.07)",
        backgroundColor: "#ffffff",
        border: "solid 1px #fafafa",
        ':nth-child(1n) > select': {
            border:"none",
            borderBottom:"1px solid #eeeeee",
            width:"100%",
            height:42,
            ":focus":{
                outline:"none"
            }
        }
    },
    addNewsMap: {
        paddingBottom:25,
        display: "flex",
        alignItems: "center",
        cursor: "pointer",
        ':nth-child(1n) > div': {
            height: 45,
            width: 45,
            backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
            borderRadius: 50,
            display: "flex",
            alignItems: 'center',
            justifyContent: "center",
            fontSize: 24,
            color: "#fff",
            fontWeight: 300
        },
        ':nth-child(1n) > h5': {
            fontSize: 14,
            color: "#a9a9a9",
            margin: 0,
            paddingLeft: 7,
            fontWeight: "normal"
        }
    },
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 51,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    InputBody: {
        padding: 0,
        ':nth-child(1n) > input': {
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > select': {
            color: 'rgb(154, 154, 154)',
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },
    Login: {
        padding: "0 0 0 15px"
    },
    labelCheck: {
        paddingRight: 50,
        fontSize: 12,
        cursor: 'pointer',
        fontWeight: 300,
        lineHeight: 4,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    checkIconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    forgot: {
        cursor: "pointer",
        textDecoration: "underline",
        width: 105,
        height: 15,
        fontSize: 12,
        fontWeight: 300,
        lineHeight: 2,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    IconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        border: "solid 2px rgb(235, 235, 235)",
        borderRadius: '5px',
        cursor: 'pointer',
    },
    check: {
        display: 'none',
    },
    checkIcon: {
        textAlign: "center",
        paddingTop: "2px",
        fontSize: '10px',
        width: '20px',
        height: '18px',
        border: "solid 2px rgb(109, 219, 162)",
        borderRadius: '5px',
        backgroundColor: "rgb(109, 219, 162)",
        color: '#fff'
    },
    SingIn: {
        ':nth-child(1n) > button': {
            width: 170,
        }
    },
    Button: {
        border: "none",
        backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
        borderRadius: 7,
        height: 53,
        color: "rgb(255, 255, 255)",
        fontFamily: "Poppins",
        fontSize: 18,
        lineHeight: 2.44,
        letterSpacing: 0.4,
    },
    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    progressDiv: {
        width:"92%",
        margin:"auto",
        justifyContent:"center",
        display: 'flex',
        alignItems: "center",
    },
    progressNow: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(161, 255, 211)",
        borderRadius: 50
    },
    progress: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(238, 238, 238)",
        borderRadius: 50
    },
    progressLine: {
        height: 1,
        width: "24%",
        backgroundColor: "rgb(238, 238, 238)"
    },
    labelNow:{
        textAlign:"center",
        color:"rgb(161, 255, 211)",
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelLast:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"right",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelfirst:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"left",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    label:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"center",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    text: {
        display: "flex",
        width:"100%",
        justifyContent:"space-between",
    },
    footer: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: "flex-end",
        flex: 1,
        ':nth-child(1n) > div': {
            justifyContent: "space-between",
            display: "flex",
            paddingBottom: "54px",
            alignItems: "flex-end",
            ':nth-child(1n) > a': {
                position:"relative",
                zIndex:1000,
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > span': {
                        fontSize: 14,
                        fontWeight: 300,
                        fontFamily: "Poppins",
                        lineHeight: 2,
                        letterSpacing: "0.2px",
                        color: "RGB(68,68,68)",
                        paddingLeft: 23
                    }
                },
                ":hover": {
                    textDecoration: "none",

                },
                ':nth-child(1n) > button': {
                    cursor:"pointer",
                    outline:"none",
                    border: "none",
                    backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
                    borderRadius: 7,
                    width: 189,
                    height: 53,
                    color: "rgb(255, 255, 255)",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: 2.44,
                    letterSpacing: 0.4,
                }
            }
        }
    }
});

export default (withRouter(PaymentLogin))
