import React from 'react';
import {withGoogleMap, GoogleMap, Marker} from "react-google-maps";

export const MyMapComponent =  withGoogleMap((props) => {
    console.log(props)
    return (
        <GoogleMap
            // onClick={(e) => {
            //     props.handelMarker(e.latLng.lat(), e.latLng.lng())
            // }}
            defaultZoom={15}
            defaultCenter={{lat:Number(props.lat),lng:  Number(props.log)}}
        >
            <Marker
                position={{lat: props.markers[0].lat, lng: props.markers[0].lng}}
                draggable={false}
            />
        </GoogleMap>
    )
});
