import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import StarRatings from 'react-star-ratings';


class Review extends React.Component {
    render() {
        return (
            <div className={css(style.content)}>
                <div className={css(style.logo)}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div>
                    <div className={css(style.text)}>
                        <label className={css(style.labelfirst)}>Customer information</label>
                        <label className={css(style.label)}>Shipping address</label>
                        <label className={css(style.label)}>Payment method</label>
                        <label style={{textAlign: "right"}} className={css(style.labelLast)}>Review &
                            Confirmation</label>
                    </div>
                    <div className={css(style.progressDiv)}>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progressNow)}></div>

                    </div>
                </div>
                <div className={css(style.reviewList)}>
                    <div className={css(style.reviewName)}>
                        <div>DELIVERY TIME</div>
                        <div>SUPERMARKET</div>
                        <div>DELIVERY ADDRESS</div>
                        <div>STATUS</div>
                    </div>
                    <div className={css(style.reviewContent)}>
                        <div className={css(style.reviewColor)}>10-15 min</div>
                        <div><StarRatings
                            starSpacing="1px"
                            starDimension="15px"
                            rating={5}
                            starRatedColor="#ff776d"
                            numberOfStars={5}
                            name='rating'
                        /></div>
                        <h5>497 Evergreen Rd. Roseville,<br/>CA 95673 apt 2, floor 5</h5>
                        <div  className={css(style.reviewColor)}>Accepted</div>
                    </div>
                    {/*<ul className={css(style.reviewList)}>*/}
                    {/*<li>*/}
                    {/*<div>DELIVERY TIME</div>*/}
                    {/*<span>10-15 min</span></li>*/}
                    {/*<li>*/}
                    {/*<div>SUPERMARKET</div>*/}
                    {/*<span></span></li>*/}
                    {/*<li>*/}
                    {/*<div>DELIVERY ADDRESS</div>*/}
                    {/*<span>497 Evergreen Rd. Roseville,<br/>CA 95673 apt 2, floor 5</span></li>*/}
                    {/*<li>*/}
                    {/*<div>STATUS</div>*/}
                    {/*<span>Accepted</span></li>*/}
                    {/*</ul>*/}
                </div>
                <div className={css(style.footer)}>
                    <div>
                        <NavLink
                            to="/buy/PLogin">
                            <div>
                                <img src={require('../sources/right-arrow .png')} alt="arrow"/>
                                <span>Return to payment method</span>
                            </div>
                        </NavLink>
                        <a>
                            <button>
                                Place your order
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    reviewColor:{
        fontSize:16,
        color:"#a1ffd3",
    },
    reviewName: {
        marginRight: 100,
        ':nth-child(1n) > div': {
            fontSize: 16,
            marginBottom: 52
        }
    },
    reviewContent: {
        ':nth-child(1n) > div': {
            marginBottom: 52
        },
        ':nth-child(1n) > h5': {
            fontWeight:"normal",
            marginBottom:44,
            fontSize:14
        }
    },
    reviewList: {
        marginTop: 100,
        display: "flex",
        flexDirection: "row",
    },
    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    progressDiv: {
        width: "92%",
        margin: "auto",
        justifyContent: "center",
        display: 'flex',
        alignItems: "center",
    },
    progressNow: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(161, 255, 211)",
        borderRadius: 50
    },
    progress: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(238, 238, 238)",
        borderRadius: 50
    },
    progressLine: {
        height: 1,
        width: "24%",
        backgroundColor: "rgb(238, 238, 238)"
    },
    labelNow: {
        textAlign: "center",
        color: "rgb(161, 255, 211)",
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelLast: {
        color: "rgb(161, 255, 211)",
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "right",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelfirst: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "left",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    label: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "center",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    text: {
        display: "flex",
        width: "100%",
        justifyContent: "space-between",
    },
    footer: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: "flex-end",
        flex: 1,
        ':nth-child(1n) > div': {
            justifyContent: "space-between",
            display: "flex",
            paddingBottom: "54px",
            alignItems: "flex-end",
            ':nth-child(1n) > a': {
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > span': {
                        fontSize: 14,
                        fontWeight: 300,
                        fontFamily: "Poppins",
                        lineHeight: 2,
                        letterSpacing: "0.2px",
                        color: "RGB(68,68,68)",
                        paddingLeft: 23
                    }
                },
                ":hover": {
                    textDecoration: "none",

                },
                ':nth-child(1n) > button': {
                    cursor:"pointer",
                    outline:"none",
                    border: "none",
                    backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
                    borderRadius: 7,
                    width: 189,
                    height: 53,
                    color: "rgb(255, 255, 255)",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: 2.44,
                    letterSpacing: 0.4,
                }
            }
        }
    }
});

export default (withRouter(Review))
