import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';


class PaymentLogout extends React.Component {
    constructor() {
        super()
        this.state = {
            rememberMe: true,
        }
    }
    render() {
        return (
            <div className={css(style.content)}>
                <div className={css(style.logo)}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div>
                    <div className={css(style.text)}>
                        <label  className={css(style.labelfirst)}>Customer information</label>
                        <label  className={css(style.label)}>Shipping address</label>
                        <label  className={css(style.labelNow)}>Payment method</label>
                        <label style={{textAlign:"right"}} className={css(style.labelLast)}>Review & Confirmation</label>
                    </div>
                    <div className={css(style.progressDiv)}>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progressNow)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>

                    </div>
                </div>
                <div className={` ${css(style.body)}`}>
                    <div className={` ${css(style.Card)}`}>
                        <div className={` ${css(style.LogoBody)}`}>
                            <p>CREDIT CARD INFO</p>
                            <span>* REQUIRED</span>
                        </div>
                        <div className={` ${css(style.InputBody)}`}>
                            <select>
                                <option defaultChecked={true} hidden={true}>Cardholder Name*</option>
                                <option>UA</option>
                            </select>
                            <input placeholder="Card Number*"/>
                            <input placeholder="Address*"/>
                            <input placeholder="Expire Data*"/>
                            <input placeholder="CVV*"/>
                        </div>
                        <div className={css(style.SingIn)}>
                            <div>
                                <label className={css(style.labelCheck)}>
                                    {this.state.rememberMe ?
                                        <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                            <span  className={css(style.IconSpan)}>

                                            </span>)
                                    }
                                    <input
                                        className={css(style.check)}
                                        type="checkbox"
                                        checked={this.state.rememberMe}
                                        onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                                    />
                                    Save card
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className={` ${css(style.Address)}`}>
                        <div className={` ${css(style.LogoBody)}`}>
                            <p>BILLING ADDRESS</p>
                            <span>* REQUIRED</span>
                        </div>
                        <div className={` ${css(style.InputBody)}`}>
                            <input placeholder="Full Name*"/>
                            <input placeholder="Address1*"/>
                            <input placeholder="Address2*"/>
                            <input placeholder="Country. Region*"/>
                            <input placeholder="ZIP Cod*"/>
                        </div>
                    </div>
                </div>
                <div className={css(style.footer)}>
                    <div>
                        <NavLink
                            to="/buy/info">
                            <div>
                                <img src={require('../sources/right-arrow .png')} alt="arrow"/>
                                <span>Return to shipping address</span>
                            </div>
                        </NavLink>
                        <NavLink
                            to="/buy/review">
                            <button>
                                Continue
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 51,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    body:{
        display:'flex',
        height:"100vh",
        alignItems:"center",
    },
    Card:{
      display:"flex",
      marginRight:10,
      flexDirection:"column",
      width:"50%"
    },
    Address:{
        display:"flex",
        margin:"-56px 0 0 10px",
        flexDirection:"column",
        width:"50%"
    },
    labelCheck: {
        paddingRight:50,
        fontSize: 12,
        cursor: 'pointer',
        fontWeight: 300,
        lineHeight: 4,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    checkIconSpan:{
        marginTop:"-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    forgot:{
        cursor:"pointer",
        textDecoration:"underline",
        width: 105,
        height: 15,
        fontSize: 12,
        fontWeight: 300,
        lineHeight: 2,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    IconSpan:{
        marginTop:"-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        border: "solid 2px rgb(235, 235, 235)",
        borderRadius: '5px',
        cursor: 'pointer',
    },
    check: {
        display: 'none',
    },
    checkIcon: {
        textAlign:"center",
        paddingTop:"2px",
        fontSize: '10px',
        width: '20px',
        height: '18px',
        border: "solid 2px rgb(109, 219, 162)",
        borderRadius: '5px',
        backgroundColor: "rgb(109, 219, 162)",
        color: '#fff'
    },
    InputBody: {
        padding: 0,
        ':nth-child(1n) > input': {
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > select': {
            color: 'rgb(154, 154, 154)',
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },

    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    progressDiv: {
        width:"92%",
        margin:"auto",
        justifyContent:"center",
        display: 'flex',
        alignItems: "center",
    },
    progressNow: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(161, 255, 211)",
        borderRadius: 50
    },
    progress: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(238, 238, 238)",
        borderRadius: 50
    },
    progressLine: {
        height: 1,
        width: "24%",
        backgroundColor: "rgb(238, 238, 238)"
    },
    labelNow:{
        textAlign:"center",
        color:"rgb(161, 255, 211)",
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelLast:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"right",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelfirst:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"left",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    label:{
        color: 'rgb(68, 68, 68)',
        overflow:"hidden",
        whiteSpace:"nowrap",
        textOverflow:"ellipsis",
        fontSize: "1.5vh",
        width:"23.5%",
        textAlign:"center",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    text: {
        display: "flex",
        width:"100%",
        justifyContent:"space-between",
    },
    footer: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: "flex-end",
        flex: 1,
        ':nth-child(1n) > div': {
            justifyContent: "space-between",
            display: "flex",
            paddingBottom: "54px",
            alignItems: "flex-end",
            ':nth-child(1n) > a': {
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > span': {
                        fontSize: 14,
                        fontWeight: 300,
                        fontFamily: "Poppins",
                        lineHeight: 2,
                        letterSpacing: "0.2px",
                        color: "RGB(68,68,68)",
                        paddingLeft: 23
                    }
                },
                ":hover": {
                    textDecoration: "none",

                },
                ':nth-child(1n) > button': {
                    cursor:"pointer",
                    outline:"none",
                    border: "none",
                    backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
                    borderRadius: 7,
                    width: 189,
                    height: 53,
                    color: "rgb(255, 255, 255)",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: 2.44,
                    letterSpacing: 0.4,
                }
            }
        }
    }
});

export default (withRouter(PaymentLogout))
