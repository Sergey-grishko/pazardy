import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import * as create from "../../action/auth";
import {toast} from "react-toastify";
import Modal from "react-modal";

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        padding: 0,
        marginRight: '-50%',
        borderRadius: 0,
        border: "none",
        width: 473,
        height: 344,
        boxShadow: "0 5px 9px 1px rgba(174, 175, 180, 0.25)",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "RGBA(0,0,0,0.5)",
    }
};

class CustomerInfo extends React.Component {
    constructor() {
        super()
        this.state = {
            rememberMe: true,
            Subscribe: false,
            password: "",
            email: "",
            Telephone: '',
            Phone: "",
            First: "",
            Last: "",
            PasswordReg: "",
            RePassword: "",
            EmailReg: "",
        }
        this.SignUp = this.SignUp.bind(this);
        this.SignIn = this.SignIn.bind(this)
        this.handleModal = this.handleModal.bind(this)
        this.Verify = this.Verify.bind(this)
    }

    componentDidMount() {
        if (localStorage.getItem("pazardy-token") !== null) {
            this.props.history.push("/buy/address")
        }
    }

    async SignIn() {
        try {
            if (this.state.First === "" || this.state.First.length <= 2) throw new Error("The field FirstName is very short. Minimum number of characters 3")
            if (this.state.Last === "" || this.state.Last.length <= 2) throw new Error("The field LastName is very short. Minimum number of characters 3")
            if (this.state.EmailReg === "") throw new Error("The field Email is empty")
            if (this.state.PasswordReg === "" || this.state.PasswordReg.length <= 6) throw new Error("The field Password is very short. Minimum number of characters 7")
            if (this.state.RePassword === "" || this.state.RePassword.length <= 6) throw new Error("The field RePassword is very short. Minimum number of characters 7")
            if (this.state.Phone === "") throw new Error("The field Phone is empty")
            if (this.state.Telephone === "") throw new Error("The field Telephone is empty")
            if (this.state.PasswordReg !== this.state.RePassword) throw new Error("Passwords are different. Try again")
            if (reg.test(this.state.EmailReg) === false) throw new Error("Email incorrect")
            let body = {
                "email": this.state.EmailReg,
                "password": this.state.PasswordReg,
                "password_confirmation": this.state.RePassword,
                "first_name": this.state.First,
                "last_name": this.state.Last,
                "telephone_number": this.state.Telephone,
                "mobile_phone_number": this.state.Phone
            }
            let response = await create.signup(body)
            toast.success(response.message)
            this.setState({ModalOpen: true})
        } catch (error) {
            toast.error(error.message);
        }
    }


    async Verify() {
        try {
            if (this.state.Code === "") throw new Error("The field is empty")
            let body = {
                "verification_code": this.state.Code,
            }
            let response = await create.Verify(body)
            toast.success(response.message)
            this.setState({ModalOpen: false})
            this.props.history.push("/account")
        } catch (error) {
            toast.error(error.message);
        }
    }

    handleModal() {
        this.setState({
            ModalOpen: !this.state.ModalOpen
        })
        this.props.history.push("/account")
    }

    async SignUp(e) {
        e.preventDefault();
        try {
            if (this.state.email === "") throw new Error("The field Email is empty")
            if (this.state.password === "" || this.state.password.length <= 6) throw new Error("The field Password is very short. Minimum number of characters 7")
            if (reg.test(this.state.email) === false) throw new Error("Email incorrect")
            let body = {
                "email": this.state.email,
                "password": this.state.password,

            }
            let response = await create.signIn(body)
            this.props.history.push("/buy/address")
        } catch (error) {
            toast.error(error.message);
        }

    }

    render() {
        return (
            <div className={css(style.content)}>
                <Modal
                    style={customStyles}
                    isOpen={this.state.ModalOpen}
                    // onRequestClose={()=> this.props.handleCloseModal()}
                    contentLabel="Minimal Modal Example"
                    onRequestClose={() => this.handleModal()}
                >
                    <div className={css(style.ModalStyle)}>
                        <p>
                            <img onClick={() => this.handleModal()}
                                 src={require('../sources/cancel.png')} alt="cancel"/>
                        </p>
                        <span>
                            We’ve sent a 4 digital verification code by
                            SMS to 078XX-XXXXXXXX.
                            Please enter the code bellow.
                        </span>
                        <input maxlength="4" value={this.state.Code}
                               onChange={(e) => this.setState({Code: e.target.value})}
                               placeholder="Verification Code"/>
                        <div>
                            <button onClick={this.Verify}>Verify</button>
                            <h5 onClick={() => this.handleModal()}>Cancel Verification</h5>
                        </div>
                    </div>
                </Modal>
                <div className={`${css(style.logo)}`}>
                    <NavLink
                        to="/">
                        <img src={require('../sources/PazardyLogo.png')} alt="arrow"/>
                    </NavLink>
                </div>
                <div>
                    <div className={css(style.text)}>
                        <label className={css(style.labelNow)}>Customer information</label>
                        <label className={css(style.label)}>Shipping address</label>
                        <label className={css(style.label)}>Payment method</label>
                        <label style={{textAlign: "right"}} className={css(style.labelLast)}>Review &
                            Confirmation</label>
                    </div>
                    <div className={css(style.progressDiv)}>
                        <div className={css(style.progressNow)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>
                        <div className={css(style.progressLine)}></div>
                        <div className={css(style.progress)}></div>

                    </div>
                </div>
                <div className={`col-12 row ${css(style.body)}`}>
                    <div className={`col-6 row ${css(style.RegAco)}`}>
                        <div className={`col-11 ${css(style.LogoBody)}`}>
                            <p>CREATE ACOUNT</p>
                            <span>* REQUIRED</span>
                        </div>
                        <form className={`col-11 ${css(style.InputBody)}`}>
                            <input value={this.state.First} onChange={(e) => this.setState({First: e.target.value})}
                                   placeholder="First name"/>
                            <input value={this.state.Last} onChange={(e) => this.setState({Last: e.target.value})}
                                   placeholder="Last name"/>
                            <input value={this.state.EmailReg}
                                   onChange={(e) => this.setState({EmailReg: e.target.value})}
                                   placeholder="name@mail.com"/>
                            <input value={this.state.PasswordReg}
                                   onChange={(e) => this.setState({PasswordReg: e.target.value})}
                                   placeholder="Password*" type="password"/>
                            <input value={this.state.RePassword}
                                   onChange={(e) => this.setState({RePassword: e.target.value})}
                                   placeholder="Re-Enter Password*" type="password"/>
                            <input value={this.state.Phone} onChange={(e) => this.setState({Phone: e.target.value})}
                                   placeholder="Phone Number*" type="phone"/>
                            <input value={this.state.Telephone}
                                   onChange={(e) => this.setState({Telephone: e.target.value})}
                                   placeholder="Telephone Number*" type="phone"/>
                        </form>
                        <div className={`col-11 ${css(style.Subscribe)}`}>
                            <label className={css(style.labelCheck)}>
                                {this.state.Subscribe ?
                                    <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                        <span className={css(style.IconSpan)}>

                                            </span>)
                                }
                                <input
                                    className={css(style.check)}
                                    type="checkbox"
                                    checked={this.state.Subscribe}
                                    onChange={() => this.setState({Subscribe: !this.state.Subscribe})}
                                />
                                Subscribe to receive email updates.
                            </label><br/>
                            <span>Pazady doos not share or sell personal information</span>
                        </div>
                    </div>
                    <div className={`col-6 row ${css(style.Login)}`}>
                        <div className={`col-12 ${css(style.LogoBody)}`}>
                            <p>LOGON</p>
                            <span>* REQUIRED</span>
                        </div>
                        <form onSubmit={this.SignUp} className={`col-11 ${css(style.InputBody)}`}>
                            <input value={this.state.email} onChange={(e) => this.setState({email: e.target.value})}
                                   placeholder="Email*"/>
                            <input value={this.state.password}
                                   onChange={(e) => this.setState({password: e.target.value})} placeholder="Password*"
                                   type="password"/>
                            <button style={{display:"none"}} type="submit"></button>
                        </form>
                        <div className={css(style.SingIn)}>
                            <button onClick={this.SignUp} className={css(style.Button)}>Sing In</button>
                            <div>
                                <label className={css(style.labelCheck)}>
                                    {this.state.rememberMe ?
                                        <span className={css(style.checkIconSpan)}>
                                            <i className={`fas fa-check ${css(style.checkIcon)}`}> </i>
                                        </span> : (
                                            <span className={css(style.IconSpan)}>

                                            </span>)
                                    }
                                    <input
                                        className={css(style.check)}
                                        type="checkbox"
                                        checked={this.state.rememberMe}
                                        onChange={() => this.setState({rememberMe: !this.state.rememberMe})}
                                    />
                                    Remember Me
                                </label>
                                <span className={css(style.forgot)}>Forgot Password</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css(style.footer)}>
                    <div>
                        <NavLink
                            to="/buy/cart">
                            <div>
                                <img src={require('../sources/right-arrow .png')} alt="arrow"/>
                                <span>Return to cart</span>
                            </div>
                        </NavLink>
                        <NavLink
                            to="/buy/address">
                            <button className={css(style.Button)}>
                                Continue
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

const style = StyleSheet.create({
    ModalStyle: {
        padding: 20,
        ':nth-child(1n) > p': {
            margin: 0,
            display: "flex",
            alignItems: 'center',
            justifyContent: "flex-end",
            ':nth-child(1n) > img': {
                cursor: "pointer"
            }
        },
        ':nth-child(1n) > span': {
            width: 350,
            height: 66,
            color: '#808080',
            fontFamily: 'Poppins',
            fontSize: 16,
            fontWeight: 400,
            lineHeight: "26px",
            display: 'flex',
            textAlign: "center",
            margin: "30px auto"
        },
        ':nth-child(1n) > input': {
            width: "100%",
            height: 48,
            padding: "0 0 0 26px",
            borderRadius: 5,
            margin: "20px 0 0 0",
            border: "solid 1px rgba(85, 85, 85,0.5)",
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > div': {
            marginTop: 20,
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            ':nth-child(1n) > button': {
                color: '#ffffff',
                fontFamily: 'Poppins',
                fontSize: 18,
                fontWeight: 400,
                width: 156,
                height: 53,
                borderRadius: 7,
                cursor: "pointer",
                border: "none",
                backgroundImage: 'linear-gradient(to right, #37a973 0%, #6ddba2 100%)',
                ":focus": {
                    outline: "none"
                }
            },
            ':nth-child(1n) > h5': {
                cursor: "pointer",
                color: '#9a9a9a',
                fontFamily: 'Poppins',
                fontSize: 16,
                fontWeight: 300,
                textDecoration: 'underline',
            },
        }
    },
    labelCheck: {
        paddingRight: 50,
        fontSize: 12,
        cursor: 'pointer',
        fontWeight: 300,
        lineHeight: 4,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    checkIconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    forgot: {
        cursor: "pointer",
        textDecoration: "underline",
        width: 105,
        height: 15,
        fontSize: 12,
        fontWeight: 300,
        lineHeight: 2,
        letterSpacing: 0.4,
        color: "rgb(169, 169, 169)"
    },
    IconSpan: {
        marginTop: "-2px",
        width: '18px',
        height: '18px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle',
        marginRight: '10px',
        border: "solid 2px rgb(235, 235, 235)",
        borderRadius: '5px',
        cursor: 'pointer',
    },
    check: {
        display: 'none',
    },
    checkIcon: {
        textAlign: "center",
        paddingTop: "2px",
        fontSize: '10px',
        width: '20px',
        height: '18px',
        border: "solid 2px rgb(109, 219, 162)",
        borderRadius: '5px',
        backgroundColor: "rgb(109, 219, 162)",
        color: '#fff'
    },
    content: {
        fontFamily: "Poppins",
        height: "100%",
        width: "75%",
        float: "right",
        display: "flex",
        flexDirection: "column"
    },
    logo: {
        padding: "29px 0 104px 0"
    },
    Subscribe: {
        padding: 0,
        ':nth-child(1n) > span': {
            fontSize: 12,
            lineHeight: 2,
            letterSpacing: 0.4,
            color: "rgb(169, 169, 169)"
        }
    },
    progressDiv: {
        width: "92%",
        margin: "auto",
        justifyContent: "center",
        display: 'flex',
        alignItems: "center",
    },
    progressNow: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(161, 255, 211)",
        borderRadius: 50
    },
    progress: {
        height: 45,
        width: 45,
        backgroundColor: "rgb(238, 238, 238)",
        borderRadius: 50
    },
    progressLine: {
        height: 1,
        width: "24%",
        backgroundColor: "rgb(238, 238, 238)"
    },
    labelNow: {
        textAlign: "left",
        color: "rgb(161, 255, 211)",
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    labelLast: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "right",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    label: {
        color: 'rgb(68, 68, 68)',
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        fontSize: "1.5vh",
        width: "23.5%",
        textAlign: "center",
        lineHeight: 3.43,
        letterSpacing: 0.4,
        fontWeight: 600
    },
    text: {
        display: "flex",
        width: "100%",
        justifyContent: "space-between",
    },
    body: {
        padding: "0 0 0 15px",
        flex: "0 0 0",
        marginTop: 20,
        justifyContent: "space-between",
    },
    LogoBody: {
        fontFamily: 'Poppins',
        paddingBottom: 20,
        paddingLeft: 0,
        ':nth-child(1n) > p': {
            fontSize: 18,
            lineHeight: 1.56,
            margin: 0,
            color: "#000"
        },
        ':nth-child(1n) > span': {
            fontSize: 16,
            lineHeight: 1.75,
            letterSpacing: 0.5,
            color: "rgb(169, 169, 169)"
        }
    },
    InputBody: {
        padding: 0,
        ':nth-child(1n) > input': {
            height: 48,
            width: "100%",
            borderRadius: 5,
            border: "solid 1px rgba(85, 85, 85,0.5)",
            marginBottom: 26,
            paddingLeft: 26,
            fontFamily: "Poppins",
            fontSize: 16,
            letterSpacing: 0.5,
            fontWeight: 300,
            ":focus": {
                outline: "none"
            }
        }
    },
    RegAco: {
        justifyContent: 'flex-end',
        padding: 0
    },
    Login: {
        padding: "0 0 0 15px"
    },
    SingIn: {
        ':nth-child(1n) > button': {
            width: 170,
        }
    },
    Button: {
        cursor:'pointer',
        outline: "none",
        border: "none",
        backgroundImage: 'linear-gradient(to right, rgb(55, 169, 115), rgb(109, 219, 162));',
        borderRadius: 7,
        height: 53,
        color: "rgb(255, 255, 255)",
        fontFamily: "Poppins",
        fontSize: 18,
        lineHeight: 2.44,
        letterSpacing: 0.4,
    },
    footer: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: "flex-end",
        flex: 1,
        ':nth-child(1n) > div': {
            justifyContent: "space-between",
            display: "flex",
            paddingBottom: "54px",
            alignItems: "flex-end",
            ':nth-child(1n) > a': {
                ':nth-child(1n) > div': {
                    ':nth-child(1n) > span': {
                        fontSize: 14,
                        fontWeight: 300,
                        fontFamily: "Poppins",
                        lineHeight: 2,
                        letterSpacing: "0.2px",
                        color: "RGB(68,68,68)",
                        paddingLeft: 23
                    }
                },
                ":hover": {
                    textDecoration: "none",

                },
                ':nth-child(1n) > button': {
                    width: 189,
                }
            }
        }
    }
});

export default (withRouter(CustomerInfo))
