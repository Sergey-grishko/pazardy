import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import FixHeader from '../control/Fixheader'
import Header from '../control/header'
import Footer from '../control/footer'
import VisibilitySensor from "react-visibility-sensor";

const NewsArr = [
    {
        id:"1",
        description:"“Lorem Ipsum is simply dummy \n" +
        "text of the printing and typesetting”.\n",
        date:"12/03/2018\n",
        img:"photo-1530114583627-c759422e5088.png"

    },
    {
        id:"2",
        description:"“Lorem Ipsum is simply dummy \n" +
        "text of the printing and typesetting”.\n",
        date:"12/03/2018\n",
        img:"photo-1530173822362-c9e47227cb87.png"
    },
    {
        id:"3",
        description:"“Lorem Ipsum is simply dummy \n" +
        "text of the printing and typesetting”.\n",
        date:"12/03/2018\n",
        img:"photo-1530888571925-c766bb10af92.png"
    },
    {
        id:"4",
        description:"“Lorem Ipsum is simply dummy \n" +
        "text of the printing and typesetting”.\n",
        date:"12/03/2018\n",
        img:"photo-1530883154101-ce08df5ec9be.png"
    },

]

class News extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    onChangeIsVisible = (isVisible) => {
        this.setState({
            isVisible
        })
    }
    //
    componentDidMount(){
        window.scrollTo(0, 0);
    }

    render() {
        let news = NewsArr.map((value, index) => {
            return (
                <div className={`col-xl-6 col-md-12 ${css(style.ItemsSlideBottom)}`}>
                    <img src={require(`../sources/${value.img}`)} alt="imgNews"/>
                    <div>
                        <span>{value.description}</span>
                        <span>{value.date}</span>
                    </div>
                    <NavLink
                        to={`/news/${value.id}`}>
                                <span>Read More <img src={require('../sources/right-arrowsmoll.png')}
                                                     alt="choose"/></span>
                    </NavLink>
                </div>
            )
        })
        return (
            <div>
                <VisibilitySensor
                    onChange={this.onChangeIsVisible.bind(this)}
                    partialVisibility={true}
                >
                    <Header/>
                </VisibilitySensor>
                {!this.state.isVisible && <FixHeader/>}
                <div className={`container`}>
                    <div className={`col-xl-12 row ${css(style.News)}`}>
                        <div className={`col-xl-6 ${css(style.textNews)}`}>
                            <span>"Something"</span>
                            <h5></h5>
                            <div>
                                Jun 26, 2018<br/>
                                Our very own CEO and social media intern, Halli, sat down
                                for a chat with Dribbble cofounder Dan Cederholm on
                                Episode 38 of Overtime. They discuss how Halli went from
                                an engineering student to founding a global agency with
                                60 employees.
                                <br/><br/>
                                Halli peels back the curtain to reveal Ueno's origins and
                                where he hopes our agency will go in the next few years
                                (hint: it involves world domination).
                                Our very own CEO and social media intern, Halli, sat down
                                fora chat with Dribbble cofounder Dan Cederholm on
                                Episode 38 of Overtime. They discuss how Halli went from
                                an engineering student to founding a global agency with
                                60 employees.
                                <br/><br/>
                                Halli peels back the curtain to reveal Ueno's origins and
                                where he hopes our agency will go in the next few years
                                (hint: it involves world domination).
                            </div>
                        </div>
                        <div className={`col-xl-6 row`}>
                            <img src={require(`../sources/photo-1.png`)} alt="carrarDow"/>
                        </div>
                    </div>
                    <div className={`col-xl-12 ${css(style.otherNews)}`}>
                        <div>
                            <p>Other News</p>
                            <h5></h5>
                            <div className="col-xl-12 row">
                                {news}
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

const style = StyleSheet.create({
    otherNews:{
        marginTop:100,
        ':nth-child(1n) > div': {
            ':nth-child(1n) > div': {
                padding:0,
                margin:0
            },
            ':nth-child(1n) > p': {
                color: "#000000",
                fontFamily: "Lato",
                fontSize: 36,
                fontWeight: 300,
                textAlign: "center",
                width: "100%",
            },
            ':nth-child(1n) > h5': {
                margin: "0 auto",
                marginBottom:40,
                width: 114,
                height: 2,
                backgroundColor: "#000000"
            },
        }
    },
    ItemsSlideBottom: {
        display: "flex",
        flexDirection: 'column',
        ':nth-child(1n) > div': {
            margin: "25px 0 0 0",
            display: "flex",
            justifyContent: "space-between",
            color: "#aaaaaa",
            fontFamily: "Lato",
            fontWeight: 300,
            ':nth-child(1n) > span:first-child': {
                width: 330,
                fontSize: 20,

            },
            ':nth-child(1n) > span:last-child': {
                fontSize: 18,

            }
        },
        ':nth-child(1n) > a': {
            margin: "50px 0 50px 0",
            textDecoration: "none",
            ':nth-child(1n) > span': {
                marginTop: 50,
                color: "#ff776d",
                fontFamily: "Lato",
                fontSize: 20,
                fontWeight: 400,
                ':nth-child(1n) > img': {
                    paddingLeft: 20
                }
            }
        }
    },
    News: {
        display:'flex',
        justifyContent:"center",
        margin: "140px 0 0 0",
        padding: 0,
        ':nth-child(1n) > div:last-child': {
            display:'flex',
            justifyContent:"center"
        }
    },
    textNews: {
        padding: "0 50px 0 0",
        display:"flex",
        flexDirection:'column',
        '@media (max-width: 1199px)': {
            padding: "0",
        },
        ':nth-child(1n) > span': {
            color: "#000000",
            margin: "0 0 24px 0",
            fontFamily: "Lato",
            fontSize: 36,
            textAlign: "center",
            width: "100%",
            fontWeight: 300,
        },
        ':nth-child(1n) > h5': {
            margin: "0 auto",
            marginBottom:40,
            width: 114,
            height: 2,
            backgroundColor: "#000000"
        },
        ':nth-child(1n) > div': {
            color: "#000000",
            fontFamily: "Poppins",
            fontSize: 16,
            fontWeight: 400,
            lineHeight: "28px"
        }
    }
});

export default (withRouter(News))
