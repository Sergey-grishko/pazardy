import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {css, StyleSheet} from 'aphrodite';
import Dropdown from 'rc-dropdown';
import 'rc-dropdown/assets/index.css';


import FixHeader from '../control/Fixheader'
import Header from '../control/header'
import Footer from '../control/footer'
import VisibilitySensor from "react-visibility-sensor";
import StarRatings from 'react-star-ratings';

import * as Item from "../../action/items";
import connect from "react-redux/es/connect/connect";
import {toast, ToastContainer} from "react-toastify";
import * as Cart from "../../action/cart";
import {store} from "../../index";
import {CART} from "../../reducers/const";


class Items extends React.Component {
    constructor() {
        super()
        this.state = {
            count: 1,
            isVisible: true,
            Items
        }
        this.onAdd = this.onAdd.bind(this)
    }

    async componentDidMount() {
        let id = this.props.match.params.id;
        let Items = await Item.GetItem(id)
        this.setState({
            Items
        })
        console.log(this.state.Items)
    }

    AddCart = async () => {
        let items = this.state.Items
        if (localStorage.getItem("PazardyCartId") !== null) {
            let date = {
                cart_item: {
                    store_item_id: items.item.id,
                    quantity: this.state.count
                }
            }
            await Cart.AddCartLoged(localStorage.getItem("PazardyCartId"), date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        } else {
            let cartId = await Cart.CartInitialize()
            localStorage.setItem("PazardyCartId", cartId.cart.cart_id)
            let date = {
                cart_item: {
                    store_item_id: items.item.id,
                    quantity: this.state.count
                }
            }
            await Cart.AddCartLoged(cartId.cart.cart_id, date)
            let cart = await Cart.GetCartLoged()
            store.dispatch({
                type: CART.ITEMSCART,
                payload: cart.cart_items.length
            });
            toast.success("Products have been added to the cart");
        }
    }

    onChangeIsVisible(isVisible) {
        this.setState({
            isVisible
        })
    }

    onAdd(count) {
        if (count !== -1)
            this.setState({count: count})
    }

    render() {
        // let ItemsInfo = slide.map((value, index) => {
        //     return (
        //         <div className={`col-xl-3 col-md-6 ${css(style.Items)}`} key={index}>
        //             <div className={` ${css(style.slideBox)}`}>
        //                 <div>
        //                             <span><StarRatings
        //                                 starSpacing="1px"
        //                                 starDimension="15px"
        //                                 rating={value.rating}
        //                                 starRatedColor="#ff776d"
        //                                 numberOfStars={5}
        //                                 name='rating'
        //                             /></span><br/>
        //                     <img
        //                         src={require('../sources/pngpix-com-lays-potato-chips-pack-png-image-500x689-copy-11.png')}
        //                         alt="pngpix"/>
        //                     <div>
        //                         <p>${value.price}</p>
        //                         <div>
        //                             <div>-</div>
        //                             <div>{this.state.count}</div>
        //                             <div>+</div>
        //                         </div>
        //                     </div>
        //                 </div>
        //                 <div className={css(style.selectSlider)}>
        //                             <span className={css(style.selectStyle)}>
        //                                <ul className="col-xl-3 col-md-12 top-level-menu">
        //                                     <li>
        //                                         <a className={css(style.sortByTitle)}>{value.store.store_name}<img
        //                                             src={require('../sources/down-arrow.png')} alt="arrow"/></a>
        //                                         <ul className={`second-level-menu ${css(style.sortBy)}`}>
        //                                             <li>
        //                                                 <a>
        //                                                 Other available store<br/>
        //                                                 in your aresa</a>
        //                                             </li>
        //                                             <li><a>{value.store.store_name}<span>10$</span></a></li>
        //                                         </ul>
        //                                     </li>
        //                                 </ul>
        //                             </span>
        //                     <button>Add</button>
        //                 </div>
        //             </div>
        //             <div className={css(style.NameItems)}>
        //                 <div>
        //                     <p>{value.item.english_name}</p>
        //                     <h5>{value.item.english_description}</h5>
        //                 </div>
        //                 <span>
        //                            <StarRatings
        //                                changeRating={() => this.AddtoFavorite(value.id)}
        //                                starSpacing="1px"
        //                                starDimension="20px"
        //                                rating={1}
        //                                starRatedColor={this.state.idFavorite.some((val) => val.id === value.id) ? "#6ddba2" : "#cbd3e3"}
        //                                numberOfStars={1}
        //                                name='rating'
        //                            />
        //                         </span>
        //             </div>
        //         </div>
        //     )
        // });
        return (
            <div>
                <VisibilitySensor
                    onChange={this.onChangeIsVisible.bind(this)}
                    partialVisibility={true}
                >
                    <Header/>
                </VisibilitySensor>
                {!this.state.isVisible && <FixHeader/>}
                <div className={`container ${css(style.categoriesItems)}`}>
                    <div className={css(style.itemsContainer)}>
                        <p>{this.state.Items.item !== undefined ? this.state.Items.item.english_name : null}</p>
                        <div className="col-xl-12 row">
                            <div className="col-xl-6">
                                <div>
                                    <img style={{width: "400px"}}
                                         src={this.state.Items.item !== undefined ? this.state.Items.item.normal_image : null}
                                         alt="pngpix"/>
                                </div>
                                <div>
                                    <h5>${this.state.Items.price}</h5>
                                    <StarRatings
                                        starSpacing="8px"
                                        starDimension="30px"
                                        rating={this.state.Items.rating}
                                        starRatedColor="#ff776d"
                                        numberOfStars={5}
                                        name='rating'
                                    />
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <p>
                                    Details
                                </p>
                                <span>
                                  {this.state.Items.item !== undefined ? this.state.Items.item.english_description : null}
                                </span>
                                <div className={css(style.AddItems)}>
                                    <div onClick={() => this.onAdd(this.state.count - 1)}>-</div>
                                    <h5>{this.state.count}</h5>
                                    <div onClick={() => this.onAdd(this.state.count + 1)}>+</div>
                                </div>
                                <div>
                                    <label className={css(style.label)}>
                                        Choose other available store in your area
                                    </label>
                                    <span className={css(style.selectStyleItems)}>
                                       <ul className="col-xl-12 top-level-menu">
                                            <li>
                                                <a className={css(style.sortByTitle)}> <span>{this.state.Items.store !== undefined ? this.state.Items.store.store_name : null}</span><img
                                                    src={require('../sources/down-arrow.png')} alt="arrow"/></a>
                                                <ul className={`second-level-menu ${css(style.sortBy)}`}>
                                                    <li>
                                                        <a>
                                                        Other available store<br/>
                                                        in your aresa</a>
                                                    </li>
                                                    <li><a>{this.state.Items.store !== undefined ? this.state.Items.store.store_name : null}<span>10$</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </span>

                                </div>
                                <div className={css(style.ButtonAdd)}>
                                    <button onClick={() => this.AddCart()}>Add to Bag</button>
                                    <button>Order Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span>
                        <span>Reletied Products</span>
                    </span>
                    <div className="col-xl-12 row">
                        {/*{ItemsInfo}*/}
                    </div>
                </div>
                <Footer/>
                <ToastContainer/>
            </div>
        )
    }
}

const style = StyleSheet.create({
    sortBy: {
        width: 180,
        top: 30,
        ':nth-child(1n) > li': {
            padding: "0 20px 0 20px",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 14,
            display: "flex",
            fontWeight: 300,
            ':nth-child(1n) > a': {
                width: "100%",
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > span': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                    lineHeight: "40px"
                }
            }
        },
        ':nth-child(1n) > li:first-child': {
            height: "100%",
            color: "#444444",
            fontFamily: "Poppins",
            fontSize: 12,
            fontWeight: 400,
        }
    },
    ButtonAdd: {
        display: 'flex',
        justifyContent: "space-between",
        marginTop: 50,
        ':nth-child(1n) > button': {
            width: 233,
            height: 45,
            borderRadius: 7,
            background: "none",
            transition: "0.9s",
            fontSize: 24,
            fontWeight: 300,
            cursor: "pointer",
            ":hover": {
                transition: "0.9s",

            },
            ":focus": {
                outline: "none"
            }
        },
        ':nth-child(1n) > button:first-child': {
            border: "1px solid #6ddba2",
            color: "#6ddba2",
            fontFamily: "Poppins",
            ":hover": {
                color: "#fff",
                background: "#6ddba2",
            },

        },
        ':nth-child(1n) > button:last-child': {
            border: "1px solid #e62b33",
            color: "#e62b33",
            ":hover": {
                color: "#fff",
                background: "#e62b33",
            },
        }
    },
    sortByTitle: {
        width: "100%",
        justifyContent: "flex-end",
        color: "#444444",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 400,
    },
    slideBox: {
        borderRadius: 7,
        border: "1px solid #e1e1e1",
        height: "300px",
        ':nth-child(1n) > div:first-child': {
            padding: "10px 10px 0 10px",
            borderBottom: "1px solid #e1e1e1",
            ':nth-child(1n) > img': {
                display: "flex",
                margin: "25px auto"
            },
            ':nth-child(1n) > div': {
                display: "flex",
                justifyContent: "space-between",
                ':nth-child(1n) > p': {
                    color: "#6ddba2",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    fontWeight: 400,
                },
                ':nth-child(1n) > div': {
                    display: "flex",
                    alignItems: "center",
                    height: "14px",
                    marginTop: 8,
                    ':nth-child(1n) > div': {
                        padding: "0 12.5px",
                        color: "#a9a9a9",
                        fontFamily: "Poppins",
                        fontSize: 14,
                        fontWeight: 300,
                    },
                    ':nth-child(1n) > div:last-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                    ':nth-child(1n) > div:first-child': {
                        width: 22,
                        textAlign: "center",
                        background: "#fafafa",
                        borderRadius: 5,
                        cursor: "pointer"
                    },
                }
            }
        }
    },
    selectSlider: {
        display: 'flex',
        alignItems: 'center',
        height: "calc(100% - 248px)",
        ':nth-child(1n) > span': {
            width: "80%",
            alignItems: 'center',
            height: "100%",
            ':nth-child(1n) > select': {
                color: "#000000",
                border: "none",
            }
        },
        ':nth-child(1n) > button': {
            background: "none",
            border: "none",
            borderLeft: "1px solid #e1e1e1",
            color: "#6ddba2",
            fontFamily: "Poppins",
            fontSize: 18,
            fontWeight: 400,
        }
    },
    selectStyle: {
        width: "14%",
        display: 'flex',
        ':nth-child(1n) > div': {
            marginLeft: "130px",
            pointerEvents: 'none',
            position: 'absolute',
            '@media (max-width: 1199px)': {
                display: "none"
            }
        },
        ':nth-child(1n) > select': {
            width: "100%",
            paddingLeft: "15px",
            fontSize: 17,
            fontFamily: "Lato",
            color: "#a9a9a9",
            background: "none",
            border: 'none',
            borderLeft: "1px solid #555555",
            borderRight: "1px solid #555555",
            appearance: 'none',
            height: 29,
            ":focus": {
                outline: "none"
            },

        },
        ':nth-child(1n) > ul': {
            ':nth-child(1n) > li': {
                display: "flex",
                justifyContent: 'center',
                ':nth-child(1n) > ul': {
                    width: 192,
                    top: 50
                }
            }
        }
    },
    NameItems: {
        marginTop: 25,
        display: "flex",
        justifyContent: "space-between",
        ':nth-child(1n) > div': {
            ':nth-child(1n) > p': {
                margin: 0,
                cursor: "pointer",
                color: "#444444",
                fontSize: 18,
                fontFamily: "Poppins",
                fontWeight: 400
            },
            ':nth-child(1n) > h5': {
                color: "#a9a9a9",
                fontSize: 14,
                fontFamily: "Poppins",
                fontWeight: 300
            }
        }
    },
    itemsContainer: {
        marginBottom: "140px",
        ':nth-child(1n) > p': {
            color: "#444",
            marginBottom: "40px",
            fontFamily: "Poppins",
            fontSize: 24,
            fontWeight: 400,
        },
        ':nth-child(1n) > div': {
            padding: 0,
            margin: 0,
            ':nth-child(1n) > div:first-child': {
                height: 700,
                padding: 0,
                borderRadius: 7,
                border: "1px solid #e1e1e1",
                ':nth-child(1n) > div:first-child': {
                    height: "85%",
                    display: "flex",
                    justifyContent: 'center',
                    alignItems: "center",
                    borderBottom: "2px solid #555"
                },
                ':nth-child(1n) > div:last-child': {
                    display: "flex",
                    margin: "0 25px",
                    height: "15%",
                    justifyContent: 'space-between',
                    alignItems: "center",
                    ':nth-child(1n) > h5': {
                        color: "#6ddba2",
                        fontFamily: "Poppins",
                        fontSize: 41,
                        fontWeight: 400
                    }
                }
            },
            ':nth-child(1n) > div:last-child': {
                padding: "0 0 0 30px",
                ':nth-child(1n) > p': {
                    color: "#444444",
                    fontFamily: "Poppins",
                    fontSize: 20,
                    fontWeight: 500
                },
                ':nth-child(1n) > span': {
                    color: "#444444",
                    fontFamily: "Poppins",
                    fontSize: 18,
                    lineHeight: "38px",
                    fontWeight: 300
                },
            }
        }
    },
    AddItems: {
        marginTop: 50,
        display: 'flex',
        ':nth-child(1n) > div': {
            width: 52,
            userSelect: 'none',
            height: 33,
            borderRadius: 5,
            backgroundColor: "#fafafa",
            color: "#a9a9a9",
            fontFamily: "Poppins",
            fontSize: 32,
            fontWeight: 300,
            display: "flex",
            justifyContent: 'center',
            alignItems: "center",
            cursor: "pointer"
        },
        ':nth-child(1n) > h5': {
            color: "#a9a9a9",
            display: "flex",
            justifyContent: 'center',
            alignItems: "center",
            fontFamily: "Poppins",
            fontSize: 32,
            fontWeight: 300,
            width: 52,
            height: 33,
            margin: "0 5px"
        }

    },
    categoriesItems: {
        ':nth-child(1n) > span': {
            display: "flex",
            marginBottom: 40,
            justifyContent: "space-between",
            ':nth-child(1n) > span:first-child': {
                color: "#444444",
                fontFamily: "Poppins",
                fontSize: 18,
                fontWeight: 400,
            }
        },
        ':nth-child(1n) > div:last-child': {
            margin: 0,
            padding: 0
        }
    },
    label: {
        color: "#a9a9a9",
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: 300,
        marginTop: 100
    },
    selectStyleItems: {
        width: "100%",
        display: 'flex',
        ':nth-child(1n) > div': {
            width: "100%",
            marginLeft: "130px",
            pointerEvents: 'none',
            position: 'absolute',
            '@media (max-width: 1199px)': {
                display: "none"
            }
        },
        ':nth-child(1n) > select': {
            width: "100%",
            paddingLeft: "15px",
            fontSize: 17,
            fontFamily: "Lato",
            color: "#a9a9a9",
            background: "none",
            border: 'none',
            borderLeft: "1px solid #555555",
            borderRight: "1px solid #555555",
            appearance: 'none',
            height: 29,
            ":focus": {
                outline: "none"
            },

        },
        ':nth-child(1n) > ul': {
            ':nth-child(1n) > li': {
                display: "flex",
                width: "100%",
                justifyContent: 'center',
                ':nth-child(1n) > ul': {
                    width: "100%",
                    top: 50
                },
                ':nth-child(1n) > a': {
                    padding: "0 10px",
                    backgroundColor: "#fafafa",
                    justifyContent: 'space-between'
                }
            }
        }
    },
});

export default connect(store => ({store: store}))(withRouter(Items))
