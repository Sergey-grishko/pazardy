import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
// import { syncWithStore, setLanguage } from 'react-redux-multilang';
import logger from 'redux-logger';
import "./reset.css"

// redux & router
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import createHistory from 'history/createBrowserHistory'
import {ConnectedRouter, routerMiddleware} from 'react-router-redux'
import reducers from './reducers'




import Wrapper from './components/wrapper'

const history = createHistory()
const middleware = routerMiddleware(history)

export const store = createStore(reducers, applyMiddleware(middleware, logger));

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div id="container">
                <Wrapper/>
            </div>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
)
registerServiceWorker()