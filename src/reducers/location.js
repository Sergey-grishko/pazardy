import {LOCATION} from './const'

const initState = {
    code: {
        latitude: 41.002209,
        longitude: 29.084418
    }

}

export default function (state = initState, action) {
    switch (action.type) {
        case LOCATION.LOCATION:
            return {...state, code: action.payload};
        case LOCATION.UNSET:
            return {...initState}
        default:
            return state
    }
}