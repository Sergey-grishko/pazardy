import { CART } from './const'

const initState = {
    ItemsCart:0,
    idItems:[
        {
            id:0
        }
    ]
}

export default function (state = initState, action) {
    switch (action.type) {
        case CART.ITEMSCART:
            return {...state, ItemsCart: action.payload};
        case CART.UNSET:
            return { ...initState }
        default:
            return state
    }
}