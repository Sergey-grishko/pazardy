import { routerReducer } from 'react-router-redux'
import { combineReducers } from 'redux'

import cart from './сart'
import location from './location'
import categories from './categories'

export default combineReducers({
    router: routerReducer,
    cart,
    location,
    categories
})