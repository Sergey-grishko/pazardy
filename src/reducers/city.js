import { CITY } from './const'

const initState = {
   city:[]
}

export default function (state = initState, action) {
    switch (action.type) {
        case CITY.CITY:
            return {...state, ItemsCart: action.payload};
        case CITY.UNSET:
            return { ...initState }
        default:
            return state
    }
}