import {CATEGORIES} from './const'

const initState = {
    name:""

}

export default function (state = initState, action) {
    switch (action.type) {
        case CATEGORIES.CATEGORIES:
            return {...state, name: action.payload};
        case CATEGORIES.UNSET:
            return {...initState}
        default:
            return state
    }
}