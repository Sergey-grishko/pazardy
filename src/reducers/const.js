export const CART = {
    ITEMSCART: 'ITEMSCART',
    UNSET: 'CART_UNSET',
}
export const LOCATION = {
    LOCATION: 'LOCATION',
    UNSET: 'LOCATION_UNSET',
}

export const CATEGORIES = {
    CATEGORIES: 'CATEGORIES',
    UNSET: 'CATEGORIES_UNSET',
}

export const CITY = {
    CITY: 'CITY',
    UNSET: 'CITY_UNSET',
}
