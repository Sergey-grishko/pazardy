import Api from './api'
// import { store } from '../index'
// import { push } from 'react-router-redux'
// import { USER } from '../reducers/const'



export async function signup(body) {
    let res = await Api.post("user/auth", body, false, true);
    if (res.status === 200) throw new Error("This account with either Email or Phone already exists")
    if (res.status !== 201) throw new Error("Oops ... problems")
    localStorage.setItem('pazardy-token', res.headers.get("access-token"));
    localStorage.setItem('client', res.headers.get("client"));
    localStorage.setItem('expiry', res.headers.get("expiry"));
    localStorage.setItem('uid', res.headers.get("uid"));
    let data = await res.json()
    return data
}

export async function signIn(body) {
    let res = await Api.post("user/auth/sign_in", body, false, true);
    if (res.status !== 200) throw new Error("Invalid login credentials. Please try again.")
    localStorage.setItem('pazardy-token', res.headers.get("access-token"));
    localStorage.setItem('client', res.headers.get("client"));
    localStorage.setItem('expiry', res.headers.get("expiry"));
    localStorage.setItem('uid', res.headers.get("uid"));
    let data = await res.json()
    return data
}

export async function Verify(body) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.put(`user/auth/verification_code?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, body, true, true);
    if (res.status !== 200) throw new Error(res.message)
    let data = await res.json()
    return data
}
export async function ChangePassword(password) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.put(`user/auth/password?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + `&password=`+password+`&password_confirmation=`+password+``, null, true, true);
    // if (res.status !== 200) throw new Error("error")
    let data = await res.json()
    return data
}

export async function signOut(body) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.deletion(`user/auth/sign_out?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, body, true, true);
    let data = await res.json()
    return data
}