import Api from './api'

export async function GetCategorie() {
    let res = await Api.get(`categories`, null, false, true);
    let data = await res.json()
    return data
}
export async function GetCategorieName() {
    let res = await Api.get(`store_categories`, null, false, true);
    let data = await res.json()
    return data
}
// export async function GetSubCategorie(id) {
//     let res = await Api.get(`categories/`+id+`/sub_categories`, null, false, true);
//     let data = await res.json()
//     return data
// }
