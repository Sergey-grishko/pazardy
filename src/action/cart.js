import Api from "./api";

export async function AddCartLoged(id,body) {
    let res = await Api.post(`cart_items?cart_id=`+id, body, true, true);
    let data = await res.json()
    GetCartLoged()
    return data
}

export async function GetCartLoged() {
    let res = await Api.get(`cart_items?cart_id=`+localStorage.getItem("PazardyCartId"), null, true, true);
    let data = await res.json()
    return data
}

export async function CartInitialize() {
    let res = await Api.post(`carts`, null, true, true);
    let data = await res.json()
    return data
}

export async function DeletItemCart(id) {
    let res = await Api.deletion(`cart_items/`+id+`?cart_id=`+localStorage.getItem("PazardyCartId"), null,true);
    let data = await res.json()
    return data
}