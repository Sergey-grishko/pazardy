import Api from "./api";
import {store} from "../index";
import {CART} from "../reducers/const";

export async function addAddress(body) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.post(`addresses?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, body, true, true);
    let data = await res.json()
    return data
}
// export async function GetAllArddress() {
//     let res = await Api.get(`cities`);
//     let data = await res.json()
//     console.log(data)
//     // return data
// }

export async function GetArddress() {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.get(`addresses?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``);
    let data = await res.json()
    console.log(data)
    return data
}
export async function PutArddress(id,body) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.put(`addresses/`+id+`?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, body, true, true);
    let data = await res.json()
    console.log(data)
    return data
}