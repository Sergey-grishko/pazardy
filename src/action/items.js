import Api from './api'

export async function GetItemsCategories(id, page,lat,log) {
    let res = await Api.get(`store_items?category_id=` + id + `&lat=41.002209&long=29.084418&page=` + page, null, false, true);
    // let res = await Api.get(`store_items?category_id=` + id + `&lat=`+lat+`&long=`+log+`&page=` + page, null, false, true);
    let data = await res.json()
    return data
}

export async function GetItemsSubCategories(category_id, sub_category_id, page,lat,log) {
    let res = await Api.get(`store_items?category_id=` + category_id + `&sub_category_id=` + sub_category_id + `&lat=`+lat+`&long=`+log+`&page=` + page, null, false, true);
    let data = await res.json()
    return data
}

export async function AllFavorited() {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.get(`store_items/favorites?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, null, true, true);
    if (res.status !== 200) throw new Error(res.message)
    let data = await res.json()
    return data
}

export async function AddFavorited(id) {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.get(`store_items/` + id + `/toggle_favorite?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, null, true, true);
    if (res.status === 500) throw new Error("You need to register to add items")
    if (res.status !== 200) throw new Error(res.message)
    let data = await res.json()
    AllFavorited()
    return data
}
export async function GetItem(id) {
    let res = await Api.get(`store_items/`+id, null, false, true);
    if (res.status !== 200) throw new Error(res.message)
    let data = await res.json()
    return data
}