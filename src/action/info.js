import Api from './api'

export async function GetInfo() {
    let accessToken = localStorage.getItem('pazardy-token');
    let client = localStorage.getItem('client');
    let expiry = localStorage.getItem('expiry');
    let uid = localStorage.getItem('uid');
    let res = await Api.get(`addresses?access-token=` + accessToken + `&token-type=Bearer&client=` + client + `&expiry=` + expiry + `&uid=` + uid + ``, null, true, true);
    let data = await res.json()
    return data
}
