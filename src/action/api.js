// import { store } from '../index'

export default class Api{

    static host = " https://pazardy-test-api.herokuapp.com/api/v1/";

    static getTokenFromLocalStorage(){
        return localStorage.getItem('access-token');
    }

    static setTokenToLocalStorage(token){
        return localStorage.setItem('pazardy_token', token);
    }


    static deleteTokenFromLocalStorage(){
        return localStorage.removeItem('pazardy_token');
    }

    static get(endpoint, authorization = true, id = ''){
        let headers = {

        }

        if(authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'GET',
            headers
        })
    }

    static post(endpoint, body = null, authorization = true, headers = {}, convertToJSON = true){

        if (convertToJSON) body = JSON.stringify(body)
        if(headers !== false && Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }
        if (headers === false) headers = {}
        if(authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()
        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'POST',
            headers,
            body
        })

    }

    static put(endpoint, body = null, authorization = true, headers = {}){
        body = JSON.stringify(body)

        if(Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }

        if(authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'PUT',
            headers,
            body
        })
    }

    static deletion(endpoint, body = null, authorization = true, headers = {}) {
        body = JSON.stringify(body)

        if (Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }

        if (authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'DELETE',
            headers,
            body
        })
    }

    // static getLanguage(){
    //     let language = localStorage.getItem('language');
    //     if(!language) language = navigator.language || navigator.userLanguage
    //     if(!language || ['en','nl'].indexOf(language) < 0) language = 'en'
    //
    //     return language
    // }
}